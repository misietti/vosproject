package it.unibz.vos.testing.util;

import java.util.ArrayList;
import java.util.Date;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidEmailException;
import it.unibz.vos.dbclasses.exceptions.InvalidNameException;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.InvalidQuestionException;
import it.unibz.vos.dbclasses.exceptions.InvalidSurnameException;
import it.unibz.vos.dbclasses.exceptions.InvalidUserNameException;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.dbclasses.exceptions.UsernameAlreadyTakenException;
import it.unibz.vos.managementLayer.WUserManager;

import org.hibernate.Session;
import org.junit.Assert;
public class TestingUtilities {

	private static ArrayList<WUser> usersToDelete =  new ArrayList<WUser>();
	private static ArrayList<Poll>  pollsToDelete = new ArrayList<Poll>();
	private static ArrayList<Option>  optionsToDelete = new ArrayList<Option>();
	
	public static void addOptionToDelete(Option option){
		optionsToDelete.add(option);
	}
	
	public static void deleteALlTestGarbage(Session session){
		for(Option o : optionsToDelete){
			TestingUtilities.deleteOption(session,o);
		}
		
		for (Poll p : pollsToDelete){
			TestingUtilities.deletePoll(session,p);
		}
		
		for (WUser u : usersToDelete){
			TestingUtilities.deleteUser(session,u);
		}
		optionsToDelete.clear();
		pollsToDelete.clear();
		usersToDelete.clear();
		
	}
	
	/**
	 * return a poll not saved on the database
	 * @param creator
	 * @return
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidPreferenceNumber 
	 */
	public static Poll getPollForTestingNotSavedOnDatabase(Session session,WUser creator) throws PollAlreadyExistsException, InvalidPreferenceNumber{
		Poll thePoll = null;
		try {
			thePoll = new Poll(session,"therdf12345 is a qufdsafdsafdsfdsaestion",false,new java.util.Date(),10, creator,true,false);
		} catch (InvalidQuestionException e) {
			Assert.fail("Already tested though");
		}
		return thePoll;
	}
	

	/**
	 * return a poll not saved on the database
	 * @param creator
	 * @return
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidPreferenceNumber 
	 */
	public static Poll getPollForTestingNotSavedOnDatabase(Session session,WUser creator,String question) throws PollAlreadyExistsException, InvalidPreferenceNumber{
		Poll thePoll = null;
		try {
			thePoll = new Poll(session,question,false,new java.util.Date(),10, creator,true,false);
		} catch (InvalidQuestionException e) {
			Assert.fail("Already tested though");
		}
		return thePoll;
	}
	
	public static Option insertOptionForTesting(Session session, Poll thePoll) throws EmptyOptionTextException{
		Option theQuestion = new Option("This is the text of a question", thePoll);
		session.save(theQuestion);
		optionsToDelete.add(theQuestion);
		thePoll.getOptions().add(theQuestion);
		session.merge(thePoll);
		session.flush();
		return theQuestion;
	}
	
	public static Option insertOptionForTesting(Session session , Poll thePoll, String text) throws EmptyOptionTextException{
		Option theQuestion = new Option(text, thePoll);
		session.save(theQuestion);
		optionsToDelete.add(theQuestion);
		thePoll.getOptions().add(theQuestion);
		session.merge(thePoll);
		session.flush();
		return theQuestion;
	}
	
	/**
	 * The poll that is returned is signed for deletion with use of the method deleteALlTestGarbage()
	 * @param creator the use that creates the poll
	 * @return a poll saved on the databse
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidPreferenceNumber 
	 */
	public static Poll insertPollForTesting(Session session, WUser creator) throws PollAlreadyExistsException, InvalidPreferenceNumber{
		

		
		Poll thePoll = null;
		try {
			thePoll = new Poll(session,"there is a question",false,new java.util.Date(),10, creator,true,false);
		} catch (InvalidQuestionException e) {
			Assert.fail("Already tested though");
		}
	
		session.save(thePoll);
		

		pollsToDelete.add(thePoll);
		return thePoll;
	}
	
	/**
	 * The poll that is returned is signed for deletion with use of the method deleteALlTestGarbage()
	 * @param creator the use that creates the poll
	 * @return a poll saved on the databse
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidPreferenceNumber 
	 */
	public static Poll insertPollForTesting(Session session,WUser creator ,String question) throws PollAlreadyExistsException, InvalidPreferenceNumber{
		
	
		Poll thePoll = null;
		try {
			thePoll = new Poll(session,question,false,new java.util.Date(),10, creator,true,true);
		} catch (InvalidQuestionException e) {
			Assert.fail("Already tested though");
		}
	
		session.save(thePoll);
		

		
		pollsToDelete.add(thePoll);
		return thePoll;
	}
	
	/**
	 * The poll that is returned is signed for deletion with use of the method deleteALlTestGarbage()
	 * @param creator the use that creates the poll
	 * @return a poll saved on the databse
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidPreferenceNumber 
	 */
	public static Poll insertPollForTesting(Session session, WUser creator,
			String question, Date date) throws PollAlreadyExistsException, InvalidPreferenceNumber {
		Poll thePoll = null;
		try {
			thePoll = new Poll(session,question,false,date,10, creator,true,true);
		} catch (InvalidQuestionException e) {
			Assert.fail("Already tested though");
		}
	
		session.save(thePoll);
		

		
		pollsToDelete.add(thePoll);
		return thePoll;
	}
	/**
	 * The user that is returned is signed for deletion with use of the method deleteALlTestGarbage()
	 * if username is null, a static one will be used
	 * if the email is null, a static ont will be used
	 * @param session 
	 * @return a Wuserr saved on the database
	 */
	public static WUser insertWUserForTesting(Session session) {
		 
		String username = "username123";
		String emailstart = "fda";
		String email= emailstart + "@gmail.com";
		String name = "gino";
		String password = "there is this password";
		String surname = "datsur";
		WUser user = null;
		try {
			user = new WUser(session,username, name, surname, password, email);
		} catch (UsernameAlreadyTakenException e2) {
			// These exception should not happen, leaving the print stack trace
			Assert.fail(e2.getMessage());
		} catch (InvalidUserNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidSurnameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidPassowrdException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidEmailException e2) {
			Assert.fail(e2.getMessage());
		}

		WUserManager.insertWUser(session,user);
		usersToDelete.add(user);
		
		return user;

	}

	public static WUser insertWUserForTesting(Session session,String username, String emailstart) {
		 
		String email= emailstart + "@gmail.com";
	
		String name = "gino";
		String password = "there is this password";
		String surname = "datsur";
		WUser user = null;
		try {
			user = new WUser(session,username, name, surname, password, email);
		} catch (UsernameAlreadyTakenException e2) {
			// These exception should not happen, leaving the print stack trace
			Assert.fail(e2.getMessage());
		} catch (InvalidUserNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidSurnameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidPassowrdException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidEmailException e2) {
			Assert.fail(e2.getMessage());
		}

		WUserManager.insertWUser(session,user);
		usersToDelete.add(user);
		
		return user;

	}
	/**
 * 
 * @return a Wuser not saved ont the database
 */
	public static WUser getWuserForTestingNotSavedOnDatabase(Session session) {
		String email = "reqwrefdsawqrewq@m.ail";
		String name = "gino";
		String password = "there is this password";
		String surname = "datsur";
		String username = "rewrcccewqrbbewqrqew";
		WUser user = null;
		try {
			user = new WUser(session,username, name, surname, password, email);
		} catch (UsernameAlreadyTakenException e2) {
			// These exception should not happen, leaving the print stack trace
			Assert.fail(e2.getMessage());
		} catch (InvalidUserNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidSurnameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidPassowrdException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidEmailException e2) {
			Assert.fail(e2.getMessage());
		}

		return user;

	}
/**
 * delete the user from the database
 * @param user
 */
	public static void deleteUser(Session session,WUser user) {
		WUser userToDelete = (WUser) session.get(WUser.class, user.getID());
		if (userToDelete != null)
		session.delete(userToDelete);
		session.flush();
	}
	/**
	 * 
	 */
	public static void deletePoll(Session session,Poll thePoll){
		
		Poll pollToDelete = (Poll) session.get(Poll.class, thePoll.getID());
		
		if (pollToDelete != null)
		session.delete(pollToDelete);
	}

public static void deleteOption(Session session,Option option){

	Option optionToDelete = (Option) session.get(Option.class, option.getId());
	
	if (optionToDelete != null)
	session.delete(optionToDelete);
	
}



}
