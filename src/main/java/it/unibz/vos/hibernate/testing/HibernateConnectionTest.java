package it.unibz.vos.hibernate.testing;

import it.unibz.vos.hibernate.HibernateSession;

import org.junit.Test;

public class HibernateConnectionTest {

	/**
	 * Test the connection to the database, if it is possible
	 */
	@Test
	public void TestConnection() {
		HibernateSession.getInstance().openSession();
	}

}
