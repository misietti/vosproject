package it.unibz.vos.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletReqResContextContainer {
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	private ServletContext servletContext = null;
	
	public ServletReqResContextContainer(HttpServletRequest request, HttpServletResponse response,ServletContext servletContext){
		if (request == null) throw new NullPointerException("null request");
		if (response == null) throw new NullPointerException("null response");
		if (servletContext == null) throw new NullPointerException("null servletContext");
		this.request = request;
		this.response = response;
		this.servletContext = servletContext;
	}
	public HttpServletRequest getRequest()
	{
		return request;
	}
	
	public HttpServletResponse getResponse()
	{
		return response;
	}
	
	public ServletContext getServletContext()
	{
		return servletContext;
	}
}
