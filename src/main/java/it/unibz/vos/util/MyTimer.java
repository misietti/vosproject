package it.unibz.vos.util;

import it.unibz.vos.managementLayer.PollManager;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class MyTimer {

	/**
	 * Sets a timer to check whether there is some poll whose deadline is the current day.
	 */
	 public static void executeStartUpTasks() {
	 
	 Timer timer = new Timer();
	 Calendar startingTime = Calendar.getInstance(TimeZone.getDefault());
	 
	  // Starting at 0:02 AM in every day the Morning
	  startingTime.set(Calendar.HOUR_OF_DAY, 00);
	  startingTime.set(Calendar.MINUTE, 02);
	  startingTime.set(Calendar.SECOND, 00);
	 
	 
	 //Query the db to know when
	 
	  
	  timer.schedule(new TimerTask() {
		   public void run() {
		    System.out.println("Runs everday morning 0.02 AM");
		    PollManager.closePollsAutomatically();
		    
		   }
		  }, startingTime.getTime(), 1000 * 60 * 60 * 24);
		 }
	 
	 }

