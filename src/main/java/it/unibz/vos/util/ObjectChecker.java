package it.unibz.vos.util;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;

import org.hibernate.Session;

public class ObjectChecker {
	/**
	 * check the session, if it null, open, committed, rolledback,active
	 * @param session
	 */
	public static void checkSession(Session session){
		if (session == null) throw new NullPointerException("Cannot pass null session");
		if (!session.isOpen()) throw new IllegalArgumentException("the session must be open!");
		if (session.getTransaction().wasCommitted()) throw new IllegalArgumentException("the Transaction was committed!");
		if (session.getTransaction().wasRolledBack()) throw new  IllegalArgumentException("the Transaction was rolled back!");
		if (!session.getTransaction().isActive()) throw new IllegalArgumentException("the Transaction must be active!");
	}
/**
 * check the poll for null, not saved on database or deleted
 * @param session
 * @param poll
 */
	public static void checkPoll(Session session, Poll poll){
		if (poll == null) throw new NullPointerException("Cannot pass null poll");
		if (poll.getID() == null) throw new IllegalArgumentException("the Poll isn't saved on the database!");
		if (session.get(Poll.class, poll.getID()) == null)throw new IllegalArgumentException("the Poll has been deleted from the database");
		if (poll.isClosed()) throw new IllegalArgumentException("cannot pass a closed poll");
	}
	
	public static void checkPollallowClosed(Session session,Poll poll)
	{
		if (poll == null) throw new NullPointerException("Cannot pass null poll");
		if (poll.getID() == null) throw new IllegalArgumentException("the Poll isn't saved on the database!");
		if (session.get(Poll.class, poll.getID()) == null)throw new IllegalArgumentException("the Poll has been deleted from the database");
		
		
	}
	/**
	 * check the WUser for null, not saved or deleted
	 * @param session
	 * @param user
	 */
	public static void checkWUser(Session session, WUser user){
		if (user == null) throw new NullPointerException("Cannot pass null user");
		if (user.getID() == null) throw new IllegalArgumentException("the user isn't saved on the database!");
		if (session.get(WUser.class, user.getID()) == null)throw new IllegalArgumentException("the user has been deleted from the database");
	}
	/**
	 * check the option for null, not saved or deleted
	 * @param session
	 * @param user
	 */
	public static void checkOption(Session session, Option option){
		if (option == null) throw new NullPointerException("Cannot pass null option");
		if (option.getId() == null) throw new IllegalArgumentException("the option isn't saved on the database!");
		if (session.get(Option.class, option.getId()) == null)throw new IllegalArgumentException("the option has been deleted from the database");

	}
}
