package it.unibz.vos.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebPagesUtilities {
	public static void javaScriptPopup(HttpServletResponse response, String msg) throws IOException {
		
		PrintWriter out = response.getWriter();  
		response.setContentType("text/html");  
		out.println("<script type=\"text/javascript\">");  
		out.println("alert(' " +msg+ " ');");  
		out.println("</script>");
	}
	
	/**
	 * this method take the request and the response 
	 * and check is the user has logged in 
	 * if it has it reditcts the to the login page with an erro message
	 * 
	 * 
	 * @throws IOException if there is something wrong with the rd forward
	 * @throws ServletException there is a servlet exception 
	 * @returns true if it has been logged, false it isn't logged
	 */
	public static boolean redirectIfNotLogged(HttpServletResponse response, HttpServletRequest request,
			ServletContext context) throws ServletException, IOException{
		   boolean logged = CookiesManager.isUserLogged(request);
		    if (!logged) 
		    	{
		    
		    	RequestDispatcher rd = context.getRequestDispatcher("/Pages/login.jsp");
		    	String message = "Sorry, you have to login first!";
		    	request.setAttribute("errorLoginMessage", "<p id=\"errorForLogin\">" + message + "</p>");
		    	rd.include(request, response);
		    	return true;
		    //	response.sendRedirect(getServletContext().getResource("/Pages/login.jsp").);
		    	}
		    return false;
		
	}
	
	public static boolean redirectIfNotLogged(ServletReqResContextContainer servletcont) throws ServletException, IOException{
	return redirectIfNotLogged(servletcont.getResponse(),servletcont.getRequest(), servletcont.getServletContext());
		
	}

	public static void redirectToErrorSite(ServletReqResContextContainer servltContainer,String errorMessage) throws ServletException, IOException{
		redirectToErrorSite(servltContainer.getResponse(), servltContainer.getRequest(),
				servltContainer.getServletContext(),errorMessage);
		
	}
	
	public static void redirectToErrorSite(HttpServletResponse response, HttpServletRequest request,
			ServletContext context,String errorMessage) throws ServletException, IOException{
		RequestDispatcher rd = context.getRequestDispatcher("/Pages/errorPage.jsp");
		request.setAttribute("theError", errorMessage);	
	   	rd.forward(request, response);
	
}
	public static void redirectToPageFromRoot(ServletReqResContextContainer servltContainer,
			String string) throws ServletException, IOException {
		RequestDispatcher rd = servltContainer.getServletContext().getRequestDispatcher(string);
   
	    	
	   	rd.forward(servltContainer.getRequest(), servltContainer.getResponse());
		
	}
}
