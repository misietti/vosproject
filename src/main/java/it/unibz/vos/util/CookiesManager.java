package it.unibz.vos.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookiesManager {

	public static boolean isUserLogged(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();

		for(Cookie cookie : cookies){
		    if("name".equals(cookie.getName())){
		    	return true;
		    }
		}
		return false;
		
	}
	
	public static String geLoggedUserName(HttpServletRequest request)
	{
		Cookie[] cookies = request.getCookies();

		for(Cookie cookie : cookies){
			  if("name".equals(cookie.getName())){
			    	return cookie.getValue();
			    }
		}
		return null;
		
	}
}
