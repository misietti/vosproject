package it.unibz.vos.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;

import org.hibernate.Session;

public class ServletUtil {

	public static void closeRollBackSession(Session session) {
		session.getTransaction().rollback();
		session.close();
	}

	/**
	 * The name says it all
	 * 
	 * @param String
	 *            s
	 * @return
	 */
	public static boolean isNullOrEmpty(String s) {
		if (s == null)
			return true;
		if (s.trim().isEmpty())
			return true;
		return false;

	}

	/**
	 * The name says it all
	 * 
	 * @param s
	 * @return
	 */
	public static boolean isNullOrEmpty(String[] s) {
		if (s == null)
			return true;
		if (s.length == 0)
			return true;
		return false;
	}

	/**
	 * 
	 * @param parameterName
	 *            the name of the parameter to get
	 * @param request
	 *            the request of the Servlet
	 * @param response
	 *            the response of the Servlet
	 * @param servletContext
	 *            the context of the Servlet
	 * @param errorMessage
	 *            the message you want to show on the error page
	 * @return the parameter if it not null or empty, otherwise it returns null
	 * @throws ServletException
	 *             if the underlying redirection to error has problems
	 * @throws IOException
	 *             if the underlying redirection to error has problems
	 */
	public static String getParameterRedirectToErrorIfNullOrEmpty(
			String parameterName, ServletReqResContextContainer container,
			String errorMessage) throws ServletException, IOException {

		String pollIdString = container.getRequest()
				.getParameter(parameterName);

		if (isNullOrEmpty(pollIdString)) {
			WebPagesUtilities
					.redirectToErrorSite(container,
							"Plase use the botton in the homePage to make the request, (no pollid)");
		} else {
			return pollIdString;
		}
		return null;
	}
	
	/**
	 * 
	 * @param date the date you want to parse
	 * @return
	 */
	public static Date getDateFromString(String date){
		if (isNullOrEmpty(date)) throw new NullPointerException("empty or null date passed");
		String[] dmy = date.split("/");
		Calendar k = new GregorianCalendar();
		// first day
		k.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dmy[0]));
		// sercond month
		k.set(Calendar.MONTH, (Integer.parseInt(dmy[1]) - 1));
		// third year
		k.set(Calendar.YEAR, Integer.parseInt(dmy[2]));
		
		return k.getTime();

	}

}
