package it.unibz.vos.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class CheckListener
 *
 */
@WebListener
public class CheckListener implements ServletContextListener {


	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         
    	System.out.println("--- SERVLET CONTEXT DESTROYED ---");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
         
    	System.out.println("--- BEGIN SERVLET CONTEXT ---");
    	System.out.println("Executing startup operations");
    	MyTimer.executeStartUpTasks();
    }
	
}
