package it.unibz.vos.managementLayer;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidEmailException;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.managementLayer.exceptions.NoWUserFoundException;
import it.unibz.vos.util.ObjectChecker;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class WUserManager {
	   /**
	    *  This method obtains an object of type WUser from a string 
	    *  return null if there is no user with such an username
	    * @param username - username of the object WUser has to be searched in the db.
	    * @return WUser matching the username passed as parameter.
	    */
	  public static WUser getWUserFromUsername(Session session, String username) {
			ObjectChecker.checkSession(session);
		  if (username == null ) throw new NullPointerException("The username cannot be null");
		  
				  WUser wUser = null;
		  Query q = session.createQuery("from WUser wuser where wuser.username = :username");
	        q.setString("username", username);
	        @SuppressWarnings("unchecked")
			List<WUser> wuserList = q.list();
	       
	        //Returns null if no user is found!
	        
	        if ( wuserList.isEmpty()) {
	        	
	        	return null;
	        	
	        } else {
	        	
	        	wUser = wuserList.get(0);
	        }
	        
		  return wUser;
	  }
	  /**
	   * Inserts the passed user
	   * use only for the insert! Do not use for consequent saves!
	   * @param wuser
	   */
		public static void insertWUser(Session session,WUser wuser) {
			ObjectChecker.checkSession(session);
			if (wuser == null) throw new NullPointerException("cannot insert a null object");
			 if (wuser.getID() != null) throw new IllegalArgumentException("cannot use the method for saving users after the first save");
			  session.save(wuser);
			  session.flush();
		}
		/**
		 * return the user that is referenced by the passed id
		 * returns null if an user with such an id doesn't exists
		 * @param userId
		 * @return
		 */
		public static WUser getUser(Session session,Long userId){
			ObjectChecker.checkSession(session);
			if (userId == null) throw new NullPointerException("pollId cannot be null");
		
			  WUser toreturn = (WUser) session.get(WUser.class, userId);
			  return toreturn;
		}
		/**
		 * Change the pasword of the user
		 * @param wuser the user that wants to have the password changed
		 * @param password the string containgin the clean password
		 * @throws InvalidPassowrdException if the password is empty or null
		 */
		public static void changeWUserPassword(Session session,WUser wuser, String password) throws InvalidPassowrdException{
			ObjectChecker.checkSession(session);
			if (wuser == null) throw new NullPointerException("canot pass a null user");
			 if (wuser.getID() == null) throw new
			 IllegalArgumentException("cannot use the method on users that haven't been loaded from the database");

			
			  wuser.setPassword(password);
			  session.merge(wuser);
		}
		/**
		 * Change the mail of the wuser
		 * @param session 
		 * @param wuser the user that wants to have the mail changed
		 * @param newMail the new mail of the user
		 * @throws InvalidEmailException if email is invalid
		 */
 		public static void changeWUserMail(Session session, WUser wuser, String newMail) throws InvalidEmailException {
 			ObjectChecker.checkSession(session);
			if (wuser == null) throw new NullPointerException("canot pass a null user");
			 if (wuser.getID() == null) throw new
			 IllegalArgumentException("cannot use the method on users that haven't been loaded from the database");
			  wuser.setEmail(newMail);
			  session.merge(wuser);
		}
 		// TODO test this !
		public static WUser getWUserFromEmail(Session session, String email) throws NoWUserFoundException {
 			ObjectChecker.checkSession(session);
 			  if (email == null ) throw new NullPointerException("The email cannot be null");
 			  
			  WUser wUser = null;
	  Query q = session.createQuery("from WUser wuser where wuser.email = :email");
        q.setString("email", email);
        @SuppressWarnings("unchecked")
		List<WUser> wuserList = q.list();
       
        //Returns null if no user is found!
        
        if ( wuserList.isEmpty()) {
        	
        	throw new NoWUserFoundException("The user is not registered!");
        	
        } else {
        	
        	wUser = wuserList.get(0);
        }
        
	  return wUser;
			
		}
		
}
