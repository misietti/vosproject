package it.unibz.vos.managementLayer.exceptions;

public class NoWUserFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6887149684029883538L;

	public NoWUserFoundException() {
	
		super();
	}

	public NoWUserFoundException(String message) {
		super(message);
		
	}

	
}
