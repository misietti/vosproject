package it.unibz.vos.managementLayer.exceptions;

public class OptionAlreadyVotedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4297076064839197266L;

	public OptionAlreadyVotedException (String msg){
		super(msg);
	}
}
