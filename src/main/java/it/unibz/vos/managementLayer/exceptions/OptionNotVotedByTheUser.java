package it.unibz.vos.managementLayer.exceptions;

public class OptionNotVotedByTheUser extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4356653242688868029L;

	public OptionNotVotedByTheUser(String msg) {
		super(msg);
	}
}
