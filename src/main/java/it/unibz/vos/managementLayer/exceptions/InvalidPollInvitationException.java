package it.unibz.vos.managementLayer.exceptions;

public class InvalidPollInvitationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3941223935563896399L;

	public InvalidPollInvitationException(String msg) {
		super(msg);
	}
	
}
