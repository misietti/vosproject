package it.unibz.vos.managementLayer.exceptions;

public class CannotDeleteVotedOption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1186008242278996760L;

	public CannotDeleteVotedOption(String msg){
		super(msg);
	}
}
