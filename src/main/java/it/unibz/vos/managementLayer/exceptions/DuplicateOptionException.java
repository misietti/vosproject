package it.unibz.vos.managementLayer.exceptions;

public class DuplicateOptionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7322514384968529418L;

	
	public DuplicateOptionException() {
		
		super();
	}
	
	public DuplicateOptionException(String msg) {
		
		super(msg);
	}
}
