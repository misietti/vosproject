package it.unibz.vos.managementLayer.exceptions;

public class PollDoesNotContainInvitationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8207049485616603162L;

	public PollDoesNotContainInvitationException(String msg){
		super(msg);
	}
}
