package it.unibz.vos.managementLayer.exceptions;

public class TooManyVotesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 955531779544213165L;

	public TooManyVotesException (String msg){
		super(msg);
	}
}
