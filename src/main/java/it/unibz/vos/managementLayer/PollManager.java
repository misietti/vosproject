package it.unibz.vos.managementLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.exceptions.DuplicateOptionException;
import it.unibz.vos.managementLayer.exceptions.InvalidPollInvitationException;
import it.unibz.vos.managementLayer.exceptions.OptionAlreadyVotedException;
import it.unibz.vos.managementLayer.exceptions.OptionNotVotedByTheUser;
import it.unibz.vos.managementLayer.exceptions.PollDoesNotContainInvitationException;
import it.unibz.vos.managementLayer.exceptions.TooManyVotesException;
import it.unibz.vos.util.EmailSender;
import it.unibz.vos.util.ObjectChecker;

public class PollManager {
	/**
	 * Obtain the poll with the given id if there is no poll with such an id,
	 * then return a null item
	 * 
	 * @param pollId
	 *            the id of the poll
	 * @return the required poll
	 * 
	 * */

	public static Poll getPoll(Session session, Long pollId) {
		if (pollId == null)
			throw new NullPointerException("pollId cannot be null");
		Poll toreturn = (Poll) session.get(Poll.class, pollId);
		session.flush();
		return toreturn;
	}

	/**
	 * Insert the given poll
	 * 
	 * @param pollToInsert
	 */
	public static void insertPoll(Session session, Poll pollToInsert) {
		ObjectChecker.checkSession(session);
		if (pollToInsert == null)
			throw new NullPointerException("The poll to insert cannot be null");
		if (pollToInsert.getID() != null)
			throw new IllegalArgumentException(
					"The poll cannot be already inserted");
		session.save(pollToInsert);
		session.flush();
	}

	/**
	 * make the poll public
	 * 
	 * @param thePoll
	 */
	public static void makePollPublic(Session session, Poll thePoll) {
		// TODO this has to destroy all invites!!
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, thePoll);

		thePoll.makePublic();
		session.merge(thePoll);
		session.flush();
	}

	/**
	 * make the poll private
	 */
	public static void makePollPrivate(Session session, Poll thePoll) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, thePoll);

		thePoll.makePrivate();
		session.merge(thePoll);
		session.flush();
	}

	/**
	 * enable the possibility for the student to add Options
	 */
	public static void enableUserToAddOptions(Session session, Poll thePoll) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, thePoll);

		thePoll.enableUserCanAddOptions();
		session.merge(thePoll);
		session.flush();
	}

	/**
	 * disable the possibility for the student to add Options
	 */
	public static void disableeUserToAddOptions(Session session, Poll thePoll) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, thePoll);

		thePoll.disableUserCanAddOptions();
		session.merge(thePoll);
		session.flush();
	}

	/**
	 * This method queries the db to know all the polls which have been created
	 * by the given user.
	 * 
	 * @return ArrayList<Poll> created by the user.
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<Poll> getPersonalPolls(Session session, WUser user) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkWUser(session, user);
		Criteria cr = session.createCriteria(Poll.class);
		cr.add(Restrictions.eq("creator.id", user.getID()));
		cr.list();
		List<Poll> listOfPoll= cr.list();
		ArrayList <Poll> listOfPolls = new ArrayList<Poll>(new LinkedHashSet<Poll>(listOfPoll)); // remove any duplicates..


		return listOfPolls;

	}

	// TODO TEST THIS
	/**
	 * This method queries the db to know all the user can vote on
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<Poll> getVotablePolls(Session session, WUser user) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkWUser(session, user);
		// initialize returning list
		ArrayList<Poll> pollList = new ArrayList<Poll>();
		// get the list of non private Polls
		ArrayList<Poll> nonPrivateList = null;
		Criteria cr = session.createCriteria(Poll.class);
		cr.add(Restrictions.eq("isPrivate", false));
		// don't get the polls created by you
		cr.add(Restrictions.eq("isPrivate", false));
		cr.add(Restrictions.ne("creator.id", user.getID()));
		nonPrivateList = (ArrayList<Poll>) cr.list();
		for (Poll p : nonPrivateList) {
			pollList.add(p);
		}
		// get the list of polls the user has been invited to
		session.flush();
		Set<Poll> enabledList = user.getEnabledPolls();
		for (Poll p : enabledList) {
			pollList.add(p);
		}
		// remove the polls on whiche the user has already voted on
		pollList.removeIf(i -> isPollVotedByUser(session, i, user));
		session.flush();

		// remove the closed ones from the array

		for (Iterator<Poll> iterator = pollList.iterator(); iterator.hasNext();) {
			Poll i_poll = iterator.next();
			if (i_poll.isClosed()) {
				iterator.remove();
			}
		}
		
		ArrayList <Poll> pollListWithNoDuplicates = new ArrayList<Poll>(new LinkedHashSet<Poll>(pollList)); // remove any duplicates, just to be sure

		
		
		
		
		return pollListWithNoDuplicates;

	}

	private static boolean isPollVotedByUser(Session session, Poll poll,
			WUser user) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkWUser(session, user);
		ObjectChecker.checkPollallowClosed(session, poll);
		for (Option o : poll.getOptions()) {
			if (o.getUsersThatHaveVoted().contains(user))
				return true;
		}

		return false;
	}

	/**
	 * TODO : to be tested Get all the polls that the user has already voted.
	 * 
	 * @param wuser
	 *            - the wuser whose polls have to be queried.
	 * @return ArrayList<Poll> containing polls voted by the given user.
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<Poll> getVotedPolls(Session session, WUser wuser) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkWUser(session, wuser);
		ArrayList<Poll> pollList = new ArrayList<Poll>();

		// Get the list of all public polls
		ArrayList<Poll> nonPrivateList = null;
		Criteria cr = session.createCriteria(Poll.class);
		cr.add(Restrictions.eq("isPrivate", false));
		nonPrivateList = (ArrayList<Poll>) cr.list();

		for (Poll p : nonPrivateList) {

			if (isPollVotedByUser(session, p, wuser))
				pollList.add(p);
		}
		// Get the list of polls the user has been invited to
		Set<Poll> enabledList = wuser.getEnabledPolls();
		for (Poll p : enabledList) {

			if (isPollVotedByUser(session, p, wuser))
				pollList.add(p);
		}
		
		ArrayList <Poll> pollListWithNoDuplicates = new ArrayList<Poll>(new LinkedHashSet<Poll>(pollList)); // remove any duplicates, just to be sure

		return pollListWithNoDuplicates;

	}

	public static Option insertOptionIntoPoll(Session session, Poll thePoll,
			String optionText, WUser user) throws EmptyOptionTextException, DuplicateOptionException {
		ObjectChecker.checkSession(session);

		// can always insert a new option if you are the creator.
		if (thePoll.getCreator() != user) {
			if (thePoll.isPrivate()) {

			}
		}
		// the check of the the poll and the optionTExt is done in thee creation
		// of the option
		
		Set<Option> pollOptionsList = thePoll.getOptions();
		
			for (Option o : pollOptionsList) {
				
				if (o.getOptionText().compareTo(optionText) == 0){
					
					throw new DuplicateOptionException("Sorry! This option already exists in this poll!");
						
				}
				
			}

					Option option = new Option(optionText, thePoll);
					session.save(option);
					pollOptionsList.add(option);
					session.merge(user);
					session.merge(thePoll);
					session.flush();

		return option;

	}

	// TestThis
	public static Set<Option> getOptionsOfaPoll(Poll thePoll) {

		Session session = HibernateSession.getInstance().openSession();
		session.beginTransaction();
		Set<Option> setToReturn = thePoll.getOptions();
		session.close();
		return setToReturn;
	}

	// TODO test this completely
	public static void InviteUserToPoll(Session hsession, Poll poll, WUser user)
			throws InvalidPollInvitationException {
		ObjectChecker.checkSession(hsession);
		ObjectChecker.checkPoll(hsession, poll);
		ObjectChecker.checkWUser(hsession, user);

		if (!poll.isPrivate())
			throw new InvalidPollInvitationException(
					"Cannot invite users into public polls");

		if (poll.getCreator().equals(user))
			throw new IllegalArgumentException(
					"Cannot invite the creator of the poll to vote ");
		poll.getWuserAllowedToVote().add(user);
		user.getEnabledPolls().add(poll);
		EmailSender.sendEmail(user, "The user " + user.getUsername() + "invited you to answer the poll" +
		poll.getQuestion(), "Participate to the poll!");
		hsession.merge(poll);
		hsession.merge(user);
		hsession.flush();

	}

	// TODO test this completely
	public static void removeInviteFromPoll(Session session, Poll poll,
			WUser user) throws InvalidPollInvitationException,
			PollDoesNotContainInvitationException {
		if (!poll.isPrivate())
			throw new InvalidPollInvitationException(
					"The poll has no invites, as it is public");
		if (poll.getID() == null)
			throw new IllegalArgumentException(
					"Cannot work to a poll that is not saved in the database ");
		if (user.getID() == null)
			throw new IllegalArgumentException(
					"Cannot work a user non in the database");

		if (poll.getWuserAllowedToVote().contains(user)) {
			poll.getWuserAllowedToVote().remove(user);
		} else {
			throw new PollDoesNotContainInvitationException(
					"the poll doesn't contain the invitation");
		}
		session.flush();
	}

	// TODO test
	public static void addVote(Session session, Option option, WUser user)
			throws OptionAlreadyVotedException, TooManyVotesException {
		// check for irregularities
		ObjectChecker.checkSession(session);
		ObjectChecker.checkOption(session, option);
		ObjectChecker.checkWUser(session, user);
		// check if the option hasn't been already voted by the user on the poll
		if (user.getOptionVoted().contains(option))
			throw new OptionAlreadyVotedException(
					"The user has already voted this poll");
		// check if the user already reached the maximum allowed votes on the
		// option
		int countOfVotesOnPoll = getOptionVotedByUSerOnPoll(session,
				option.getPoll(), user).size();
		if (countOfVotesOnPoll > option.getPoll().getMaxPreferences())
			throw new TooManyVotesException(
					"The user cannot add another vote as he already has made too many votes");

		option.getUsersThatHaveVoted().add(user);
		user.getOptionVoted().add(option);
		session.merge(option);
		session.merge(user);
		session.merge(option.getPoll());
		//session.flush();
	}

	public static Set<Option> getOptionVotedByUSerOnPoll(Session session,
			Poll poll, WUser user) {

		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, poll);
		ObjectChecker.checkWUser(session, user);

		Set<Option> optionsVoted = new HashSet<Option>();

		Set<Option> optionsOfPoll = poll.getOptions();
		for (Option o : optionsOfPoll) {
			if (o.getUsersThatHaveVoted().contains(user)) {
				optionsVoted.add(o);
			}
		}

		return optionsVoted;

	}

	// TODO test this
	public static void removeVote(Session session, Option option, WUser user)
			throws OptionNotVotedByTheUser {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkOption(session, option);
		ObjectChecker.checkWUser(session, user);

		if (!user.getOptionVoted().contains(option))
			throw new OptionNotVotedByTheUser(
					"Option isn't voted by the passed user");

		user.getOptionVoted().remove(option);
		option.getUsersThatHaveVoted().remove(user);

		session.merge(option);
		session.merge(user);
		session.flush();

	}

	public static void closePoll(Session session, Poll thePoll) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, thePoll);

		// close the poll
		thePoll.closePoll();
		notifyUsers(session,thePoll);
		session.merge(thePoll);
		session.flush();
	}

	public static Map<String, Integer> getResults(Session session, Poll thePoll) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPollallowClosed(session, thePoll);
		if (thePoll.getOptions().isEmpty())
			throw new IllegalArgumentException("The poll has no option");
		Map<String, Integer> map = new HashMap<String, Integer>();

		for (Option option : thePoll.getOptions()) {
			map.put(option.getOptionText(), option.getUsersThatHaveVoted()
					.size());
		}

		return map;
	}

	// TODO test this one :D
	public static int getNumberOfUsersThatVoted(Session session, Poll poll) {
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPollallowClosed(session, poll);
		List<WUser> usersThatHaveVotedForAllOptions = new ArrayList<WUser>();
		for (Option o : poll.getOptions()) {
			for (WUser user : o.getUsersThatHaveVoted()) {
				if (!usersThatHaveVotedForAllOptions.contains(user)) {
					usersThatHaveVotedForAllOptions.add(user);
				}
			}

		}

		return usersThatHaveVotedForAllOptions.size();
	}

	/**
	 * This method checks in the database if an open poll already exists with
	 * the given question.
	 * 
	 * @return
	 */
	public static boolean isPollAlreadyPresent(Session session, String question) {

		boolean found = false;

		Query query = session.createQuery("SELECT COUNT(*) FROM Poll WHERE question=:thequestion"
				+ " AND isClosed=:boolean");
		query.setParameter("thequestion", question);
		query.setParameter("boolean", false);
		@SuppressWarnings("rawtypes")
		List results = query.list();
		long count = (long) results.get(0);

		if (count > 0) {

			found = true;
		}

		return found;

	}
	
	/**
	 * This method checks whether there are users to be notified on the poll closure
	 * @param session
	 * @param thePoll
	 */
	public static boolean notifyUsers(Session session, Poll thePoll) {
		//TODO test this
		String hqlQuery = "SELECT willNotify from Poll where ID=:id";
		Query query = session.createQuery(hqlQuery);
		query.setParameter("id", thePoll.getID());
		List<?> list = query.list();
		boolean shouldNotify = (boolean) list.get(0);
		boolean notified = false;
		List<WUser> wuserList= getUsersThatVotedPoll(thePoll);
		System.out.println("Size of users: " + wuserList.size());
		
		if (shouldNotify) {
			
			if (wuserList.size() > 0) {
				
				
				for (WUser user : wuserList) {
					
					EmailSender.sendEmail(user, "The poll " + "\"" + thePoll.getQuestion() + "\"" +" has been closed! Final results"
							+ " are available!", "Poll closed notification!");
					notified = true;
				}
				
				
			}
			
			
		}
		
		return notified;
		
	}


	/**
	 * This method gets all the users that voted a specific poll.
	 * @param poll
	 * @return
	 */
	public static List<WUser> getUsersThatVotedPoll(Poll poll) {
		
		List<WUser> usersThatHaveVotedForAllOptions = new ArrayList<WUser>();
		for (Option o : poll.getOptions()) {
			for (WUser user : o.getUsersThatHaveVoted()) {
				if (!usersThatHaveVotedForAllOptions.contains(user)) {
					usersThatHaveVotedForAllOptions.add(user);
				}
			}

		}
		
		return usersThatHaveVotedForAllOptions;
	}
	
	/**
	 * This method closes all the polls that have reached the deadline
	 */
	public static void closePollsAutomatically() {
		Session session = HibernateSession.getInstance().openSession();
		session.getTransaction().begin();
		
		
		Date today = new Date();
		Query query = session.createQuery("SELECT ID from Poll where deadlineDate=:date");
		query.setParameter("date", today);
		@SuppressWarnings("unchecked")
		List<Long> results = query.list();
		
		if (results.size() > 0) {
		
			for (long id : results)  {
			
				Poll poll = (Poll) session.get(Poll.class, id);
				PollManager.closePoll(session, poll);
				System.out.println("POLL "+ poll.getQuestion() + "CLOSED");
			
			}	
		
		}
		session.getTransaction().commit();
		session.close();
		
	}
	
	
	public static void enableNotificationInPoll(Session session, Poll poll){
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, poll);
		
		poll.doNotifyUsersAtClosing();
	}
	
	public static void disableNotificationInPoll(Session session, Poll poll){
		ObjectChecker.checkSession(session);
		ObjectChecker.checkPoll(session, poll);
		
		poll.doNotNotifyUsersAtClosing();
	}
	
}
