package it.unibz.vos.managementLayer.testing;

import static org.junit.Assert.*;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidEmailException;
import it.unibz.vos.dbclasses.exceptions.InvalidNameException;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.InvalidSurnameException;
import it.unibz.vos.dbclasses.exceptions.InvalidUserNameException;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.dbclasses.exceptions.UsernameAlreadyTakenException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.managementLayer.exceptions.NoWUserFoundException;
import it.unibz.vos.testing.util.TestingUtilities;
import it.unibz.vos.util.Hasher;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WUserManagerTests {

	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Session testSession = null;
	
	Long userid = null;
	Long pollid = null;
	
	
// before after and other util methods
	@Before
	public void mountUP() throws PollAlreadyExistsException, InvalidPreferenceNumber{

		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		
		
		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		userNotSavedInDatabse = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,testingUser);
		
		userid = testingUser.getID();
		pollid = testingPoll.getID();
		
		
		pollNotSavedOnDatabase = TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession,testingUser);

	}


	@After
	public void breakDown(){

		TestingUtilities.deleteALlTestGarbage(testSession);
		testSession.getTransaction().commit();
		testSession.close();
	}

// actual test
	// InsertWUser Testing
	
	/**
	 * Test the insertion of an user
	 */
	@Test
	public void TestInsertionOfAnUser() {

		Long userId = new Long(-1);

		String email = "Some@m.ail";
		String name = "gino";
		String password = "there is this password";
		String surname = "datsur";
		String username = "anusername";
		WUser user = null;
		try {
			user = new WUser(testSession,username, name, surname, password, email);
		} catch (UsernameAlreadyTakenException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidUserNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidSurnameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidPassowrdException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidEmailException e2) {
			Assert.fail(e2.getMessage());
		}

		WUserManager.insertWUser(testSession,user);
		userId = user.getID();

		// user should be inserted so far

		WUser user2 = (WUser) testSession.get(WUser.class, userId);
		Assert.assertNotEquals(null, user2); // shouldn't be null as it was inserted...
		// check if every thing is the same
		assertEquals(user2.getEmail(), email);
		assertEquals(user2.getID(), userId);
		assertEquals(user2.getName(), name);
		assertEquals(user2.getPassword(), Hasher.getHashedString(password));
		assertEquals(user2.getSurname(), surname);
		assertEquals(user2.getUsername(), username);

		// delete the user from the database to be sure that there will be
		// nothing to break the other tests

		

		TestingUtilities.deleteUser(testSession,user2);
		user2 = (WUser) testSession.get(WUser.class, userId);
		// should be null
		if (user2 != null)
			Assert.fail("The user shouldn't exist anymore");


	}
	/**
	 * Test the Insertion Of an user
	 */
	
	@Test
	public void TestUnsertionOfAnUserThatHasbeenAlreadyInserted() {


		String email = "Some@m.ail";
		String name = "gino";
		String password = "there is this password";
		String surname = "datsur";
		String username = "anusername";
		WUser user = null;
		try {
			user = new WUser(testSession,username, name, surname, password, email);
		} catch (UsernameAlreadyTakenException e2) {
			// These exception should not happen, leaving the print stack trace
			Assert.fail(e2.getMessage());
		} catch (InvalidUserNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidNameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidSurnameException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidPassowrdException e2) {
			Assert.fail(e2.getMessage());
		} catch (InvalidEmailException e2) {
			Assert.fail(e2.getMessage());
		}

		WUserManager.insertWUser(testSession,user);
		try {
			WUserManager.insertWUser(testSession,user);
			Assert.fail("You cannot insert the same user twice....");
		} catch (IllegalArgumentException e) {
			// ok everything went fine
		}


		TestingUtilities.deleteUser(testSession,user);
	}

	@Test (expected=NullPointerException.class )
	public void TestNulInsertion() {
			WUserManager.insertWUser(testSession,null);
			Assert.fail("There should have been a nullPointerException");
	
	}
	
	// Test Obtain User with username
	
	
	/**
	 * 
	 * test the obtainng of a user from a valid username
	 */

	@Test
	public void TestObtaingingUserFromUsername() {


		WUser anotherUser = WUserManager.getWUserFromUsername(testSession,testingUser
				.getUsername());
		assertEquals(testingUser, anotherUser);

	}

	/**
	 * Test the obtating of an user from an non-existant username
	 */

	@Test 
	public void TestObtaingingUserFromUsernameWithnonExistingUsername() {
		WUser anotherUser = WUserManager
				.getWUserFromUsername(testSession,"someReservedUSernameFUNFUN");
		assertEquals(anotherUser, null);
	}

	/**
	 * Test the obtainbging of a
	 */
	@Test (expected=NullPointerException.class )
	public void TestObrtainingTheUSerWithNullUsername() {
	WUserManager.getWUserFromUsername(testSession,null);
		Assert.fail("Tehre should have been a nullpointerException");
	
	
	
	}
	
	// Test getting of user with a id 
	
	/**
	 * get the existing user
	 */
	@Test
	public void TestGettingOfTheuser() {

		WUser anotherUser = WUserManager.getUser(testSession,testingUser.getID());
		assertEquals(testingUser, anotherUser);

	}


	@Test
	public void TestGettingOfTheuserWithinexistentId() {
		WUser anotherUser = WUserManager.getUser(testSession,new Long(-1));
		assertEquals(anotherUser, null);

	}
/**
 * Test the getting of an user with a null id
 */
	@Test (expected=NullPointerException.class )
	public void TestGettingUserWithANullId(){
	
		WUserManager.getUser(testSession,null);
		Assert.fail("This should throw a nullPointerException");
	
	}


// Test the changing of the password : 
	@Test (expected=NullPointerException.class )
  public void testChangePasswordUserNull() throws InvalidPassowrdException{

		WUserManager.changeWUserPassword(testSession,null, "a new password");
		Assert.fail("there should have been a nullpointerException");

	  
  }
  
  
  @Test
  public void testChangePasswordNormal() throws InvalidPassowrdException{
	  
	 String newpass = "afdsajfkdlsajfklsdajklfjdddfadsfdsaf";

		WUserManager.changeWUserPassword(testSession,testingUser, newpass);


	  assertEquals(testingUser.getPassword(),Hasher.getHashedString(newpass));
	  	  
	  
  }
  
	@Test (expected=IllegalArgumentException.class )
  public void testchangeUserNOtSavedOnTheDatabase() throws InvalidPassowrdException{
	  WUser user = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
	  
	 
		WUserManager.changeWUserPassword(testSession,user, "frank");
		Assert.fail("Should have been a illegal argument exception");

  }
	
	@Test (expected=InvalidPassowrdException.class )
	public void testChangePasswordWithNullPassword() throws InvalidPassowrdException{
		
	
			WUserManager.changeWUserPassword(testSession,testingUser, null);
			Assert.fail("Excpected a InvalidPassowrdException");

		
	}
	
	@Test (expected=InvalidPassowrdException.class )
	public void testChangePasswordWithEmptyPassword() throws InvalidPassowrdException{

			WUserManager.changeWUserPassword(testSession,testingUser, "");
			Assert.fail("Excpected a InvalidPassowrdException");

		
	}
	// Test the changing of the e-amil, since the email checker is already tested
	// there isn't a ntted to test it again
	@Test (expected=NullPointerException.class )
	public void testEmailChangeWiuthNullUser() throws InvalidEmailException{
		
	
			WUserManager.changeWUserMail(testSession,null, "fdsafas@fdsafas.it");
			Assert.fail("expected exception");

		
		
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testEmailChangeWithlUserNotSavedInDatabase() throws InvalidEmailException{
		
		
		WUser wuser = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
		
			WUserManager.changeWUserMail(testSession,wuser, "fdsafas@fdsafas.it");
			Assert.fail("expected exception");
	
		
		
	}
	@Test
	public void testEmailChangeWithNullMail(){
		
		

		try {
			WUserManager.changeWUserMail(testSession,testingUser, null);
			Assert.fail("expected exception");
		} catch (InvalidEmailException e) {
		//Expected
		} catch(IllegalArgumentException e)
		{
			Assert.fail(e.getMessage());
			
		}
		
	}

	@Test (expected=InvalidEmailException.class)
	public void testEmailChangeWithEmptymail() throws InvalidEmailException{
			WUserManager.changeWUserMail(testSession,testingUser, "");
	}
	
	@Test
	public void testEmailChangeWithNormalUser() throws InvalidEmailException{
		
			WUserManager.changeWUserMail(testSession,testingUser, "fdasxxxfdas@fdsafdsa.com");
	
	}

	@Test
	public void getWuserWithEmailTestExisting() throws NoWUserFoundException
	{
		WUser x = WUserManager.getWUserFromEmail(testSession, testingUser.getEmail());
		assertEquals(testingUser,x);
		
	}

	@Test (expected = NoWUserFoundException.class)
	public void getWuserWithEmailTestNonExisting() throws NoWUserFoundException
	{
		WUser x = WUserManager.getWUserFromEmail(testSession, "x@x.df");
		assertEquals(null,x);
		
	}
	
	@Test (expected=NullPointerException.class)
	public void getWuserWithEmailTestNullMail() throws NoWUserFoundException
	{
		WUserManager.getWUserFromEmail(testSession,null);
	}
}
