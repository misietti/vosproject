package it.unibz.vos.managementLayer.testing;

import static org.junit.Assert.*;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.LoginManager;
import it.unibz.vos.servelts.exceptions.InvalidLoginException;
import it.unibz.vos.testing.util.TestingUtilities;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class LoginMangerTests {

	
	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Session testSession = null;
	
    Long userid = null;
    Long pollid = null;
	
	
// before after and other util methods
	@Before
	public void mountUP() throws PollAlreadyExistsException, InvalidPreferenceNumber{
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
	
		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		userNotSavedInDatabse = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,testingUser);
		
		userid = testingUser.getID();
		pollid = testingPoll.getID();
	
		
		pollNotSavedOnDatabase = TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession,testingUser);

	}


	@After
	public void breakDown(){
		
		TestingUtilities.deleteALlTestGarbage(testSession);
		testSession.getTransaction().commit();
		testSession.close();
	}
	
	@Test
	public void testLogin() throws InvalidLoginException{
		@SuppressWarnings("unused")
		LoginManager x = new LoginManager();
		boolean valid = LoginManager.isValidLogin(testingUser, "there is this password");
	    assertEquals(true,valid);
	}
	
	@Test (expected=InvalidLoginException.class)
	public void testLoginNullUser() throws InvalidLoginException
	{
		LoginManager.isValidLogin(null, "there is this password");
	}
	
	@Test 
	public void testLoginWrongPassword() throws InvalidLoginException
	{
		boolean valid =  LoginManager.isValidLogin(testingUser, "");
		   assertEquals(false,valid);
	}
	
	@Test (expected=InvalidLoginException.class)
	public void testLoginInvalidPassword() throws InvalidLoginException
	{
		LoginManager.isValidLogin(testingUser, null);
	}
	
}
