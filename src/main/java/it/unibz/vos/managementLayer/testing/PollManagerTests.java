package it.unibz.vos.managementLayer.testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.exceptions.DuplicateOptionException;
import it.unibz.vos.managementLayer.exceptions.InvalidPollInvitationException;
import it.unibz.vos.managementLayer.exceptions.OptionAlreadyVotedException;
import it.unibz.vos.managementLayer.exceptions.OptionNotVotedByTheUser;
import it.unibz.vos.managementLayer.exceptions.PollDoesNotContainInvitationException;
import it.unibz.vos.managementLayer.exceptions.TooManyVotesException;
import it.unibz.vos.testing.util.TestingUtilities;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PollManagerTests {
	
	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Session testSession = null;
	
    Long userid = null;
    Long pollid = null;
	
	
// before after and other util methods
	@Before
	public void mountUP() throws PollAlreadyExistsException, InvalidPreferenceNumber{
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
	
		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		userNotSavedInDatabse = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,testingUser);
		
		userid = testingUser.getID();
		pollid = testingPoll.getID();
	
		
		pollNotSavedOnDatabase = TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession,testingUser);

	}


	@After
	public void breakDown(){
		
		TestingUtilities.deleteALlTestGarbage(testSession);
		testSession.getTransaction().commit();
		testSession.close();
	}
// Actual tests
	
	
	// Test the insertion of the polls
	/**
	 * test the simple insertion of polls in the database
	 * the check of the possible value as parameters is done 
	 * at the creation of the object of type poll
	 */
	@Test
	public void testTheInsertionOfPolls() { // working normally

		assertEquals(null,pollNotSavedOnDatabase.getID());
	PollManager.insertPoll(testSession,pollNotSavedOnDatabase);
	
	Assert.assertNotEquals(null, pollNotSavedOnDatabase.getID());
	TestingUtilities.deletePoll(testSession,pollNotSavedOnDatabase);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testTheInsertionOfAnAlreadyInsertedPoll() { 
		
		PollManager.insertPoll(testSession,testingPoll);
		
	}
	
	@Test(expected=NullPointerException.class)
	public void testTheinsertionOfAnNullPoll(){
		PollManager.insertPoll(testSession,null);
	}
	
	// Test the getting of the poll.
	
	@Test
	public void testWorkinggetPoll(){
		assertEquals(PollManager.getPoll(testSession,pollid), testingPoll);
	}
	
	@Test
	public void testNotworkingGetPoll(){
		assertEquals(PollManager.getPoll(testSession,new Long(-1)), null);
	}
	//TODO add to getPoll the check on the session! Didn't do it because aurelia is working on it right now
	@Test (expected=NullPointerException.class)
	public void getNullPoll()
	{
		PollManager.getPoll(testSession,null);
	}
	// Test the making of a poll public
	@Test
	public void testMakePollIOnDatabasePublic(){
		PollManager.makePollPublic(testSession,testingPoll);
		assertEquals(false,testingPoll.isPrivate());
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMakePollNotOnDatabasePublic(){
		PollManager.makePollPublic(testSession,pollNotSavedOnDatabase);
	}
	
	
	@Test (expected=NullPointerException.class)
	public void testMakePollNotOnDatabasePublicNull(){
		PollManager.makePollPublic(testSession,null);
	}
	// Test the making of a poll private
	@Test
	public void testMakePollIOnDatabasePrivate(){
		PollManager.makePollPrivate(testSession,testingPoll);
		assertEquals(true,testingPoll.isPrivate());
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMakePollNotOnDatabasePrivate(){
		PollManager.makePollPrivate(testSession,pollNotSavedOnDatabase);
	
	}
	@Test (expected=NullPointerException.class)
	public void testMakePollIPrivateNULL(){
		PollManager.makePollPrivate(testSession,null);
	}
	
	
	// Test the possibility of obtaining all the polls created by one user
	@Test (expected=NullPointerException.class)
	public void testObtainingpollsFromANullUSer(){
		PollManager.getPersonalPolls(testSession,null);
	
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testObtainingpollsFromANotSavedUser(){
		PollManager.getPersonalPolls(testSession,userNotSavedInDatabse);
	
	}
	
	@Test 
	public void testObtainPollsFromSAveduserwithonepoll(){
		List<Poll> thePollList = PollManager.getPersonalPolls(testSession,this.testingUser);
		assertEquals(thePollList.contains(this.testingPoll),true);
	}
	
	@Test
	public void testObtainPollsFromSAveduserwithtwoPolls() throws PollAlreadyExistsException, InvalidPreferenceNumber {
		Poll anotherTestingPoll = TestingUtilities.insertPollForTesting(testSession,testingUser,"memememeeiuhgfaslkhfgjlksda h");

		List<Poll> thePollList = PollManager.getPersonalPolls(testSession,this.testingUser);
		
		assertEquals(true,thePollList.contains(this.testingPoll));
		assertEquals(true,thePollList.contains(anotherTestingPoll));
	}
	
	// test enable user to add options on a poll
	@Test (expected=NullPointerException.class)
	public void testEnableUserToAddOptionsOnNullPoll(){
		PollManager.enableUserToAddOptions(testSession,null);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testEnableUserToAddOptionsOnPollnotSAvedOnDatabse(){
		PollManager.enableUserToAddOptions(testSession,pollNotSavedOnDatabase);
	}
	
	@Test 
	public void testEnableUSerToaddPollsNormal(){
		PollManager.enableUserToAddOptions(testSession,testingPoll);
		assertEquals(true,testingPoll.getCanUserAddOption());
	}
	// test disable user from adding option on a poll
	@Test
	public void testDisableUserFromAddingOptions(){
		PollManager.disableeUserToAddOptions(testSession,testingPoll);
		assertEquals(false,testingPoll.getCanUserAddOption());
	}
	
	@Test (expected=NullPointerException.class)
	public void testDisableUserFromAddingOptionsWithNullPoll()
	{
		PollManager.disableeUserToAddOptions(testSession,null);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testDisableUserFromAddingOptionsWitnotsavedPolll()
	{
		PollManager.disableeUserToAddOptions(testSession,pollNotSavedOnDatabase);
	}
	
	// test the insertion of Options in a poll
	@Test
	public void testTheInsertionOfaOptionInAPoll() throws EmptyOptionTextException, DuplicateOptionException{

		Option ooo = 	PollManager.insertOptionIntoPoll(testSession,testingPoll, "PollText",testingUser);
		TestingUtilities.addOptionToDelete(ooo);
		assertEquals(1,testingPoll.getOptions().size()) ;
		for (Option o : testingPoll.getOptions())
		{
			o.getOptionText().compareTo("PollText");
		}
		
	}
	
	
	@Test (expected=DuplicateOptionException.class)
	public void testDuplicateOptionInsertion() throws EmptyOptionTextException, DuplicateOptionException {
		
		Option option = PollManager.insertOptionIntoPoll(testSession,testingPoll, "PollText2",testingUser);
		TestingUtilities.addOptionToDelete(option);
		PollManager.insertOptionIntoPoll(testSession,testingPoll, "PollText2",testingUser);
	}
	
	@Test(expected=NullPointerException.class)
	public void testInsertOfNullPoll() throws EmptyOptionTextException, DuplicateOptionException{
		@SuppressWarnings("unused")
		Option ooo = 	PollManager.insertOptionIntoPoll(testSession,null, "PollText",testingUser);
	}
	
	@Test(expected=NullPointerException.class)
	public void testInsertionOfNullOptionText() throws EmptyOptionTextException, DuplicateOptionException{
	PollManager.insertOptionIntoPoll(testSession,testingPoll, null,testingUser);
	}
	
	@Test(expected=EmptyOptionTextException.class)
	public void testInsertionOfEmptyOptionText() throws EmptyOptionTextException, DuplicateOptionException {
		PollManager.insertOptionIntoPoll(testSession,testingPoll, "",testingUser);
	}
	
	// test the invitation of An USer
	@Test
	public void TesttheInvitationOfAnUser() throws InvalidPollInvitationException, PollDoesNotContainInvitationException
	{
		WUser anotheruser = TestingUtilities.insertWUserForTesting(testSession,"seconduyeser", "and_a_mail");
		PollManager.makePollPrivate(testSession,testingPoll);
		PollManager.InviteUserToPoll(testSession,testingPoll, anotheruser);
		
		assertEquals(true,testingPoll.getWuserAllowedToVote().contains(anotheruser));
		
		PollManager.removeInviteFromPoll(testSession,testingPoll, anotheruser);
		assertEquals(false,testingPoll.getWuserAllowedToVote().contains(anotheruser));

	}
	// test the addition of a vote
	@Test
	public void TestTheAdditionOfAVote() throws OptionAlreadyVotedException, TooManyVotesException, EmptyOptionTextException, OptionNotVotedByTheUser, DuplicateOptionException{
		WUser anotherUser = TestingUtilities.insertWUserForTesting(testSession,"seconduyeser", "and_a_mail");
		Option op = PollManager.insertOptionIntoPoll(testSession,testingPoll, "A Vote",testingUser);
		PollManager.addVote(testSession, op, anotherUser);
		assertEquals(1,PollManager.getOptionVotedByUSerOnPoll(testSession,testingPoll, anotherUser).size());
		PollManager.removeVote(testSession, op, anotherUser);
		assertEquals(0,PollManager.getOptionVotedByUSerOnPoll(testSession,testingPoll, anotherUser).size());
		
		TestingUtilities.deleteOption(testSession, op);
		
	}
	
	
	// test the removal of votes
	
	
	
	// testing of the view of the polls that are votable by the user.
	@Test
	public void viewPollsThatarePubliAndNotCreatedByYourselfAdndNotVoted() throws EmptyOptionTextException, OptionAlreadyVotedException, TooManyVotesException, OptionNotVotedByTheUser, PollAlreadyExistsException, DuplicateOptionException, InvalidPreferenceNumber{
		WUser anotheruser = TestingUtilities.insertWUserForTesting(testSession,"seconduyeser", "and_a_mail");
		Poll poll1 = TestingUtilities.insertPollForTesting(testSession,anotheruser,"genaus");
		Poll poll2 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"genauso");
		ArrayList<Poll> listOfVotablePolls = PollManager.getVotablePolls(testSession,testingUser);

		
		
		assertEquals(true,listOfVotablePolls.contains(poll1));
		assertEquals(true,listOfVotablePolls.contains(poll2));
		assertEquals(false,listOfVotablePolls.contains(this.testingPoll));
		
		Option o = PollManager.insertOptionIntoPoll(testSession, poll2, "There is an option", anotheruser);
		TestingUtilities.addOptionToDelete(o);
		PollManager.addVote(testSession, o, testingUser);
		
		// shoudln't contain it anymore.
		listOfVotablePolls = PollManager.getVotablePolls(testSession,testingUser);
		assertEquals(true,listOfVotablePolls.contains(poll1));
		assertEquals(false,listOfVotablePolls.contains(poll2));
		assertEquals(false,listOfVotablePolls.contains(this.testingPoll));
		
		PollManager.removeVote(testSession, o, testingUser);
	}
	@Test
	public void testNoDuplicatesInVotablePolls() throws EmptyOptionTextException, OptionAlreadyVotedException, TooManyVotesException, OptionNotVotedByTheUser, PollAlreadyExistsException, DuplicateOptionException, InvalidPreferenceNumber{
		WUser anotheruser = TestingUtilities.insertWUserForTesting(testSession,"seconduyeser", "and_a_mail");
		Poll poll1 = TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafsdajshgg");
		Poll poll2 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafsdas3sjhgg");
		Poll poll3 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafsdddddajhgg");
		Poll poll4 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafsdddddddajhgg");
		Poll poll5 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafsdf3fffffajhgg");
		Poll poll6 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsaffdsasdajhgg");
		Poll poll7 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsavcxbvcxfsdajhgg");
		Poll poll8 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafstretertdajhgg");
		Poll poll9 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafsdgfdsgfdajhgg");
		Poll poll10 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsafsda444jhgg");
		Poll poll11 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"xdsaf3sdajhgg");
		ArrayList<Poll> listOfVotablePolls = PollManager.getVotablePolls(testSession,testingUser);

		
		
		assertEquals(true,listOfVotablePolls.contains(poll1));
		assertEquals(true,listOfVotablePolls.contains(poll2));
		assertEquals(true,listOfVotablePolls.contains(poll3));
		assertEquals(true,listOfVotablePolls.contains(poll4));
		assertEquals(true,listOfVotablePolls.contains(poll5));
		assertEquals(true,listOfVotablePolls.contains(poll6));
		assertEquals(true,listOfVotablePolls.contains(poll7));
		assertEquals(true,listOfVotablePolls.contains(poll8));
		assertEquals(true,listOfVotablePolls.contains(poll9));
		assertEquals(true,listOfVotablePolls.contains(poll10));
		assertEquals(true,listOfVotablePolls.contains(poll11));
		assertEquals(false,listOfVotablePolls.contains(this.testingPoll));
		Set<Poll> set = new HashSet<Poll>(listOfVotablePolls);

		if(set.size() < listOfVotablePolls.size()){
		    Assert.fail("There are duplicates, there shouldn't be!");
		}
		Option o = PollManager.insertOptionIntoPoll(testSession, poll2, "There is an option", anotheruser);
		TestingUtilities.addOptionToDelete(o);
		PollManager.addVote(testSession, o, testingUser);
		
		// shoudln't contain it anymore.
		listOfVotablePolls = PollManager.getVotablePolls(testSession,testingUser);
		assertEquals(true,listOfVotablePolls.contains(poll1));
		assertEquals(false,listOfVotablePolls.contains(poll2));
		assertEquals(false,listOfVotablePolls.contains(this.testingPoll));
		
		PollManager.removeVote(testSession, o, testingUser);
	}
	
	
	
	/*xxxx
	 * public void testNoDuplicatesInGetPersonalPolls(){
		
	}
	
	@Test
	public void testNoDuplicatesInVotedPolls(){
		
	}*/
	
	// 
	@Test
	public void ViewPollsthatareclosedforVoting() throws PollAlreadyExistsException, InvalidPreferenceNumber{
		WUser anotheruser = TestingUtilities.insertWUserForTesting(testSession,"seconduyeser", "and_a_mail");
		Poll poll1 = TestingUtilities.insertPollForTesting(testSession,anotheruser,"fdsafsdafdsa");
		Poll poll2 =TestingUtilities.insertPollForTesting(testSession,anotheruser,"fdsafdsafdsa cxvz");
		PollManager.closePoll(testSession, poll1); // close poll1
		ArrayList<Poll> listOfVotablePolls = PollManager.getVotablePolls(testSession,testingUser);
		assertEquals(false,listOfVotablePolls.contains(poll1)); // poll1 is not expected to be votable.
		assertEquals(true,listOfVotablePolls.contains(poll2));
		assertEquals(false,listOfVotablePolls.contains(this.testingPoll));
		
		
	}
	
	@Test
	public void testClosureOfPolls(){
		
		PollManager.closePoll(testSession, testingPoll);
		assertEquals(true,testingPoll.isClosed());
	}
	@Test
	public void testresults() throws EmptyOptionTextException, OptionAlreadyVotedException, TooManyVotesException, OptionNotVotedByTheUser{
		// text for the two options 
		String text1 = "text of 1";
		String text2 = "text of 2";

		// add two option
		
		Option option1 = TestingUtilities.insertOptionForTesting(testSession, testingPoll, text1);
		Option option2 = TestingUtilities.insertOptionForTesting(testSession, testingPoll, text2);

		// add three users
		
		WUser user1 = TestingUtilities.insertWUserForTesting(testSession,"fdsafdsa", "x");
		WUser user2 = TestingUtilities.insertWUserForTesting(testSession,"fdsafsdsa", "xs");
		WUser user3 = TestingUtilities.insertWUserForTesting(testSession,"fdsassfdsa", "ssx");

		// add 1 vote to the first option
		PollManager.addVote(testSession, option1, user1);
		// add 2 votes to the second option
		PollManager.addVote(testSession, option2, user2);
		PollManager.addVote(testSession, option2, user3);
		// check if the count of votes works
		
		Map<String, Integer> map =PollManager.getResults(testSession, testingPoll);
		
		Integer firstpollvotes= map.get(text1);
		Integer secondpollvotes= map.get(text2);
		Assert.assertNotEquals(null, firstpollvotes);
		Assert.assertNotEquals(null, secondpollvotes);
		Assert.assertEquals(1, firstpollvotes.intValue());
		Assert.assertEquals(2, secondpollvotes.intValue());

		PollManager.removeVote(testSession, option1, user1);
		PollManager.removeVote(testSession, option2, user2);

		PollManager.removeVote(testSession, option2, user3);

	}
	
	@Test
	public void isPollAlradyPresentTest() throws PollAlreadyExistsException, InvalidPreferenceNumber {
	
		
		WUser anotheruser = TestingUtilities.insertWUserForTesting(testSession,"seconduyeser", "and_a_mail");
		Poll poll1 = TestingUtilities.insertPollForTesting(testSession, anotheruser, "xxxssssdd");
		
	
		boolean found = PollManager.isPollAlreadyPresent(testSession, poll1.getQuestion());
		
		Assert.assertEquals(true, found);
		
		boolean found2 = PollManager.isPollAlreadyPresent(testSession, "Brand new question!fhjtyhuytrthdfirtji");

		
		Assert.assertEquals(false,found2);
	}
	// closePollAutomaticallyTesting
//	@Test
//	public void callclosePollAutomaticallyTestingWithoutAnythingToclose()
//	{
//		PollManager.closePollsAutomatically();
//	}
	
//	@Test
//	public void callclosePollAutomaticallyTestingWithOnePollToClose() throws PollAlreadyExistsException, InvalidPreferenceNumber
//	{
//	TestingUtilities.insertPollForTesting(testSession, testingUser, "xxxxxxssdd-- ", new Date());
//		PollManager.closePollsAutomatically();
//	}
	
//	@Test
//	public void callclosePollAutomaticallyTestingWithTwoPollsToClose()
//	{
//		PollManager.closePollsAutomatically();
//	}
	
	@Test
	public void notifyUsersTest() throws PollAlreadyExistsException, EmptyOptionTextException, OptionAlreadyVotedException, TooManyVotesException, OptionNotVotedByTheUser, InvalidPreferenceNumber {
		
		WUser anotheruser = TestingUtilities.insertWUserForTesting(testSession,"seconduyeser", "hgfdhjgfdjydstfr");
		Poll poll1 = TestingUtilities.insertPollForTesting(testSession, anotheruser, "xxxssssdd");
		Option option = TestingUtilities.insertOptionForTesting(testSession, poll1, "option1");
		PollManager.addVote(testSession, option, anotheruser);
		boolean notify = PollManager.notifyUsers(testSession, poll1);
		
		assertEquals(true,notify);
		
		PollManager.removeVote(testSession, option, anotheruser);
		
	}
}
