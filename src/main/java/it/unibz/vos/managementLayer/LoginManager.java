package it.unibz.vos.managementLayer;



import org.apache.commons.lang3.builder.EqualsBuilder;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.servelts.exceptions.InvalidLoginException;
import it.unibz.vos.util.Hasher;


public class LoginManager {


	 /**
	  * USE ONLY WITH USRES LOADED FROM THE DATABASE
	  * Method used for the check of login validity
	  * @param user - the user to check against
	  * @param password - password passed by the user
	  * @return true or false, if login is valid or not.
	 * @throws InvalidLoginException 
	  */
	 public static boolean isValidLogin(WUser user, String password) throws InvalidLoginException {

		 	if (user == null) throw new InvalidLoginException("The user cannot be null");
		 	if (password == null) throw new InvalidLoginException("the password cannot be null");
	    	String hashedpwd = Hasher.getHashedString(password);
	    	return new EqualsBuilder().append(user.getPassword(), hashedpwd).isEquals();
	       
	     }
	    


}
