package it.unibz.vos.servelts.exceptions;

public class InvalidLoginException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8302800336116766914L;

	public InvalidLoginException(String msg){
		super(msg);
	}
}
