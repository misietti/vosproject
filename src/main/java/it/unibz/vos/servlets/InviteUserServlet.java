package it.unibz.vos.servlets;

import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.ServletUtil;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InviteUserServlet
 */
@WebServlet("/InviteUserServlet")
public class InviteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InviteUserServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());

		// check if user is logged
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirec, so stop the method
		}
		// check if the id the poll is present
		String PollIdString = request.getParameter("poll_id");
		if (ServletUtil.isNullOrEmpty(PollIdString))
		{
			WebPagesUtilities.redirectToErrorSite(servcont, "THIS IS THE ERROR Plase use the botton in the homePage");
		}
		Long id = null;
		try{
		id = new Long(PollIdString);
		}
		catch (NumberFormatException e){
			WebPagesUtilities.redirectToErrorSite(servcont, "Plase use the botton in the homePage, id of poll is of the wrong type");
		}
		// redirect to the inviting page :)
		
		request.setAttribute("pollId", id);
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/inviteToPoll.jsp");
		rd.include(request, response);
		
	}

}
