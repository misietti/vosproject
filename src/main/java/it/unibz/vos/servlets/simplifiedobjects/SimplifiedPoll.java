package it.unibz.vos.servlets.simplifiedobjects;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.hibernate.Session;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.util.ObjectChecker;

public class SimplifiedPoll {

	private long ID;
	private int maxPreferences;
	private Date dealineDate;
	private Date startdate ;
	private String question;
	private boolean isPrivate;
	private boolean userCanAddOptions;
	private boolean isClosed;
	private int numberOfUserVotes;
	private String creatorUsername ;
	//TODO test the contructor if the values are effectively the same..
public 	SimplifiedPoll(Session session, Poll poll){
	ObjectChecker.checkSession(session);
	this.ID = poll.getID();
	this.maxPreferences = poll.getMaxPreferences();
	this.dealineDate = poll.getDeadlineDate();
	this.startdate = poll.getStartdate();
	this.question = poll.getQuestion();
	this.isPrivate = poll.isPrivate();
	this.userCanAddOptions = poll.getCanUserAddOption();
	this.isClosed = poll.isClosed();
	this.numberOfUserVotes = PollManager.getNumberOfUsersThatVoted(session,poll);
	this.creatorUsername = poll.getCreator().getUsername();
}
public long getID() {
	return ID;
}
public int getMaxPreferences() {
	return maxPreferences;
}
public String getDealineDate() {
	
	Calendar x = new GregorianCalendar();
	x.setTime(dealineDate);
	String dayString = "";
	String monthString = "";
	int day = x.get(Calendar.DAY_OF_MONTH);
	if (day < 10) dayString = "0" + day;
	else dayString = "" + day;
	int month = (x.get(Calendar.MONTH)+1);
		if (month < 10) monthString = "0" + month;
	else monthString = "" + month;

		
	return dayString + "/" + monthString + "/" + (x.get(Calendar.YEAR));
}
public String getStartdate() {
	Calendar x = new GregorianCalendar();
	x.setTime(startdate);
	String dayString = "";
	String monthString = "";
	int day = x.get(Calendar.DAY_OF_MONTH);
	if (day < 10) dayString = "0" + day;
	else dayString = "" + day;
	int month = (x.get(Calendar.MONTH)+1);
		if (month < 10) monthString = "0" + month;
	else monthString = "" + month;

		
	return dayString + "/" + monthString + "/" + (x.get(Calendar.YEAR));
}
public String getQuestion() {
	return question;
}
public boolean isPrivate() {
	return isPrivate;
}
public boolean isUserCanAddOptions() {
	return userCanAddOptions;
}
public boolean isClosed() {
	return isClosed;
}
public int getNumberOfUserVotes() {
	return numberOfUserVotes;
}
public String getCreatorUsername() {
	return creatorUsername;
}

}
