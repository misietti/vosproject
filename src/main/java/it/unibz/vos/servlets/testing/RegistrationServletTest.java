package it.unibz.vos.servlets.testing;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.servlets.RegistrationServlet;
import it.unibz.vos.testing.util.TestingUtilities;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
@RunWith(MockitoJUnitRunner.class)

public class RegistrationServletTest {



	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Option testingOption = null;
	Session testSession = null;
	String userPassword = "there is this password";
	String username = "";
	Long userid = new Long(-1);
	Long pollid = new Long(-1);

	// before after and other util methods
	@Before
	public void mountUP() throws EmptyOptionTextException,
			PollAlreadyExistsException, InvalidPreferenceNumber {
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		userNotSavedInDatabse = TestingUtilities
				.getWuserForTestingNotSavedOnDatabase(testSession);

		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		pollNotSavedOnDatabase = TestingUtilities
				.getPollForTestingNotSavedOnDatabase(testSession, testingUser);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,
				testingUser);
		testingOption = TestingUtilities.insertOptionForTesting(testSession,
				testingPoll);

		userid = testingUser.getID();
		pollid = testingPoll.getID();
		username = "SomeKindOfUnique";

	}

	@After
	public void breakDown() {
		TestingUtilities.deleteALlTestGarbage(testSession);

		testSession.getTransaction().commit();
		testSession.close();

	}


	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	ServletContext servletContext;
	@Mock
	RequestDispatcher requestDispatcher;
	
	
	@Test
	public void RegistrationWorkingTest()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
   		when(request.getParameter("username")).thenReturn(username);
   		when(request.getParameter("name")).thenReturn(userPassword);
		when(request.getParameter("surname")).thenReturn("someSurname");
		when(request.getParameter("password")).thenReturn("apass");
		when(request.getParameter("password2")).thenReturn("apass");
		when(request.getParameter("email")).thenReturn("thdd760jere@fdsafdsa.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertNotEquals(null,user); // user must exist
		testSession.delete(user);
		

	}
	

	@Test
	public void RegistrationNullUsername()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(null);
   		when(request.getParameter("name")).thenReturn(userPassword);
		when(request.getParameter("surname")).thenReturn("someSurname");
		when(request.getParameter("password")).thenReturn("apass");
		when(request.getParameter("password2")).thenReturn("apass");
		when(request.getParameter("email")).thenReturn("thdd760jere@fdsafdsa.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}
	

	@Test
	public void RegistrationNullSurname()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(username);
   		when(request.getParameter("name")).thenReturn(userPassword);
		when(request.getParameter("surname")).thenReturn(null);
		when(request.getParameter("password")).thenReturn("apass");
		when(request.getParameter("password2")).thenReturn("apass");
		when(request.getParameter("email")).thenReturn("thdd760jere@fdsafdsa.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}
	
	@Test
	public void RegistrationNullPassword1()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(username);
   		when(request.getParameter("name")).thenReturn(userPassword);
		when(request.getParameter("surname")).thenReturn("fdsafs");
		when(request.getParameter("password")).thenReturn(null);
		when(request.getParameter("password2")).thenReturn("apass");
		when(request.getParameter("email")).thenReturn("thdd760jere@fdsafdsa.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}

	
	@Test
	public void RegistrationNullPassword2()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(username);
   		when(request.getParameter("name")).thenReturn(userPassword);
		when(request.getParameter("surname")).thenReturn("fdsafs");
		when(request.getParameter("password")).thenReturn("apass");
		when(request.getParameter("password2")).thenReturn(null);
		when(request.getParameter("email")).thenReturn("thdd760jere@fdsafdsa.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}
	

	@Test
	public void RegistrationNullEmail()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(username);
   		when(request.getParameter("name")).thenReturn(userPassword);
		when(request.getParameter("surname")).thenReturn("fdsafs");
		when(request.getParameter("password")).thenReturn("apass");
		when(request.getParameter("password2")).thenReturn("apass");
		when(request.getParameter("email")).thenReturn(null);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}
	
	
	@Test
	public void RegistrationOfALreadyExistentUser()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(testingUser.getUsername());
   		when(request.getParameter("name")).thenReturn(userPassword);
		when(request.getParameter("surname")).thenReturn("fdsafs");
		when(request.getParameter("password")).thenReturn("apass");
		when(request.getParameter("password2")).thenReturn("apass");
		when(request.getParameter("email")).thenReturn("xxxxx@fdsafjdklsahjh832.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}
	
	
	@Test
	public void RegistrationNameInvalid()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(testingUser.getUsername() +"sdf");
   		when(request.getParameter("name")).thenReturn(userPassword + "fdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjak");
		when(request.getParameter("surname")).thenReturn("fdsafs");
		when(request.getParameter("password")).thenReturn("apass");
		when(request.getParameter("password2")).thenReturn("apass");
		when(request.getParameter("email")).thenReturn("xxxxx@fdsafjdklsahjh832.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}
	
	
	@Test
	public void RegistrationInvalidPassword()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		RegistrationServlet servletToTest = new RegistrationServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
String username = "SomeKindOfUnique";
   		when(request.getParameter("username")).thenReturn(testingUser.getUsername() +"sdf");
   		when(request.getParameter("name")).thenReturn(userPassword );
		when(request.getParameter("surname")).thenReturn("fdsafs");
		when(request.getParameter("password")).thenReturn("fdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjakfdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjakfdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjakfdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjak");
		when(request.getParameter("password2")).thenReturn("fdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjakfdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjakfdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjakfdsafdsamiopfdsjakolòfjndsakjolfh89342uhq9fsdafdsafdsafdsafdsafdsafdsafdsafdsa8heqwopihfdlsjak");
		when(request.getParameter("email")).thenReturn("xxxxx@fdsafjdklsahjh832.it");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());
		
		WUser user = WUserManager.getWUserFromUsername(testSession, username);
		assertEquals(null,user); // user must not exist

	}
}
