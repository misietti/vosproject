package it.unibz.vos.servlets.testing;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.servlets.InviteUserServlet;
import it.unibz.vos.testing.util.TestingUtilities;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)

public class InviteUserServletTest {

	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Option testingOption = null;
	Session testSession = null;

	Long userid = new Long(-1);
	Long pollid = new Long(-1);

	// before after and other util methods
	@Before
	public void mountUP() throws EmptyOptionTextException,
			PollAlreadyExistsException, InvalidPreferenceNumber {
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		userNotSavedInDatabse = TestingUtilities
				.getWuserForTestingNotSavedOnDatabase(testSession);

		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		pollNotSavedOnDatabase = TestingUtilities
				.getPollForTestingNotSavedOnDatabase(testSession, testingUser);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,
				testingUser);
		testingOption = TestingUtilities.insertOptionForTesting(testSession,
				testingPoll);

		userid = testingUser.getID();
		pollid = testingPoll.getID();

	}

	@After
	public void breakDown() {
		TestingUtilities.deleteALlTestGarbage(testSession);

		testSession.getTransaction().commit();
		testSession.close();

	}



	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	ServletContext servletContext;
	@Mock
	RequestDispatcher requestDispatcher;

	@Test
	public void InviteUserServletTestWithRightParametersForloggedUser()
			throws ServletException, IOException {

		String newPassword = "mail@gmail.it";
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InviteUserServlet servletToTest = new InviteUserServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("password1")).thenReturn(newPassword);
		when(request.getParameter("poll_id")).thenReturn(testingPoll.getID().toString());
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());

	}
	
	@Test
	public void InviteUserServletTestNotLogged()
			throws ServletException, IOException {

		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("vcxzvxz", testingUser.getUsername());

		InviteUserServlet servletToTest = new InviteUserServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("poll_id")).thenReturn(testingPoll.getID().toString());
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());

	}
	
	
	@Test
	public void InviteUserServletWitnInvalidPoll()
			throws ServletException, IOException {

		String newPassword = "mail@gmail.it";
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InviteUserServlet servletToTest = new InviteUserServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("password1")).thenReturn(newPassword);
		when(request.getParameter("poll_id")).thenReturn("-1");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		

	}
	
	@Test
	public void InviteUserServletWitnNulldPoll()
			throws ServletException, IOException {

		String newPassword = "mail@gmail.it";
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InviteUserServlet servletToTest = new InviteUserServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("password1")).thenReturn(newPassword);
		when(request.getParameter("poll_id")).thenReturn(null);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		

	}
}
