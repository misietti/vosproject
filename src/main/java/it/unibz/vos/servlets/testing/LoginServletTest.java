package it.unibz.vos.servlets.testing;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.servlets.LoginServlet;
import it.unibz.vos.testing.util.TestingUtilities;
import it.unibz.vos.util.Hasher;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;



@RunWith(MockitoJUnitRunner.class)
public class LoginServletTest {


	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Option testingOption = null;
	Session testSession = null;
	String userPassword = "there is this password";

	Long userid = new Long(-1);
	Long pollid = new Long(-1);

	// before after and other util methods
	@Before
	public void mountUP() throws EmptyOptionTextException,
			PollAlreadyExistsException, InvalidPreferenceNumber {
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		userNotSavedInDatabse = TestingUtilities
				.getWuserForTestingNotSavedOnDatabase(testSession);

		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		pollNotSavedOnDatabase = TestingUtilities
				.getPollForTestingNotSavedOnDatabase(testSession, testingUser);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,
				testingUser);
		testingOption = TestingUtilities.insertOptionForTesting(testSession,
				testingPoll);

		userid = testingUser.getID();
		pollid = testingPoll.getID();

	}

	@After
	public void breakDown() {
		TestingUtilities.deleteALlTestGarbage(testSession);

		testSession.getTransaction().commit();
		testSession.close();

	}


	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	ServletContext servletContext;
	@Mock
	RequestDispatcher requestDispatcher;

	
	
	@Test
	public void LoginWorkingTest()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		LoginServlet servletToTest = new LoginServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("username")).thenReturn(testingUser.getUsername());
		when(request.getParameter("password")).thenReturn(userPassword);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());

	}
	
	
	
	
	
	@Test
	public void LoginNullUsername()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		LoginServlet servletToTest = new LoginServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("username")).thenReturn(null);
		when(request.getParameter("password")).thenReturn(userPassword);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());

	}
	
	
	
	@Test
	public void LoginNullPassword()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		LoginServlet servletToTest = new LoginServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("username")).thenReturn(testingUser.getUsername());
		when(request.getParameter("password")).thenReturn(null);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());

	}
	
	
	
	@Test
	public void LoginNonExistentUser()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		LoginServlet servletToTest = new LoginServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("username")).thenReturn(testingUser.getUsername()+ "fdsafdsajiuopfdsa");
		when(request.getParameter("password")).thenReturn(userPassword);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());

	}
	
	
	
	@Test
	public void LoginValidUsernameInvalidPassowrd()
			throws ServletException, IOException {

		 
	//	Cookie[] loggedUSerCookies = new Cookie[1];
	//	loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		LoginServlet servletToTest = new LoginServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		//when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("username")).thenReturn(testingUser.getUsername());
		when(request.getParameter("password")).thenReturn(userPassword + "fdsa");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		//assertEquals(Hasher.getHashedString(userPassword), testingUser.getPassword());

	}
	
	
	
	
	
	
	
	
	
	
	@Test
	public void hashingtest() {
		
		System.out.println(Hasher.getHashedString("lol18"));
		
	
	}

}
