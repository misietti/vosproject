package it.unibz.vos.servlets.testing;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.servlets.ChangeEmailServlet;
import it.unibz.vos.servlets.InvitationConfirmationServlet;
import it.unibz.vos.testing.util.TestingUtilities;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InvitationConfirmationServletTest {
	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Session testSession = null;
	
    Long userid = null;
    Long pollid = null;
	

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	ServletContext servletContext;
	@Mock
	RequestDispatcher requestDispatcher;

// before after and other util methods
	@Before
	public void mountUP() throws PollAlreadyExistsException, InvalidPreferenceNumber{
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
	
		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		userNotSavedInDatabse = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,testingUser);
		
		userid = testingUser.getID();
		pollid = testingPoll.getID();
	
		
		pollNotSavedOnDatabase = TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession,testingUser);

	}


	@After
	public void breakDown(){
		
		TestingUtilities.deleteALlTestGarbage(testSession);
		testSession.getTransaction().commit();
		testSession.close();
	}
	
	@Test
	public void TryToInviteWhileNotBeingLogged() throws ServletException, IOException, PollAlreadyExistsException, InvalidPreferenceNumber {
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("nasme", "anotherUhhser");

		InvitationConfirmationServlet servletToTest = new InvitationConfirmationServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		// well nothing to assert.
	}
	
	@Test
	public void TryToInviteWithoutPollID() throws ServletException, IOException, PollAlreadyExistsException, InvalidPreferenceNumber {
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InvitationConfirmationServlet servletToTest = new InvitationConfirmationServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		// well nothing to assert.
	}
	
	@Test
	public void TryToInviteWithInvalidPollID() throws ServletException, IOException, PollAlreadyExistsException, InvalidPreferenceNumber {
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InvitationConfirmationServlet servletToTest = new InvitationConfirmationServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		when(request.getParameter("pollID")).thenReturn("asd");

		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		// well nothing to assert.
	}
	
	@Test
	public void TestWithoutEmailAndUsername() throws ServletException, IOException, PollAlreadyExistsException, InvalidPreferenceNumber {		
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InvitationConfirmationServlet servletToTest = new InvitationConfirmationServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("pollID")).thenReturn(this.testingPoll.getID().toString());
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		// well nothing to assert.
	}
	
	@Test
	public void TryToInviteWithEmailForAPublicPoll() throws ServletException, IOException, PollAlreadyExistsException, InvalidPreferenceNumber {
		WUser yetAnotherUser = TestingUtilities.insertWUserForTesting(testSession,"da","cxv");
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InvitationConfirmationServlet servletToTest = new InvitationConfirmationServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("pollID")).thenReturn(this.testingPoll.getID().toString());
		when(request.getParameter("email")).thenReturn(yetAnotherUser.getEmail());
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		// well nothing to assert.
	}
	
	@Test
	public void TryToInviteWithInvalidEmailForAPublicPoll() throws ServletException, IOException, PollAlreadyExistsException, InvalidPreferenceNumber {
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		InvitationConfirmationServlet servletToTest = new InvitationConfirmationServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("pollID")).thenReturn(this.testingPoll.getID().toString());
		when(request.getParameter("email")).thenReturn("fdasfdas@fdascc.id");
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		// well nothing to assert.
	}
	
	

}
