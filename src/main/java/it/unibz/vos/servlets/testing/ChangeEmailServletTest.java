package it.unibz.vos.servlets.testing;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.servlets.ChangeEmailServlet;
import it.unibz.vos.testing.util.TestingUtilities;

import java.io.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.*;

@RunWith(MockitoJUnitRunner.class)
public class ChangeEmailServletTest {

	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Option testingOption = null;
	Session testSession = null;

	Long userid = new Long(-1);
	Long pollid = new Long(-1);

	// before after and other util methods
	@Before
	public void mountUP() throws EmptyOptionTextException,
			PollAlreadyExistsException, InvalidPreferenceNumber {
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		userNotSavedInDatabse = TestingUtilities
				.getWuserForTestingNotSavedOnDatabase(testSession);

		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		pollNotSavedOnDatabase = TestingUtilities
				.getPollForTestingNotSavedOnDatabase(testSession, testingUser);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,
				testingUser);
		testingOption = TestingUtilities.insertOptionForTesting(testSession,
				testingPoll);

		userid = testingUser.getID();
		pollid = testingPoll.getID();

	}

	@After
	public void breakDown() {
		TestingUtilities.deleteALlTestGarbage(testSession);

		testSession.getTransaction().commit();
		testSession.close();

	}

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	ServletContext servletContext;
	@Mock
	RequestDispatcher requestDispatcher;

	@Test
	public void ChangeEmailServletTestWithRightParametersForloggedUser()
			throws ServletException, IOException {

		String newmail = "mail@gmail.it";
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		ChangeEmailServlet servletToTest = new ChangeEmailServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("email1")).thenReturn(newmail);
		when(request.getParameter("email2")).thenReturn(newmail);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		assertEquals(newmail, testingUser.getEmail());

	}

	@Test
	public void ChangeEmailWithNullFirstmail() throws ServletException,
			IOException {
		String oldMail = testingUser.getEmail();
		String newmail = "mail@gmail.it";
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		ChangeEmailServlet servletToTest = new ChangeEmailServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("email1")).thenReturn(null);
		when(request.getParameter("email2")).thenReturn(newmail);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		assertEquals(oldMail, testingUser.getEmail());

	}
	
	@Test
	public void ChangeEmailWithNullSecondMail() throws ServletException,
			IOException {
		String oldMail = testingUser.getEmail();
		String newmail = "mail@gmail.it";
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		ChangeEmailServlet servletToTest = new ChangeEmailServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("email1")).thenReturn(newmail);
		when(request.getParameter("email2")).thenReturn(null);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		assertEquals(oldMail, testingUser.getEmail());

	}
	
	@Test
	public void ChangeEmailWithNullBothMails() throws ServletException,
			IOException {
		String oldMail = testingUser.getEmail();
		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		ChangeEmailServlet servletToTest = new ChangeEmailServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("email1")).thenReturn(null);
		when(request.getParameter("email2")).thenReturn(null);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		assertEquals(oldMail, testingUser.getEmail());

	}
	
	@Test
	public void ChangeEmailServletWithTwoDifferentMails()
			throws ServletException, IOException {

		String newmail = "mail@gmail.it";
		String newmail2 = "mail2@gmail.it";
		String oldMail = testingUser.getEmail();

		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		ChangeEmailServlet servletToTest = new ChangeEmailServlet() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("email1")).thenReturn(newmail);
		when(request.getParameter("email2")).thenReturn(newmail2);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		assertEquals(oldMail, testingUser.getEmail());

	}
	
	
	@Test
	public void changemailsInvalid()
			throws ServletException, IOException {

		String newmail = "mailmail.it";
		String newmail2 = "mailmail.it";
		String oldMail = testingUser.getEmail();

		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("name", testingUser.getUsername());

		ChangeEmailServlet servletToTest = new ChangeEmailServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("email1")).thenReturn(newmail);
		when(request.getParameter("email2")).thenReturn(newmail2);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		assertEquals(oldMail, testingUser.getEmail());

	}
	
	
	@Test
	public void TryTochangeEmailWithNotLogged()
			throws ServletException, IOException {

		String newmail = "mail@gmail.it";
		String oldMail = testingUser.getEmail();

		Cookie[] loggedUSerCookies = new Cookie[1];
		loggedUSerCookies[0] = new Cookie("noNAME", "fdsafsd");

		ChangeEmailServlet servletToTest = new ChangeEmailServlet() {
			private static final long serialVersionUID = 1L;

			public ServletContext getServletContext() {
				return servletContext;
			}
		};
		when(request.getCookies()).thenReturn(loggedUSerCookies);
		when(request.getParameter("email1")).thenReturn(newmail);
		when(request.getParameter("email2")).thenReturn(newmail);
		when(servletContext.getRequestDispatcher(any(String.class)))
				.thenReturn(requestDispatcher);
		// need to commit it to the database, Because we don't want two parallel
		// transactions :D
		testSession.getTransaction().commit();
		servletToTest.doPost(request, response);
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		testingUser = (WUser) testSession.get(WUser.class, testingUser.getID());
		assertEquals(oldMail, testingUser.getEmail());

	}

	
}