package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class VoteWithAdditionalOption
 */
@WebServlet("/VoteWithAdditionalOptionRequest")
public class VoteWithAdditionalOptionRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VoteWithAdditionalOptionRequest() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());

		// check if use is logged
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirect, so stop the method
		}
		
		// generate the data for the taken the post.
		String pollIdString = request.getParameter("poll_id");
		if (pollIdString == null)
			{
			WebPagesUtilities.redirectToErrorSite(servcont,"There has been no poll passed, don't try access this page directly, use the homepage");
			return ;
			}// get the poll_id from the request
		Long pollId = null;
		try{
		pollId = new Long(pollIdString);
		}
		catch(NumberFormatException e)
		{
			WebPagesUtilities.redirectToErrorSite(servcont,"The poll id  isn't a number, don't try access this page directly, use the homepage");
			return;
		}
		Session session = HibernateSession.getInstance().openSession();
		session.beginTransaction();
		Poll thePoll =  PollManager.getPoll(session, pollId);
		// 
		// 
		Set<Option> options = PollManager.getOptionsOfaPoll(thePoll);
		HashMap<Long,String> hashOptions = new HashMap<Long,String>();
		for (Option o : options){
			hashOptions.put(o.getId(), o.getOptionText());
		}
		request.setAttribute("QuestionText", thePoll.getQuestion());
		request.setAttribute("pollId", thePoll.getID());
		request.setAttribute("userCanAddOptions", thePoll.getCanUserAddOption());

		session.getTransaction().commit();
		session.close();
		//
		WebPagesUtilities.redirectToPageFromRoot(servcont,"/Pages/vote.jsp");
		// 
		//
	}

}
