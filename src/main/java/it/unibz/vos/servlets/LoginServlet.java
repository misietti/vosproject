package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.LoginManager;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.servelts.exceptions.InvalidLoginException;
import it.unibz.vos.util.ServletUtil;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;



/**
 * Servlet implementation class MainServlet
 */
@WebServlet(name = "Login", urlPatterns = { "/Login" })

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		
	//	PrintWriter responseWriter = response.getWriter();
		 
		
		String errorMsg = null;
		
		if(ServletUtil.isNullOrEmpty(username)){
			errorMsg ="Username can't be null or empty";
		}
		if(ServletUtil.isNullOrEmpty(password)){
			errorMsg = "Password can't be null or empty";
		}
				
		if(errorMsg != null){
			
		} else {
			Session hsession = HibernateSession.getInstance().openSession();
			hsession.beginTransaction();
			WUser user_from_usename = WUserManager.getWUserFromUsername(hsession,username);
			hsession.getTransaction().commit();
			hsession.close();
			if ( user_from_usename == null) 
			{
				errorMsg = "The username/password combination is wrong!";
				RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/login.jsp");
		    	
    	    	request.setAttribute("loginerrormessage", errorMsg);
    	    	rd.include(request, response);
				
				return;
			}
			
			try {
				if (LoginManager.isValidLogin(user_from_usename, password)){
					
							Cookie ck=new Cookie("name",username);  
				            response.addCookie(ck);  
							response.sendRedirect(request.getContextPath() + "/HomePageServlet");
							return;
						

				
				}
				else
				{
					
					errorMsg = "The login is not valid!";
					RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/login.jsp");
			    	
	    	    	request.setAttribute("loginerrormessage", errorMsg);
	    	    	rd.include(request, response);
					return;
				}
				
			} catch (InvalidLoginException e) { // this is impossible to reach. but let's leave it here
				
				errorMsg = "The username/password combination is wrong!";
				RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/login.jsp");
		    	
    	    	request.setAttribute("loginerrormessage", errorMsg);
    	    	rd.include(request, response);
			}
		}
		
	}

}
