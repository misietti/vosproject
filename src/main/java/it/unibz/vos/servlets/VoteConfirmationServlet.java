package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.managementLayer.exceptions.DuplicateOptionException;
import it.unibz.vos.managementLayer.exceptions.OptionAlreadyVotedException;
import it.unibz.vos.managementLayer.exceptions.TooManyVotesException;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.ServletUtil;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class VoteConfirmationServlet
 */
@WebServlet("/VoteConfirmationServlet")
public class VoteConfirmationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VoteConfirmationServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());

		// check if the user is logged
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirec, so stop the method
		}
		
		boolean areThereSelectedValues = true;
		boolean areThereNewOptions = true;
		// variables used in the code
		Long pollId = null; // contains the pollid

	

		// obtain the values needed from the request
		String username = CookiesManager.geLoggedUserName(request);
		String[] newOptions = request.getParameterValues("newOptions[]");
		String[] selectedvalues = request
				.getParameterValues("MagicGeneratedCheckBox");
		String pollIdString = request.getParameter("poll_id");

		// check if the poll string is not null or empty
		if (ServletUtil.isNullOrEmpty(pollIdString)) {
			WebPagesUtilities.redirectToErrorSite(servcont, "You shall get what you search when you will climb the highest mountain and login to the homepage and vote from there, (Poll id not found)");
			return;
		}
		// try to parse the pollidfrom the string
		try {
			pollId = new Long(pollIdString);
		} catch (NumberFormatException e) {
			WebPagesUtilities.redirectToErrorSite(servcont, "You shall get what you search when you will climb the highest mountain and login to the homepage and vote from there, (Invalid Poll id)");
			return;
		}
		// check if the selectedvalue is empty
		if (ServletUtil.isNullOrEmpty(selectedvalues)) {
			areThereSelectedValues = false;
		}

		// check if the newoptions are empty
		if (ServletUtil.isNullOrEmpty(newOptions)) {
			areThereNewOptions = false;
		}
		// get the newly generated ones
		// if the both source of options to vote are null or empty error!
		if (!areThereSelectedValues && !areThereNewOptions) {
			WebPagesUtilities.redirectToErrorSite(servcont, "You should select at least one option, of id you are enabled to create on new option");
			return;
		}

		// if there are no new options then just ignore them....
		// if there are some new options we need to create them and then add
		// them

		// first check if the length of both arrays
		// if they are together higher than the option give an error
		int AmountOfVotes = 0;
		if (areThereSelectedValues) {
			AmountOfVotes += selectedvalues.length;
		}
		if (areThereNewOptions) {
			AmountOfVotes += newOptions.length;
		}

		Session session = HibernateSession.getInstance().openSession();
		session.beginTransaction();
		Poll thePoll = PollManager.getPoll(session, pollId);

		// check if the amount of votes is greater than the allowed one
		if (AmountOfVotes > thePoll.getMaxPreferences()) {
			ServletUtil.closeRollBackSession(session);
			WebPagesUtilities.redirectToErrorSite(servcont, "you managed to select too many options.. well you can't do that sadly :(");
			return;
		}

		WUser user = WUserManager.getWUserFromUsername(session, username);
		if (user == null) {

			ServletUtil.closeRollBackSession(session);
			String error = "You shall get what you search when you will climb the highest mountain and login to the homepage and vote from there, (User not found)";
			WebPagesUtilities.redirectToErrorSite(servcont, error);
			return;
		}

		if (areThereSelectedValues) {

			for (String selectedValue : selectedvalues) {
				Long id = null;
				try {

					id = new Long(selectedValue);
				} catch (NumberFormatException e) {
					ServletUtil.closeRollBackSession(session);
					String error = "You shall get what you search when you will climb the highest mountain and login to the homepage and vote from there, (Invalid id's)";
					WebPagesUtilities.redirectToErrorSite(servcont, error);
					return;

				}

				Option option = (Option) session.get(Option.class, id);

				if (option == null) {
					ServletUtil.closeRollBackSession(session);
					String error = "Option ids passed are wrong!";
					WebPagesUtilities.redirectToErrorSite(servcont, error);
					return;
				}
				try {

					PollManager.addVote(session, option, user);

				} catch (OptionAlreadyVotedException e) {
					ServletUtil.closeRollBackSession(session);
					String error = "You are trying to do the same thing twice, which is amazing, really!";
					WebPagesUtilities.redirectToErrorSite(servcont, error);
					return;
				} catch (TooManyVotesException e) {
					ServletUtil.closeRollBackSession(session);
					String error = "Somehow you managed to get too many votes in...";
					WebPagesUtilities.redirectToErrorSite(servcont, error);
					return;
				}

			}
		}

		ArrayList<Option> optionsAdded = new ArrayList<Option>();
		if (areThereNewOptions) {
			for (String optionText : newOptions) {
				if (!optionText.trim().isEmpty()) {
					try {

						Option option = PollManager.insertOptionIntoPoll(
								session, thePoll, optionText, user);
						optionsAdded.add(option);

					} catch (EmptyOptionTextException e) { // invalid option
															// passed
						ServletUtil.closeRollBackSession(session);
						WebPagesUtilities
								.redirectToErrorSite(servcont,
										"You managed to pass empty options to the system, congrats... you should hear the dogs already :D");
						return;
					} catch (DuplicateOptionException e) {
						ServletUtil.closeRollBackSession(session);
						WebPagesUtilities
								.redirectToErrorSite(servcont,
										"You passed the same option twice ... you should hear the dogs already :D");
						return;
					}
				}
			}
		}

		for (Option option : optionsAdded) {
			try {
				PollManager.addVote(session, option, user);
			} catch (OptionAlreadyVotedException e) {
				ServletUtil.closeRollBackSession(session);
				WebPagesUtilities
						.redirectToErrorSite(servcont,
								"You are trying to do the same thing twice, which is amazing, really!");
				return;
			} catch (TooManyVotesException e) {
				ServletUtil.closeRollBackSession(session);
				WebPagesUtilities.redirectToErrorSite(servcont,
						"Somehow you managed to get too many votes in...");
				return;
			}
		}

		session.getTransaction().commit();
		session.close();

		WebPagesUtilities.redirectToPageFromRoot(servcont,
				"/Pages/Confirmations/VoteConfirmationPage.jsp");

	}

	


}
