package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class UserSettingsRequestServlet
 */
@WebServlet("/UserSettingsRequestServlet")
public class UserSettingsRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSettingsRequestServlet() {
        super();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	// check if user is logged
    	// if he isn't redirect to login
    	if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirec, so stop the method
		}
    	String userName = CookiesManager.geLoggedUserName(request);
    	
    	Session hsession = HibernateSession.getInstance().openSession();
    	hsession.beginTransaction();
    	WUser theUser = WUserManager.getWUserFromUsername(hsession, userName);
		// take the User id and pass it on for the forms in the next page
		request.setAttribute("userid", String.valueOf(theUser.getID()));
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/setting.jsp");
		rd.include(request, response);
    
		hsession.getTransaction().commit();
		hsession.close();
    }


}
