package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.managementLayer.exceptions.NoWUserFoundException;
import it.unibz.vos.util.EmailSender;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class PasswordRecoveryServlet
 */
@WebServlet("/PasswordRecovery")
public class PasswordRecoveryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PasswordRecoveryServlet() {
        super();
        
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());
		String email = request.getParameter("email");
		
		if (email == null || email.isEmpty()) {
			
			WebPagesUtilities.redirectToErrorSite(servcont, "The email cannot be empty!");
		}
		Session session = HibernateSession.getInstance().openSession();
		session.beginTransaction();
		WUser wuser = null;
		try {
			
			wuser = WUserManager.getWUserFromEmail(session, email);
			
		} catch (NoWUserFoundException e1) {
			
			WebPagesUtilities.redirectToErrorSite(servcont, e1.getMessage());

		}
		
		String password = Long.toHexString(Double.doubleToLongBits(Math.random()));;
		try {
			wuser.setPassword(password);
			session.getTransaction().commit();
			session.close();
			EmailSender.sendEmail(wuser, "Hello! This mail has been sent to you since you asked to"
					+ " recover your password! Your new generated password is: " + password + "\nChange"
							+ " it from your personal settings after you login again! :)", "Password recovery!");
			response.sendRedirect(request.getContextPath() + "/HomePageServlet");
		} catch (InvalidPassowrdException e) {
			
			System.out.println("The generated password could not be set: " + e.getLocalizedMessage());
		}
		
		
		
	}


}
