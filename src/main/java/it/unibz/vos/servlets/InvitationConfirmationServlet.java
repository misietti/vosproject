package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.managementLayer.exceptions.InvalidPollInvitationException;
import it.unibz.vos.managementLayer.exceptions.NoWUserFoundException;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.ServletUtil;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class InvitationConfirmationServlet
 */
@WebServlet("/InvitationConfirmationServlet")
public class InvitationConfirmationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InvitationConfirmationServlet() {
        super();
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());
		// check if user is logged
		
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirec, so stop the method
		}
		String loggedUserUserName = CookiesManager.geLoggedUserName(request);
		
		
		// check if the mail field or the username field has been used for the request
		
		String pollIdString = 	ServletUtil.getParameterRedirectToErrorIfNullOrEmpty("pollID",servcont,"Plase use the botton in the homePage to make the request, (no pollid)");
		if (pollIdString == null )	{return;} // return if redirected
	
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		
		Long pollId = null;
		try{
			pollId = new Long(pollIdString);
		}
		catch(NumberFormatException e){
			WebPagesUtilities.redirectToErrorSite(servcont, "Plase use the botton in the homePage to make the request ,(invalid pollid)");
			return;
		}
		
		if (username == null && email == null)
		{
			WebPagesUtilities.redirectToErrorSite(servcont, "You have to insert a parameter for discovering the user, (no user and no username)");
			return;
		}
		WUser user = null;
		Session hsession =  HibernateSession.getInstance().openSession();
		hsession.beginTransaction();
		WUser loggedUser = WUserManager.getWUserFromUsername(hsession, loggedUserUserName) ;
		// obtain the user with one or the other value
		if (email != null)
			try {
				user = WUserManager.getWUserFromEmail(hsession, email);
			} catch (NoWUserFoundException e1) {
				WebPagesUtilities.redirectToErrorSite(servcont,"Cannot find an user with specified parameter");
				return;
			}
		if (username != null) user = WUserManager.getWUserFromUsername(hsession, username);
		
		if (user == null) {
			WebPagesUtilities.redirectToErrorSite(servcont,"Cannot find an user with specified parameter");
			ServletUtil.closeRollBackSession(hsession);
			return;
			
		}
		
		Poll thePoll = PollManager.getPoll(hsession, pollId);
		if (thePoll == null ){
			WebPagesUtilities.redirectToErrorSite(servcont, "Invalid pollid, there is no poll with such an id, please use the homepage or contact the webmaster");
			ServletUtil.closeRollBackSession(hsession);
			return;
		}
		// check if the poll is private ( if it is, block everything, we don't invite to public ones)
		if (!thePoll.isPrivate())
		{
			WebPagesUtilities.redirectToErrorSite(servcont, "You cannot invite users to public polls, everyone can access those polls");
			ServletUtil.closeRollBackSession(hsession);
			return;
		}
		// add the invite to the poll
		
		if (!thePoll.getCreator().equals(loggedUser)){
			WebPagesUtilities.redirectToErrorSite(servcont,"You cannot users to polls you don't own...");
			ServletUtil.closeRollBackSession(hsession);
			return;
		}
		
		try {
			PollManager.InviteUserToPoll(hsession, thePoll, user);
		} catch (InvalidPollInvitationException e) {
			WebPagesUtilities.redirectToErrorSite(servcont, "Error when inviting :" + e.getMessage());
			ServletUtil.closeRollBackSession(hsession);
			return;
		} catch (IllegalArgumentException e) {
			WebPagesUtilities.redirectToErrorSite(servcont,  e.getMessage());
			ServletUtil.closeRollBackSession(hsession);
			return;
		}
		// confirm the invitation 
		
		request.setAttribute("username", user.getUsername());
		WebPagesUtilities.redirectToPageFromRoot(servcont, "/Pages/Confirmations/InvitationConfirmation.jsp");
		
		hsession.getTransaction().commit();
		hsession.close();
	}

}
