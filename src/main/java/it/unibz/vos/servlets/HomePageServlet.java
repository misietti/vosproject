package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.servlets.simplifiedobjects.SimplifiedPoll;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class HomePageServlet
 */
@WebServlet("/HomePageServlet")
public class HomePageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomePageServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/login.jsp");
			rd.include(request, response);
			return; // there has been a redirec, so stop the method
		}
		
		String userUserName = CookiesManager.geLoggedUserName(request);
		Session hsession = HibernateSession.getInstance().openSession();
		hsession.beginTransaction();
		
		ArrayList<Poll> votedPolls = PollManager.getVotedPolls( hsession,WUserManager.getWUserFromUsername(hsession,userUserName));

		ArrayList<Poll> votablePolls = PollManager.getVotablePolls(hsession ,WUserManager.getWUserFromUsername(hsession,userUserName));

		ArrayList<Poll> personalPolls = PollManager.getPersonalPolls(hsession, WUserManager.getWUserFromUsername(hsession,userUserName));


		ArrayList<SimplifiedPoll> simpleVotedPolls = new ArrayList<SimplifiedPoll>();
		ArrayList<SimplifiedPoll> simpleVotablePolls = new ArrayList<SimplifiedPoll>();
		ArrayList<SimplifiedPoll> simplePersonalPolls = new ArrayList<SimplifiedPoll>();
		// fill the simpleVotedPolls
		for(Poll poll :votedPolls )
		{
			simpleVotedPolls.add(new SimplifiedPoll(hsession,poll));
		}
		
		// fill the simpleVotablePolls
		for(Poll poll :votablePolls )
		{
			simpleVotablePolls.add(new SimplifiedPoll(hsession,poll));

		}
		
		// fill the simplePersonalPolls
		for(Poll poll :personalPolls )
		{
			simplePersonalPolls.add(new SimplifiedPoll(hsession,poll));

		}
			
			
		hsession.getTransaction().commit();
		hsession.close();
		
		request.getSession().setAttribute("properhomepagerequest", true);
		if (!simpleVotedPolls.isEmpty()) 	request.setAttribute("votedpolls",simpleVotedPolls );
		if (!simpleVotablePolls.isEmpty()) 	request.setAttribute("pollsthatcanbevoted",simpleVotablePolls );
		if (!simplePersonalPolls.isEmpty()) 	request.setAttribute("personalpolls",simplePersonalPolls );

		
		
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/homePage.jsp");
		rd.include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				this.doGet(request,response); // Oh man this is dangerous, isn't it?
		
	}

}
