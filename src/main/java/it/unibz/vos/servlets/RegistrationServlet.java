	package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidEmailException;
import it.unibz.vos.dbclasses.exceptions.InvalidNameException;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.dbclasses.exceptions.InvalidSurnameException;
import it.unibz.vos.dbclasses.exceptions.InvalidUserNameException;
import it.unibz.vos.dbclasses.exceptions.UsernameAlreadyTakenException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.util.ServletUtil;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 Session hsession = HibernateSession.getInstance().openSession();
		 hsession.beginTransaction();
		  response.setContentType("text/html;charset=UTF-8");
	        try {
	            String username = (String) request.getParameter("username");
	            String name = (String) request.getParameter("name");
	            String surname = (String) request.getParameter("surname");
	            String password = (String) request.getParameter("password");
	            String password2 = (String) request.getParameter("password2");
	            String email = (String) request.getParameter("email");

	            if (password == null || password2 == null ||  password.compareTo(password2)!= 0) {
	    	    	ServletUtil.closeRollBackSession(hsession);
	            	RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/registration.jsp");
			    	request.setAttribute("registerError", "Whoops! Passwords don't match!");
	    	    	rd.include(request, response);
	    	    	return;
	            }
	            WUser wuser = new WUser(hsession,username, name, surname, password, email);
	           
	           
	            WUserManager.insertWUser(hsession,wuser);
	            request.setAttribute("success", username +", successfully registered!");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/Pages/index.jsp");
				rd.include(request, response);
				hsession.getTransaction().commit();
				hsession.close();
				
				
	        } catch (UsernameAlreadyTakenException e) {
    	    	ServletUtil.closeRollBackSession(hsession);
	        	RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/registration.jsp");
		    	request.setAttribute("registerError", "The username was already taken!");
    	    	rd.include(request, response);

    	    	return;
	        	
			} catch (InvalidUserNameException e) {
    	    	ServletUtil.closeRollBackSession(hsession);
				RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/registration.jsp");
		    	request.setAttribute("registerError", "Invalid username!");
    	    	rd.include(request, response);

    	    	return;

			} catch (InvalidNameException e) {
    	    	ServletUtil.closeRollBackSession(hsession);
				RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/registration.jsp");
		    	request.setAttribute("registerError", "Invalid name!");
    	    	rd.include(request, response);

    	    	return;


			} catch (InvalidSurnameException e) {
    	    	ServletUtil.closeRollBackSession(hsession);
				RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/registration.jsp");
		    	request.setAttribute("registerError", "Invalid surname!");
    	    	rd.include(request, response);

    	    	return;

			} catch (InvalidPassowrdException e) {
    	    	ServletUtil.closeRollBackSession(hsession);
				RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/registration.jsp");
		    	request.setAttribute("registerError", "Invalid password!");
    	    	rd.include(request, response);

    	    	return;

			} catch (InvalidEmailException e) {
    	    	ServletUtil.closeRollBackSession(hsession);
				RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/registration.jsp");
		    	request.setAttribute("registerError", "Invalid e-mail!");
    	    	rd.include(request, response);

    	    	return;
			} 
		
	}


	

}
