package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class VisualizeResult
 */
@WebServlet("/VisualizeResult")
public class VisualizeResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VisualizeResult() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		throw new ServletException("get request not supported");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirec, so stop the method
		}

		// get he id of the poll
		String stringPollid = request.getParameter("poll_id");
		if (stringPollid == null)
			throw new ServletException("Tehre isn't a poll to visualize");
		Long pollId = null;
		try {
			pollId = new Long(stringPollid);

		} catch (NumberFormatException e) {
			throw new ServletException("The passed poll id is not valid");
		}

		// calculate the thing
		// send the attribute
		Session hsession = HibernateSession.getInstance().openSession();
		hsession.beginTransaction();
		Poll thePoll = (Poll) hsession.get(Poll.class, pollId);
		if (thePoll == null)throw new ServletException(	"The passed poll id doesn't refer to a poll in the database");
		request.setAttribute("MapOfValues",
				PollManager.getResults(hsession, thePoll));
		request.setAttribute("question", thePoll.getQuestion());

		request.getRequestDispatcher("/Pages/visualize_results.jsp").forward(request,
				response);
		hsession.getTransaction().commit();
		hsession.close();
		//

	}

}
