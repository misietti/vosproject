package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.ServletUtil;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class ChangePasswordServlet
 */
@WebServlet("/ChangePasswordServlet")
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangePasswordServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());

		// check if user is logged
		// redirect if he is not..
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirec, so stop the method
		}
		
		// check if the parameters has been passed
		String pass1 = request.getParameter("password1");
		String pass2 = request.getParameter("password2");
		String username = CookiesManager.geLoggedUserName(request);
		Session hsession = HibernateSession.getInstance().openSession();
    	hsession.beginTransaction();
		WUser user = WUserManager.getWUserFromUsername(hsession, username);
		
		
		
				if (ServletUtil.isNullOrEmpty(pass1) || ServletUtil.isNullOrEmpty(pass2) ){
		    		WebPagesUtilities.redirectToErrorSite(servcont, "You have to write down the password");
		    		return;
		    	}
			// check if they are the same
			if (pass1.compareTo(pass2) != 0){
				WebPagesUtilities.redirectToErrorSite(servcont, "The two passwords didn't correspond!");
				return;
			}
		
		// change the password :)
			
		
			try {
				WUserManager.changeWUserPassword(hsession, user, pass1);
			} catch (InvalidPassowrdException e) {
				ServletUtil.closeRollBackSession(hsession);
				WebPagesUtilities.redirectToErrorSite(servcont, "The passwords is invalid : " + e.getMessage());
				return;
			}
			
			
			hsession.getTransaction().commit();
			hsession.close();
			
			WebPagesUtilities.redirectToPageFromRoot(servcont,"/Pages/Confirmations/PasswordChangeConfirmation.jsp");
	}

}
