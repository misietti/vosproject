package it.unibz.vos.servlets;

import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidEmailException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.ServletUtil;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class ChangeEmailServlet
 */
@WebServlet("/ChangeEmailServlet")
public class ChangeEmailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangeEmailServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());

		// check if the user is the same
		if (WebPagesUtilities.redirectIfNotLogged(response, request,
				this.getServletContext())) {
			return; // there has been a redirect, so stop the method
		}
		// check if the given mails are available

		String email1 = request.getParameter("email1");
		String email2 = request.getParameter("email2");
		String username = CookiesManager.geLoggedUserName(request);
		Session hsession = HibernateSession.getInstance().openSession();
		hsession.beginTransaction();
		WUser user = WUserManager.getWUserFromUsername(hsession, username);
		

		try {
			ChangeEmail(hsession,email1,email2,user);
		} catch (Exception e) {
			WebPagesUtilities.redirectToErrorSite(servcont, e.getMessage());
			return;
		}
		
		
		// show a confirmation
		
		request.setAttribute("email", email1);

		// TODO maybe send a mail of confirmation of email change :D

		hsession.getTransaction().commit();
		hsession.close();
		WebPagesUtilities.redirectToPageFromRoot(servcont,
				"/Pages/Confirmations/EmailChangeConfirmation.jsp");
	}

	public void ChangeEmail(Session session, String email1, String email2, WUser user) throws Exception{

		if (ServletUtil.isNullOrEmpty(email1)
				|| ServletUtil.isNullOrEmpty(email2)) {
			throw new Exception("You cannot send empty mails");
			
		}

		// check if the two mails given are the same and work just fine
		if (email1.compareTo(email2) != 0) {
			throw new Exception("The two emails didn't correspond!");
			
		}

		// change the mail

		try {
			WUserManager.changeWUserMail(session, user, email1);
		} catch (InvalidEmailException e) {
			throw new Exception("The passed mail is invalid");
			
		}
	}

}
