package it.unibz.vos.servlets.pollmanagement;

import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class PollSettingsServlet
 */
@WebServlet("/PollSettingsServlet")
public class PollSettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PollSettingsServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());
			// check if user is logged
		if (WebPagesUtilities.redirectIfNotLogged(servcont)){
			return; // there has been a redirec, so stop the method
		}		 String username = CookiesManager.geLoggedUserName(request);
		 
	
			// get the poll from the id
		 String pollIdString= request.getParameter("poll_id");
		 Long pollId = null;
		 try{
			 pollId = new Long(pollIdString);
		 }
		 catch(NumberFormatException e){
			    	WebPagesUtilities.redirectToErrorSite(servcont,
			    			"Please use the button in the homepage to access the setting of the pollls, (invalid poll id)");
		 }
		 Session hsession = HibernateSession.getInstance().openSession();
		 hsession.beginTransaction();
		 Poll thePoll = PollManager.getPoll(hsession, pollId);
		 if (thePoll == null) 
		 {
		    	WebPagesUtilities.redirectToErrorSite(servcont,
		    			"Please use the button in the homepage to access the setting of the pollls, (no poll on database)");
		   	
		 }
		
		// check if the user is the creator
		 
		 WUser requestUser = WUserManager.getWUserFromUsername(hsession, username);
		if ( requestUser == null) {
			 WebPagesUtilities.redirectToErrorSite(servcont,
					 "Please use the button in the homepage to access the setting of the pollls, (you don't exist on the database)");
		}
		 if (!thePoll.getCreator().equals(requestUser))
		 {
			 WebPagesUtilities.redirectToErrorSite(servcont,
					 "Please use the button in the homepage to access the setting of the pollls, (you are not the owner of the poll)");
		 }
		 // write the needed data in the request
		request.setAttribute("poll", thePoll);
		Set<Option> listofOptions = thePoll.getOptions();
		request.setAttribute("listofoptions", listofOptions);
	
		// show the data in the page
		 
		 hsession.getTransaction().commit();
		 hsession.close();
		 
		 WebPagesUtilities.redirectToPageFromRoot(servcont, "/Pages/PollSettings.jsp");
	}

}
