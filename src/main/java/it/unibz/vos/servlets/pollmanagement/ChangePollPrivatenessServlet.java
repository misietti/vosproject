package it.unibz.vos.servlets.pollmanagement;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class ChangePollPrivatenessServlet
 */
@WebServlet("/ChangePollPrivatenessServlet")
public class ChangePollPrivatenessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangePollPrivatenessServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());

		// check if use is logged
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirect, so stop the method
		}
		
		// check if the user is logged
		Long pollIdVariable = null;

		// check if the required parameters are null or empty
		String privatenessOfPoll = request.getParameter("want_private");
		if (privatenessOfPoll == null || privatenessOfPoll.trim().isEmpty()){
			WebPagesUtilities.redirectToErrorSite(servcont, 
					"Please use the settings page from the homepage to reach this function (no enabling parameter)");
		}
		
		String pollIdString = request.getParameter("poll_id");
		if (pollIdString == null ||pollIdString.trim().isEmpty()  ){
			WebPagesUtilities.redirectToErrorSite(servcont, 
					"Please use the settings page from the homepage to reach this function (no id paramenter)");
			
		}

		try{
		pollIdVariable = new Long(pollIdString);
		} catch(NumberFormatException e){
			WebPagesUtilities.redirectToErrorSite(servcont, 
					"Please use the setting page from the homepage to reach this function (invalid id parameter)");
		}
			
		
		Session hsession = HibernateSession.getInstance().openSession();
		hsession.beginTransaction();
		//Poll thePoll = PollManager.getPoll(hsession, anotherVariable);
		
		// check if the poll is of the user
		String username =CookiesManager.geLoggedUserName(request);
		
		Poll thePoll = (Poll) hsession.get(Poll.class,pollIdVariable);
		if (thePoll == null){
			WebPagesUtilities.redirectToErrorSite(servcont, 
					"Please use the settings page from the homepage to reach this function (null poll)");
		}
		
		WUser requestSender = WUserManager.getWUserFromUsername(hsession, username);
		if (requestSender == null) 
		{
			WebPagesUtilities.redirectToErrorSite(servcont, 
					"Please use the settings page from the homepage to reach this function (null logged user)");
		}
		if (!requestSender.equals(thePoll.getCreator()))
		{
			WebPagesUtilities.redirectToErrorSite(servcont, 
					"Please use the settings page from the homepage to reach this function (Not your poll)");
		}
		// if the value of the pollEnabling attribute is 0, disable, otherwise enable ( whatever the value except null)
		if (privatenessOfPoll.compareTo("0")==0){
			PollManager.makePollPublic(hsession, thePoll);
		}
		else
		{
			PollManager.makePollPrivate(hsession, thePoll);
		}
		
		hsession.getTransaction().commit();
		hsession.close();
		
		// reference back to the setting page with the given id
		// so that the use can see the change.
		
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/PollSettingsServlet");
		rd.include(request, response);
	}

}
