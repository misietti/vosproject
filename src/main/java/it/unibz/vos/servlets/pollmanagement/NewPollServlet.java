package it.unibz.vos.servlets.pollmanagement;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.InvalidQuestionException;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.managementLayer.exceptions.DuplicateOptionException;
import it.unibz.vos.util.CookiesManager;
import it.unibz.vos.util.ServletReqResContextContainer;
import it.unibz.vos.util.ServletUtil;
import it.unibz.vos.util.WebPagesUtilities;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

/**
 * Servlet implementation class NewPollServlet
 */
@WebServlet("/NewPollServlet")
public class NewPollServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewPollServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletReqResContextContainer servcont = new ServletReqResContextContainer(request,response,this.getServletContext());

		// check if use is logged
		if (WebPagesUtilities.redirectIfNotLogged(response,request,this.getServletContext())){
			return; // there has been a redirect, so stop the method
		}
		
		boolean isPrivate = false;
		boolean canAdd = false;
		boolean notifyUser = false;

		  Session session = HibernateSession.getInstance().openSession();
		  session.beginTransaction();
		//Get creator
		String username = CookiesManager.geLoggedUserName(request);
		WUser wuser = WUserManager.getWUserFromUsername(session,username);
		
		@SuppressWarnings("rawtypes")
		Map map = request.getParameterMap();
	// we need the question
	  String question = (String)request.getParameter("question");
	  
	  String[] options = (String[])	map.get("option[]");
	  if (options.length < 1) {
		  
		  WebPagesUtilities.redirectToErrorSite(servcont, "contact the webmasetr, your poll has not options...");
		  ServletUtil.closeRollBackSession(session);
		  return;
	  } 

		// Is Poll private
	  String isPollPrivate = request.getParameter("isPollPrivate");
		//  Can User add options
	  String allowedToAddOptions = request.getParameter("allowedToAddOptions");
	  String notifyUserrString = request.getParameter("NotifyUSersWhenClosed");
	  String prefsNum = request.getParameter("maxOptions");
	  int prefs = Integer.parseInt(prefsNum);
	  
	  String deadline = request.getParameter("deadlineDate");
	// DateFormat format = new SimpleDateFormat("dd/mm/yyyy"); // deprecated in java 8 <- 
	  
	  Date deadlineDate = null;

		
		//deadlineDate = format.parse(deadline);
		deadlineDate = ServletUtil.getDateFromString(deadline);
	
//	catch (ParseException e1) {
//		 ServletUtil.closeRollBackSession(session);
//		  WebPagesUtilities.redirectToErrorSite(servcont, "The Passed date cannot be parsed");
//		  return;
//		
//	}
	
	  if (isPollPrivate != null) {
		  
		  isPrivate = true;
	  }
	  
	  if (allowedToAddOptions != null) {
		  
		  canAdd = true;
		  
	  }
	  if (notifyUserrString != null)
	  {
		  notifyUser = true;
	  }
	  	
		  Poll poll = null;
		try {
			poll = new Poll(session,question, isPrivate, deadlineDate, prefs, wuser, canAdd,notifyUser);
			 PollManager.insertPoll(session,poll);
			 
		} catch (InvalidQuestionException e) {
			 ServletUtil.closeRollBackSession(session);
			  WebPagesUtilities.redirectToErrorSite(servcont, "Invalid poll : " + e.getMessage());
			  return;
			
			
		} catch (PollAlreadyExistsException e) {
			 ServletUtil.closeRollBackSession(session);
			WebPagesUtilities.redirectToErrorSite(servcont,
	    			"Sorry, but this poll exists already! Try with another question!");
			  return;
		} catch (InvalidPreferenceNumber e) {
			 ServletUtil.closeRollBackSession(session);
				WebPagesUtilities.redirectToErrorSite(servcont,
		    			"Sorry, but you can't set a negative or zero max preference number");
				return;
		}
		
		for (String option : options){
			try {
				if (!option.trim().isEmpty()) // ignore the empty ones..
				PollManager.insertOptionIntoPoll(session, poll, option, wuser);
			} catch (EmptyOptionTextException e) {
				 ServletUtil.closeRollBackSession(session);
				  WebPagesUtilities.redirectToErrorSite(servcont, "An option was empty, please check again");
				  return;
			} catch (DuplicateOptionException e) {
				 ServletUtil.closeRollBackSession(session);
				WebPagesUtilities.redirectToErrorSite(servcont,
		    			"Sorry, but you cannot have two options with the same name in a poll!");
				 return;
			}
		}
		
		session.getTransaction().commit();
		session.close();
		// show the success page of the redirection
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/Pages/Confirmations/PollCreationConfirmation.jsp");
		rd.include(request, response);
		
		
	}
}
