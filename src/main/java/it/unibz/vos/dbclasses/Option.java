package it.unibz.vos.dbclasses;

import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
public class Option implements Serializable {
	// for Hibernate
	// getters and setters
	// toString()
	// default constructor
	// implement Serializable
	@Id
	@SequenceGenerator(name="option_id_seq2", sequenceName="option_id_seq")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "option_id_seq2")
	@Column(name = "OPTION_ID")
	private Long ID;
	
	@Column(name = "OptionText")
	private String optionText;
	
	@ManyToOne
	@JoinColumn(name = "Poll_id")
	private Poll poll;
	
	@ManyToMany(fetch = FetchType.EAGER, mappedBy= "optionsVoted")
	private Set<WUser> usersThatHaveVoted = new HashSet<WUser>();

	public Set<WUser> getUsersThatHaveVoted (){
		return usersThatHaveVoted;
	}
	public Option(){
		
	}
	public Option( String optionText, Poll belongingPoll) throws EmptyOptionTextException{
		if (belongingPoll == null) throw new NullPointerException("The passed poll cannot be null");
		if (belongingPoll.getID() == null) throw new IllegalArgumentException("You cannot assign the option to a poll not saved on the database");
	
		this.poll = belongingPoll;
		setOptionText(optionText);
	}
/**
 * 
 * @return the id of the option
 */
	public Long getId() {
		return ID;
	}
	public Poll getPoll(){
		return poll;
	}
	
/**
 * 
 * @return returns the text of the option
 */
	public String getOptionText() {
		return optionText;
	}
/**
 * set the text of the option
 * @param optionText text of the option
 * @throws EmptyOptionTextException 
 */
	public void setOptionText(String optionText) throws EmptyOptionTextException {
		if (optionText == null) throw new NullPointerException("the OptionText cannot be null");
		if (optionText.trim().isEmpty()) throw new EmptyOptionTextException("Teh text of the Option cannnot be null");
		this.optionText = optionText;
	}
	
	@Override 
	    public boolean equals(Object obj) {
		if (obj == null) return false;
		if ( !( obj instanceof Option) ) return false;
		Option op = (Option) obj;
		return new EqualsBuilder().append(op.getId(), this.ID)
				.append(op.getOptionText(), this.getOptionText())
				.isEquals();
	}
	
	@Override 
	public int hashCode(){
		return new HashCodeBuilder(17,31)
		.append(this.ID).append(this.optionText).hashCode();
	}

	@Override
	public String toString() {
		return ID + " " + optionText;
	}


	
	
}
