package it.unibz.vos.dbclasses.exceptions;

public class InvalidPassowrdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1688251594016789756L;

	public InvalidPassowrdException(){
		super();
	}
	
	public InvalidPassowrdException(String message){
		super(message);
	}
}
