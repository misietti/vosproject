package it.unibz.vos.dbclasses.exceptions;

public class PollAlreadyExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public PollAlreadyExistsException() {
		
		super ();
	}
	
	
	public PollAlreadyExistsException(String msg) {
		
		super (msg);
	}
}
