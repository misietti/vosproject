package it.unibz.vos.dbclasses.exceptions;

public class UsernameAlreadyTakenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4679115836408178862L;

	public UsernameAlreadyTakenException(String theString){
		super(theString);
	}
}
