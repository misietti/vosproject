package it.unibz.vos.dbclasses.exceptions;

public class EmptyOptionTextException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -141953250365717103L;

	public EmptyOptionTextException(String msg)
	{
		super(msg);
	}
}
