package it.unibz.vos.dbclasses.exceptions;

public class InvalidEmailException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6232713255733562042L;
	public InvalidEmailException(){
		super();
	}
	public InvalidEmailException(String message){
		super(message);
	}
}
