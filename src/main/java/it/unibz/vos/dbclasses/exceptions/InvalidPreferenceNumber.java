package it.unibz.vos.dbclasses.exceptions;

public class InvalidPreferenceNumber extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidPreferenceNumber(String s)
	{
		super(s);
	}
}
