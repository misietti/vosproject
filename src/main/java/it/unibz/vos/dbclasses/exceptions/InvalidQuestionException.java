package it.unibz.vos.dbclasses.exceptions;

public class InvalidQuestionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3175918188226722596L;
	public InvalidQuestionException(){super();}
	public InvalidQuestionException(String message){
		super(message);
	}
	
	
}
