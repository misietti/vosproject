package it.unibz.vos.dbclasses.exceptions;

public class InvalidUserNameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5988840637507720423L;

	public InvalidUserNameException()
	{
		super();
	}
	
	public InvalidUserNameException(String message)
	{
		super(message);
	}
}
