package it.unibz.vos.dbclasses.exceptions;

public class InvalidDeadLineException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5268534463911692811L;

	public InvalidDeadLineException(String message){
		super(message);
	}
}
