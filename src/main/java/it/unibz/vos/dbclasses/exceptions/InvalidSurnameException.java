package it.unibz.vos.dbclasses.exceptions;

public class InvalidSurnameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3921272008669067452L;

	public InvalidSurnameException(){
		super();
	}
	
	public InvalidSurnameException(String message){
		super(message);
	}
}
