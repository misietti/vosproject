package it.unibz.vos.dbclasses.exceptions;

public class InvalidNameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7248931646555654754L;

	public InvalidNameException (){
		super();
	}
	
	public InvalidNameException(String message){
		super(message);
	}
}
