package it.unibz.vos.dbclasses;

import it.unibz.vos.dbclasses.exceptions.InvalidDeadLineException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.InvalidQuestionException;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.util.ObjectChecker;

import java.sql.Date;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Session;

@Entity
public class Poll {
	
	@Id
	@SequenceGenerator(name="poll_id_seq2", sequenceName="poll_id_seq")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "poll_id_seq2")
	@Column (name="POLL_ID")
	private Long ID;
	@Column (name="maxnpreferences")
	private int maxPreferences;
	@Column (name="deadlinedate")
	private Date deadlineDate;
	@Column (name="startdate")
	private Date startdate ;
	@Column (name="question")
	private String question;
	@Column (name="isPrivate")
	private boolean isPrivate;
	@Column (name="usercanaddoptions")
	private boolean userCanAddOptions;
	@Column (name="isClosed")
	private boolean isClosed;
	@Column (name="willNotify")
	private boolean willNotify; // the users of the closing
	// here for the many to many you have to set the name of
	// the container in the relation holding class
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="poll")
	private Set<Option> options = new HashSet<Option>();
	
	@ManyToOne
	@JoinColumn(name="WUSER_ID")
	private WUser creator;
	

	
	 @ManyToMany
    @JoinTable(name="CanVote", 
                joinColumns={@JoinColumn(name="POLL_ID")}, 
                inverseJoinColumns={@JoinColumn(name="WUSER_ID")})
	private Set<WUser> wuserAllowedToVote = new HashSet<WUser>();

	 
	 public boolean willNotifyUsers(){
		 return willNotify;
	 }
	 
	 public void doNotifyUsersAtClosing() {
		 willNotify = true;
	 }
	 
	 public void doNotNotifyUsersAtClosing(){
		 willNotify = false;
	 }
	 
	/**
	 * 
	 * @return the list of the WUsers allowed to vote on this poll
	 */
	public Set<WUser> getWuserAllowedToVote() {
		return wuserAllowedToVote;
	}
	/**
	 * @return the id of the users
	 */
	public Long getID() {
		return ID;
	}
	/**
	 * returns the maximum number of preferences
	 * @return
	 */
	public int getMaxPreferences() {
		return maxPreferences;
	}
	/**
	 * set the maximum number of preferences a given user can set
	 * @param maxPreferences the integer representing the max Preferences
	 * @throws InvalidPreferenceNumber 
	 */
	public void setMaxPreferences(int maxPreferences) throws InvalidPreferenceNumber {
		// cannot be lower than 1
		// write a really high number for other stuff.
		if ( maxPreferences < 1)
			throw new InvalidPreferenceNumber("You cannot have less than 1 preference");
		
		
		
		
		this.maxPreferences = maxPreferences;
	}
	/**
	 * @return the deadline date
	 */
	public Date getDeadlineDate() {
		return deadlineDate;
	}
	public void setDeadlineDate(Date dealineDate) throws InvalidDeadLineException {
		// TODO this shold be globalized to use the geographic time
		if (dealineDate.before(Calendar.getInstance().getTime())) throw new InvalidDeadLineException("The deadline cannot be before the current date");
		
		this.deadlineDate.setTime(dealineDate.getTime());
		
	}

	/**
	 * 
	 * @return the start date of the poll
	 */
	public Date getStartdate() {		return startdate;
	}
	/**
	 * 
	 * @return the text of the question
	 */
	public String getQuestion() 
{
		return question;
	}
	public void setQuestion(String question) throws InvalidQuestionException {
		if (question == null ) throw new NullPointerException("The question cannot be null");
		if	(question.isEmpty()) throw new InvalidQuestionException("The question cannot be empty");
		this.question = question;
	}
	public void makePrivate(){isPrivate = true;}
	public void makePublic(){isPrivate = false;}
	/**
	 * 

 * @return if the poll is private
	 */
	public boolean isPrivate() {
		return isPrivate;
	}
	/**
	 * enable the possibility for users to add options
	 */
	public void enableUserCanAddOptions(){
		userCanAddOptions = true;
	}
	/**
	 * disable the possibility for users to add options
	 */
	public void disableUserCanAddOptions(){
		userCanAddOptions = false;
	}
	/**
	 * 
	 * @return wether the user can add options or not.
	 */
	public boolean getCanUserAddOption(){
		return userCanAddOptions;
	}
	public WUser getCreator(){
		return this.creator;
	}
	
	public Set<Option> getOptions(){
		return options;
	} 
	
	public boolean isClosed(){
		return this.isClosed;
	}
	
	public void closePoll(){
		this.isClosed = true;
	}
	
	
	/**
 

* constructor for the building of the poll
 * @param question
 * @param isPrivate is 
 * @param dealineDate the dealine date
 * @param maxPreferences the maximum number of preferences each user can submit
 * @param creator the user that creates hte poll
 * @throws InvalidQuestionException 
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidPreferenceNumber 
 */
	public Poll(Session session, String question, boolean isPrivate, java.util.Date dealineDate, int maxPreferences, WUser creator, boolean userCanaAddOptions, boolean notifyUsers) throws InvalidQuestionException, PollAlreadyExistsException, InvalidPreferenceNumber{
		
		
		ObjectChecker.checkSession(session);
		boolean present = PollManager.isPollAlreadyPresent(session, question);
		
		if (present) {
			
			throw new PollAlreadyExistsException("There is already an active Poll that has the same name");
		}
		
		this.setQuestion(question);
		this.isPrivate = isPrivate;
		this.deadlineDate = new Date(dealineDate.getTime());
		this.setMaxPreferences(maxPreferences);
		this.creator = creator;
		this.startdate = new Date(Calendar.getInstance().getTimeInMillis()); 
		this.userCanAddOptions = userCanaAddOptions;
		this.isClosed = false;
		this.willNotify = notifyUsers;
	}
	
	/**
	 * CAUTION : DO NOT USE THIS, THIS IS ONLY FOR HIBERNATE USE
	 */
	
	public Poll(){
		
	}
	@Override 
	public int hashCode(){
		// the id is enough.
		// there are no two objects poll with the same id
		return new HashCodeBuilder(17,31).append(ID).hashCode();
	}
	
	@Override 
	public boolean equals(Object o){
		if (o == null) return false;
		if (!(o instanceof Poll)) return false;
		Poll other = (Poll)o;
		return new EqualsBuilder()
		.append(this.getID(), other.getID())
		.isEquals();
	}
	
	//public void 

	
}
