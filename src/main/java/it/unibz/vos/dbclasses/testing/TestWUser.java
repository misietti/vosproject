package it.unibz.vos.dbclasses.testing;

import static org.junit.Assert.*;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidEmailException;
import it.unibz.vos.dbclasses.exceptions.InvalidNameException;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.InvalidSurnameException;
import it.unibz.vos.dbclasses.exceptions.InvalidUserNameException;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.dbclasses.exceptions.UsernameAlreadyTakenException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.testing.util.TestingUtilities;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestWUser {
	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Session testSession = null;
	
	long userid = -1;
	long pollid = -1;
	
	
// before after and other util methods
	@Before
	public void mountUP() throws PollAlreadyExistsException, InvalidPreferenceNumber{
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		
		
		userNotSavedInDatabse = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
		pollNotSavedOnDatabase = TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession,testingUser);
		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,testingUser);
	
		userid = testingUser.getID();
		pollid = testingPoll.getID();
		

	}


	@After
	public void breakDown(){
		TestingUtilities.deleteALlTestGarbage(testSession);
		testSession.getTransaction().commit();
		testSession.close();
		
		
	}
	// creating and reading tests



	@Test
	public void createAndReadTest()   {
		// the before makes the stuff
		}


// email testing
	@Test
public void TestEmailmodification() throws InvalidEmailException{
		String newmail = "anewmail@jfg.kj";
			testingUser.setEmail(newmail);

	}

	@Test(expected=InvalidEmailException.class)
	public void TestEmailNull() throws InvalidEmailException{
	testingUser.setEmail(null);
	}

	@Test(expected=InvalidEmailException.class)
	public void TEstEmailEmpty() throws InvalidEmailException{
		testingUser.setEmail("");
	}

	@Test(expected=InvalidEmailException.class)
	public void TestInvalidEmail() throws InvalidEmailException{
		testingUser.setEmail("fdsafdsafdsafdsa");
	}
	
	@Test(expected=InvalidEmailException.class)
	public void TestInvalidEmai2() throws InvalidEmailException{
		testingUser.setEmail("fdsafdsa@fdsafdsa");
	}
	
	

	@Test (expected=InvalidUserNameException.class)
	public void TestSetUsernameforEmptyStriing() throws UsernameAlreadyTakenException, InvalidUserNameException{
			testingUser.setUsername(testSession,"");
	
	
	}
	
	@Test (expected=InvalidUserNameException.class)
	public void TestSetUsernameforNullString() throws UsernameAlreadyTakenException, InvalidUserNameException{

			testingUser.setUsername(testSession,null);


	}
	
	@Test (expected=InvalidUserNameException.class)
	public void TestSetUsernameforTooLongString() throws UsernameAlreadyTakenException, InvalidUserNameException{
			testingUser.setUsername(testSession,"12345678901234567890123456789012345678901234567890123456789012345678901234567890");
	}
	
	
	@Test (expected=UsernameAlreadyTakenException.class)
	public void TestSetUsernameForAlreadyPresentUsernames() throws InvalidNameException, InvalidUserNameException, UsernameAlreadyTakenException{
	 WUser wuser = TestingUtilities.insertWUserForTesting(testSession,"xxxcvbdfsgxxdxxxx", "fdsddxxa");
	 
	 wuser.setUsername(testSession,testingUser.getUsername());
	
	}
	@Test
	public void TestSetUsernameForYourOwnUsername() throws InvalidNameException, UsernameAlreadyTakenException, InvalidUserNameException{
		 testingUser.setUsername(testSession,testingUser.getUsername());
		
		}
	

	@Test (expected=InvalidNameException.class)
	public void TestSetNameEmpty() throws InvalidNameException{
		testingUser.setName("");
	}
	@Test (expected=InvalidNameException.class)
	public void TestSetNameNulll() throws InvalidNameException{
		testingUser.setName(null);
	}
	
	@Test (expected=InvalidNameException.class)
	public void TestSetNameToolong() throws InvalidNameException{
		testingUser.setName("dfsfdsfsdfdsafdsailfhdsalkjòfhdsfdsajiklòahfòlksdahfklòsdhòalkhfsdaòlk");
	}
	
	
	// SetSurname
	@Test (expected=InvalidSurnameException.class)
	public void TestSetSurameEmpty() throws InvalidSurnameException{
		testingUser.setSurname("");
	}
	@Test (expected=InvalidSurnameException.class)
	public void TestSetSurnameNulll() throws InvalidSurnameException  {
		testingUser.setSurname(null);
	}
	
	@Test (expected=InvalidSurnameException.class)
	public void TestSetSurnameToolong() throws InvalidSurnameException  {
		testingUser.setSurname("dfsfdsfsdfdsafdsailfhdsalkjòfhdsfdsajiklòahfòlksdahfklòsdhòalkhfsdaòlk");
	}
	
	//setPassword
	
	@Test (expected=InvalidPassowrdException.class)
	public void TestSetPasswordEmpty() throws InvalidPassowrdException  {
		testingUser.setPassword("");
	}
	@Test (expected=InvalidPassowrdException.class)
	public void TestSetPasswordNulll() throws InvalidPassowrdException    {
		testingUser.setPassword(null);
	}
	
	@Test (expected=InvalidPassowrdException.class)
	public void TestSetPasswordToolong() throws InvalidPassowrdException    {
		testingUser.setPassword("dfsfdsfsdfdsafdsailfhdsalkjòfhdsfdsajiklòahfòlksdahfklòsdhòalkhfsdaòlk");
	}
	
	@Test 
	public void TestGetters()
	{
		testingUser.getEmail();
		testingUser.getEnabledPolls();
		testingUser.getName();
		testingUser.getID();
		testingUser.getName();
		testingUser.getOptionVoted();
		testingUser.getPassword();
		testingUser.getSurname();
		testingUser.getUsername();
		
		testingUser.hashCode();
		
		assertEquals(testingUser.equals(null),false);
		assertEquals(testingUser.equals(testingPoll),false);
		assertEquals(testingUser.equals(testingUser),true);
	}
	
	

}
