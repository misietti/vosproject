package it.unibz.vos.dbclasses.testing;

import static org.junit.Assert.*;
import it.unibz.vos.dbclasses.Option;
import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.EmptyOptionTextException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.managementLayer.PollManager;
import it.unibz.vos.managementLayer.exceptions.OptionAlreadyVotedException;
import it.unibz.vos.managementLayer.exceptions.OptionNotVotedByTheUser;
import it.unibz.vos.managementLayer.exceptions.TooManyVotesException;
import it.unibz.vos.testing.util.TestingUtilities;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestOption {

	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Option testingOption = null;
	Session testSession = null;
	
	Long userid = new Long(-1);
	Long pollid = new Long(-1);
	
	
// before after and other util methods
	@Before
	public void mountUP() throws EmptyOptionTextException, PollAlreadyExistsException, InvalidPreferenceNumber{
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		userNotSavedInDatabse = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
	

		
	
		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		pollNotSavedOnDatabase = TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession,testingUser);
		testingPoll = TestingUtilities.insertPollForTesting(testSession, testingUser);
		testingOption = TestingUtilities.insertOptionForTesting(testSession, testingPoll);
		
		
		userid = testingUser.getID();
		pollid = testingPoll.getID();

	}


	@After
	public void breakDown(){
		TestingUtilities.deleteALlTestGarbage(testSession);

		testSession.getTransaction().commit();
		testSession.close();
		
	}
	/*
	 * Test that checks if it is possible to a new Option This Test has first to
	 * create a poll, because an option cannot exist without a poll.
	 */
	@Test
	public void testAdditionAndRead() throws EmptyOptionTextException {

		Long questionID = new Long(-1);


		Option theQuestion = new Option("This is the text of a question", testingPoll);
		testSession.save(theQuestion);
		questionID = theQuestion.getId();

		testSession.getTransaction().commit();
		testSession.close();
		// try to read it
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();

		Option option2 = (Option) testSession.get(Option.class, questionID);
		assertEquals(option2.getId(), questionID);
		// need to delete on both sides
		testingPoll.getOptions().remove(option2);
		testSession.delete(option2);


		testSession.getTransaction().commit();
		testSession.close();
	
		// see if they have been deleted
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		
		option2 = (Option) testSession.get(Option.class, questionID);
		
		assertEquals(option2, null);
	}

	/*
	 * test the setting of the optiontext
	 */
	@Test
	public void testSettingOfTheOptionText() throws EmptyOptionTextException {
		Long questionID = new Long(-1);

		Option theQuestion = new Option("This is the text of question", testingPoll);
	
		testSession.save(theQuestion);

		questionID = theQuestion.getId();

		testSession.getTransaction().commit();
		testSession.close();
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		
		theQuestion = (Option) testSession.load(Option.class, questionID);
		theQuestion.setOptionText("gino");
		testSession.save(theQuestion);
		
		testSession.getTransaction().commit();
		testSession.close();
		// 
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
		
		theQuestion = (Option) testSession.load(Option.class, questionID);
		assertEquals(theQuestion.getOptionText(),"gino");
		
		testingPoll.getOptions().remove(theQuestion);
		testSession.delete(theQuestion);
		
	}

	/*
	 * test the reading of wUsers that have voted
	 */
	@Test
	public void testReadingOfWUserThatHaveVoted() throws EmptyOptionTextException, PollAlreadyExistsException, InvalidPreferenceNumber, OptionAlreadyVotedException, TooManyVotesException, OptionNotVotedByTheUser {

		Poll x = TestingUtilities.insertPollForTesting(testSession, testingUser,"xxxvdashgdas");
		Option j = TestingUtilities.insertOptionForTesting(testSession, x, "fdsafdsaf");
		WUser user = TestingUtilities.insertWUserForTesting(testSession, "anotheruser", "safdsafjdsiaopf");
		PollManager.addVote(testSession,j, user);
		x = (Poll) testSession.get(Poll.class, x.getID());
		assertEquals(1,j.getUsersThatHaveVoted().size());
		assertEquals(true,j.getUsersThatHaveVoted().contains(user));
		PollManager.removeVote(testSession,j, user);
	}
	
	@Test
	public void testOptionEquaals() throws EmptyOptionTextException{
		
		Option anotherQuestion = TestingUtilities.insertOptionForTesting(testSession, testingPoll);

			assertEquals(false,testingOption.equals(null));
			assertEquals(false,testingOption.equals(testingPoll));
			assertEquals(true,testingOption.equals(testingOption));
			assertEquals(false,testingOption.equals(anotherQuestion));

		
	}
	
	@Test
	public void testToString()
	{
		assertEquals(testingOption.toString(),testingOption.getId() + " " + testingOption.getOptionText());

	}
	//public void testConstructor 
	
	@Test (expected=NullPointerException.class)
	public void testcontructorWithNullPoll() throws EmptyOptionTextException{
		new Option("a",null);
	}
	
	@Test (expected=EmptyOptionTextException.class)
	public void testcontructorWithemptyText() throws EmptyOptionTextException{
		new Option("",testingPoll);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testcontructorWithePéollNotSavedOnDatabase() throws EmptyOptionTextException, PollAlreadyExistsException, InvalidPreferenceNumber{
		Poll  x =TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession, testingUser, "abc abc");
		new Option("getPollForTestingNotSavedOnDatabase", x);
	}
	
	@Test (expected = NullPointerException.class)
	public void testsetOptionWithNull() throws EmptyOptionTextException{
		testingOption.setOptionText(null);
	}
}
