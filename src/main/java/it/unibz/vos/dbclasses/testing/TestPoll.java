package it.unibz.vos.dbclasses.testing;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Set;

import it.unibz.vos.dbclasses.Poll;
import it.unibz.vos.dbclasses.WUser;
import it.unibz.vos.dbclasses.exceptions.InvalidDeadLineException;
import it.unibz.vos.dbclasses.exceptions.InvalidPreferenceNumber;
import it.unibz.vos.dbclasses.exceptions.InvalidQuestionException;
import it.unibz.vos.dbclasses.exceptions.PollAlreadyExistsException;
import it.unibz.vos.hibernate.HibernateSession;
import it.unibz.vos.testing.util.TestingUtilities;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestPoll {
	WUser testingUser = null;
	WUser userNotSavedInDatabse = null;
	Poll testingPoll = null;
	Poll pollNotSavedOnDatabase = null;
	Session testSession = null;
	
    Long userid = null;
    Long pollid = null;
	
	
// before after and other util methods
	@Before
	public void mountUP() throws PollAlreadyExistsException, InvalidPreferenceNumber{
		
		testSession = HibernateSession.getInstance().openSession();
		testSession.beginTransaction();
	
		testingUser = TestingUtilities.insertWUserForTesting(testSession);
		userNotSavedInDatabse = TestingUtilities.getWuserForTestingNotSavedOnDatabase(testSession);
		testingPoll = TestingUtilities.insertPollForTesting(testSession,testingUser);
		
		userid = testingUser.getID();
		pollid = testingPoll.getID();
	
		
		pollNotSavedOnDatabase = TestingUtilities.getPollForTestingNotSavedOnDatabase(testSession,testingUser);

	}


	@After
	public void breakDown(){
		TestingUtilities.deleteALlTestGarbage(testSession);
		testSession.getTransaction().commit();
		testSession.close();

		
	}
// Actual tests



	/**
	 * This test checks if it is possible to create a poll with hibernate by
	 * creating it, saving it and then reading it. This is the version without
	 * using the comprehensive constructor
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidPreferenceNumber 
	 */

	@Test
	public void createPollAndRead() throws PollAlreadyExistsException, InvalidPreferenceNumber {
		
		
		Long pollId = new Long(-1);
		Long userId= -new Long(-1);
		// first add it.

		WUser creator = TestingUtilities.insertWUserForTesting(testSession,"fdsafsddscc","cfvxvxcvcxxvcv");
		if ( creator == null) Assert.fail("impossible to create the testing user");
		// first have to save the creator
		// second create the poll using the creator
		Poll thePoll = TestingUtilities.insertPollForTesting(testSession,creator,"xxxx TESTING");
		
		pollId = thePoll.getID();
		userId = creator.getID();
		
		
		// open another session

		Poll y = (Poll) testSession.get(Poll.class, pollId);
		assertEquals(pollId, y.getID());
		WUser creator2 = (WUser) testSession.get(WUser.class, userId);
		if (creator2 == null) Assert.fail("There should be an user here ");
	}

	/**
	 * Test the setting of the deadline before the today date.
	 * @throws PollAlreadyExistsException 
	 * @throws InvalidDeadLineException 
	 */
	@Test (expected=InvalidDeadLineException.class )
	public void TestDeadlineDateBeforeTodayDate() throws PollAlreadyExistsException, InvalidDeadLineException {
			testingPoll.setDeadlineDate(new java.sql.Date(new Date(0).getTime()));
	
	}

	// test the users that are allowed to vote that is add them and remove them
	/**t
	 * Test the addition of users that can vote,
	 * Then remove it
	 * @throws PollAlreadyExistsException 
	 */
	@Test
	public void AdditionInTheListOfUSersThanCanVote() throws PollAlreadyExistsException {
		// create the variables for containing the id's of the objects
		
		Long pollId =  new Long(-1);
		// first create the objects and add an user to the list
		
		WUser enabledUser = TestingUtilities.insertWUserForTesting(testSession, "fdsafdsafsdafsd", "xczvvcvcxzvxc");
		
		//Set<WUser> setOfUsers = thePoll.getWuserAllowedToVote();
		if (testingPoll.getWuserAllowedToVote() == null) Assert.fail("There is something strange, the list shouldn't be null");
		if (!testingPoll.getWuserAllowedToVote().isEmpty()) {
		Assert.fail("There shouldn't be anything in the list, please delete all present elemnts");
		}
		
		testingPoll.getWuserAllowedToVote().add(enabledUser);
		if (testingPoll.getWuserAllowedToVote().isEmpty()) Assert.fail("the insert doesn't work");		
		
	
		// save the id
		pollId = testingPoll.getID();
		
		// ok now that that is done , we can go on and try to load it up and delete
		// the user form the users allowed to vote NOTE : the list should be enabled only when
		// 												  the poll is private 
		
		testingPoll = null;
		testingPoll = (Poll) testSession.get(Poll.class,pollId);
		Set<WUser> newListOfUsers = testingPoll.getWuserAllowedToVote();
		if (newListOfUsers.isEmpty()) Assert.fail("the set is completely empty");
		if (!newListOfUsers.contains(enabledUser)) Assert.fail("The user has not been associated ");;
		testSession.merge(testingPoll);
		
		
		// see if the deletion of the user from the list worked
		
		
		
		testingPoll = null;
		testingPoll = (Poll) testSession.get(Poll.class,pollId);
		
		testingPoll.getWuserAllowedToVote().remove(enabledUser);
		if (!testingPoll.getWuserAllowedToVote().isEmpty()) Assert.fail("The deletion didn't work :D ");;
		testSession.merge(testingPoll);
		
		
	}

	/**
	 * Testing of the setting of private/public
	 * @throws PollAlreadyExistsException 
	 */
	@Test
	public void testTheSystemOfsettinnItPrivatePublic() throws PollAlreadyExistsException {


		testingPoll.makePublic();
		assertEquals(testingPoll.isPrivate(), false);

		testingPoll.makePrivate();
		assertEquals(testingPoll.isPrivate(), true);

		testingPoll.makePublic();
		assertEquals(testingPoll.isPrivate(), false);

		
		
		
	}

	/**
	 * test if the setting of the enabling the users to add options
	 * @throws PollAlreadyExistsException 
	 */
	@Test
	public void PrivateSetting() throws PollAlreadyExistsException {


		testingPoll.enableUserCanAddOptions();
		assertEquals(testingPoll.getCanUserAddOption(), true);

		testingPoll.disableUserCanAddOptions();
		assertEquals(testingPoll.getCanUserAddOption(), false);

		testingPoll.enableUserCanAddOptions();
		assertEquals(testingPoll.getCanUserAddOption(), true);

	}

	@Test
	public void testEnableDisableNotifyUsers(){
		testingPoll.doNotifyUsersAtClosing();
		assertEquals(testingPoll.willNotifyUsers(), true);

		testingPoll.doNotNotifyUsersAtClosing();
		assertEquals(testingPoll.willNotifyUsers(), false);

		testingPoll.doNotifyUsersAtClosing();
		assertEquals(testingPoll.willNotifyUsers(), true);
	}
	
	
	@Test 
	public void testVariousSettingsOfUserMexaPreferences() throws InvalidPreferenceNumber{
		testingPoll.setMaxPreferences(1);
		assertEquals(testingPoll.getMaxPreferences(), 1);
		testingPoll.setMaxPreferences(3);
		assertEquals(testingPoll.getMaxPreferences(), 3);
		testingPoll.setMaxPreferences(5);
		assertEquals(testingPoll.getMaxPreferences(), 5);
		testingPoll.setMaxPreferences(7);
		assertEquals(testingPoll.getMaxPreferences(), 7);

	}
	
	
	
	@Test (expected = InvalidPreferenceNumber.class)
	public void testInvalidMaxPreferences() throws InvalidPreferenceNumber{
		testingPoll.setMaxPreferences(0);
		assertEquals(testingPoll.getMaxPreferences(),0);

	}
	@Test
	public void testSettingAndGettingOfDeadLine() throws InvalidDeadLineException{
		java.sql.Date date1 = new java.sql.Date(new Date().getTime() + 10000000);
		java.sql.Date date2 = new java.sql.Date(new Date().getTime() + 1000000);

		testingPoll.setDeadlineDate(date1);
		assertEquals(testingPoll.getDeadlineDate(),date1);
		testingPoll.setDeadlineDate(date2);
		assertEquals(testingPoll.getDeadlineDate(),date2);
	}
	
	@Test (expected = InvalidDeadLineException.class)
	public void testSettingAndGettingOfDeadLineINVALID() throws InvalidDeadLineException{
		java.sql.Date date1 = new java.sql.Date(new Date().getTime() - 10000000);

		testingPoll.setDeadlineDate(date1);
	}
	
	@Test
	public void simpleTestCallOfGetStartDate()
	{
		testingPoll.getStartdate();

	}
	
	@Test
	public void testOfComparingPolls() throws PollAlreadyExistsException, InvalidPreferenceNumber
	{
		assertEquals(false,testingPoll.equals(3));
		assertEquals(false,testingPoll.equals(null));
		assertEquals(true,testingPoll.equals(testingPoll));
		
		Poll anotherPoll = TestingUtilities.insertPollForTesting(testSession, testingUser, "abc ------");
		assertEquals(false,testingPoll.equals(anotherPoll));


	}
	
	@Test (expected = PollAlreadyExistsException.class)
	public void testConstructorWithAlreadyPresentPoll() throws InvalidQuestionException, PollAlreadyExistsException, InvalidPreferenceNumber {
		new Poll(testSession, testingPoll.getQuestion(), false, new java.sql.Date(new Date().getTime() + 30000000), 2, testingUser, true, true);
	}
	
	
	@Test
	public void callHash()
	{
		testingPoll.hashCode();
	}
	
	@Test (expected=InvalidQuestionException.class)
	public void SetQuestionEmpty() throws InvalidQuestionException{
		testingPoll.setQuestion("");
	}
	
	@Test(expected=NullPointerException.class)
	public void SetQuestionNull() throws InvalidQuestionException{
		testingPoll.setQuestion(null);
	}
	//TODO test if a poll can be created with an user that hasn't been saved on the database yet
	//P.S Shouldn't
}
