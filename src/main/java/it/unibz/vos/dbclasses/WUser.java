package it.unibz.vos.dbclasses;

import it.unibz.vos.dbclasses.exceptions.InvalidEmailException;
import it.unibz.vos.dbclasses.exceptions.InvalidNameException;
import it.unibz.vos.dbclasses.exceptions.InvalidPassowrdException;
import it.unibz.vos.dbclasses.exceptions.InvalidSurnameException;
import it.unibz.vos.dbclasses.exceptions.InvalidUserNameException;
import it.unibz.vos.dbclasses.exceptions.UsernameAlreadyTakenException;
import it.unibz.vos.managementLayer.WUserManager;
import it.unibz.vos.util.EmailChecker;
import it.unibz.vos.util.Hasher;
import it.unibz.vos.util.ObjectChecker;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Session;


@Entity
public class WUser {
	@SuppressWarnings("unused")
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	
	@Id
	@Column( name="WUSER_ID", nullable = false, unique=true)
	@SequenceGenerator(name="user_id_seq2",
	sequenceName="user_id_seq")
	// using auto instead of using SEQUENCE as it uses a strange thing
	// at gets hunged ids.
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "user_id_seq2")
	Long id; 
	@Column(name="username", nullable = false, length = 40)
	String username;
	@Column(name="name", nullable = false,length = 40)
	String name;
	@Column(name="surname", nullable = false,length = 40)
	String surname;
	@Column (name="password", nullable = false,length = 64)
	String password ;
	@Column (name="email", nullable = false,length = 40, unique = true)
	String email;
	
	@ManyToMany(fetch = FetchType.EAGER, mappedBy="wuserAllowedToVote")
	    private Set<Poll> enabledPolls = new HashSet<Poll>();
	
	 @ManyToMany
	 @JoinTable (name="Chooses", joinColumns={@JoinColumn (name="WUSER_ID")}, 
	 			inverseJoinColumns={@JoinColumn(name="OPTION_ID")})
	 private Set<Option> optionsVoted = new HashSet<Option>();
	 
	
	
	public Long getID() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(Session session,String username) throws UsernameAlreadyTakenException, InvalidUserNameException {
		if (username == null) throw new InvalidUserNameException("the username cannot be null");
		if (username.trim().isEmpty()) throw new InvalidUserNameException("the username cannot be empty");
			if (username.length() < 40) {
				// TODO maybe pass the session as the paramenter, as there might be a problem with the 
				// merging of the object in two different sessions...
				// that is an object might be exist here and not there...
				
				WUser userToCheckAgainst= WUserManager.getWUserFromUsername(session,username);
				
				if (userToCheckAgainst != null)
				{
					if(!userToCheckAgainst.equals(this)){
					
					if (userToCheckAgainst.getUsername().compareTo(username ) == 0)
					throw new UsernameAlreadyTakenException("the username has already been taken by another user, please insert another one");
					}
				}
				
				this.username = username;
			} else
			{
				throw new InvalidUserNameException("the username cannot be longer than 40 characters");
			}
		
		

	}
	public String getName() {
		return name;
	}
	public void setName(String name) throws InvalidNameException {
		if (name == null) throw new InvalidNameException("the name cannot be null");
		if (name.trim().isEmpty()) throw new InvalidNameException("the name cannot be empty");
		if (name.length() > 40) throw new InvalidNameException("the name cannot be longer than 40 characters");
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) throws InvalidSurnameException {
		if (surname == null) throw new InvalidSurnameException("the surname cannot be null");
		if (surname.trim().isEmpty()) throw new InvalidSurnameException("the surname cannot be empty");
		if (surname.length() > 40) throw new InvalidSurnameException("the surname be longer than 40 characters");
		this.surname = surname;
	}
	public String getPassword() {
		return password; 
	}
	public void setPassword(String password) throws InvalidPassowrdException  {
		
		if (password == null) throw new InvalidPassowrdException("the password cannot be null");
		if (password.trim().isEmpty()) throw new InvalidPassowrdException("the password cannot be empty");
		if (password.length() > 40) throw new InvalidPassowrdException("the password be longer than 40 characters");
	    password = Hasher.getHashedString(password);
	    
	    this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) throws InvalidEmailException {

		
		if (email == null) throw new InvalidEmailException("the email cannot be null ");
		if (email.trim().isEmpty()) throw new InvalidEmailException("the email cannot be empty");
		EmailChecker y = EmailChecker.getInstance();
		if (!y.validate(email)) throw new InvalidEmailException("The string passed is not a valid mail");
		this.email = email;
	}

	public  Set<Poll> getEnabledPolls()
	{ 
		return enabledPolls;
	}
		
	public Set<Option> getOptionVoted(){
		return optionsVoted;
	}
	
	/**
	 * Initailizes the WUser 
	 * @param username
	 * @param name
	 * @param surname
	 * @param password
	 * @param email
	 * @throws UsernameAlreadyTakenException 
	 * @throws InvalidUserNameException 
	 * @throws InvalidNameException 
	 * @throws InvalidSurnameException 
	 * @throws InvalidPassowrdException 
	 * @throws InvalidEmailException 
	 * @throws IllegalArgumentException 
	 */
	public WUser(Session session,String username, String name, String surname, String password, String email) throws UsernameAlreadyTakenException, InvalidUserNameException, InvalidNameException, InvalidSurnameException, InvalidPassowrdException, InvalidEmailException
	{
		ObjectChecker.checkSession(session);
		this.setUsername(session,username);
		this.setName(name);
		this.setSurname(surname);
		this.setPassword(password);
		this.setEmail(email);
	}
	
	public WUser() {
	}
	
	@Override
	public int hashCode(){
		// the id  is enough
		return new HashCodeBuilder(17,31)
		.append(this.getID())
		.hashCode();
	}
	
	@Override
	public boolean equals(Object o){
		if (o == null) return false;
		if (!(o instanceof WUser)) return false;
		WUser other = (WUser) o;
		return new EqualsBuilder()
		.append(this.getID(),other.getID())
		.isEquals();
	}
	
	
	
}
