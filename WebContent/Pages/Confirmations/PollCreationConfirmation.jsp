<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   
<!DOCTYPE html>
 <html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Poll Created!</title>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/homepage.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">


	<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-main-collapse">
				<i class="fa fa-bars"></i>
			</button>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div
			class="collapse navbar-collapse navbar-right navbar-main-collapse">
			<ul class="nav navbar-nav">
				<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
				<li class="hidden"><a href="#page-top"></a></li>
				<li><a class="page-scroll" href="#all">Home</a></li>
				<li><a class="page-scroll" href="#voted">Your Polls</a></li>
				<li><a
					href="${pageContext.request.contextPath}/Pages/newPoll.jsp">Create
						New Poll</a></li>
				<li><a
					href="${pageContext.request.contextPath}/Pages/settings.jsp">Settings</a></li>
				<li><a href="${pageContext.request.contextPath}/Logout">Logout</a></li>
			</ul>
		</div>
		
		<!-- /.navbar-collapse -->
	</div>
	</nav>
		<header class="intro">
	<div class="intro-body">
		<div class="container">
		<br/>		<br/>
<h1>Your poll has been created :) </h1>
<p>THANK YOU FOR YOUR HELP!</p>
<p> Our team of scientist cats are trying to figure out what you've written... if you want to skip the furry waiting, click here : <a
						href="${pageContext.request.contextPath}/HomePageServlet">
						No cats!</a>  </p>
<div><img src="${pageContext.request.contextPath}/img/catsWitComputer.jpg"  width="300" height="300" /></div>
</div></div></header>
	<script type="text/javascript">
		function redirect() {
			window.location
					.replace("${pageContext.request.contextPath}/HomePageServlet");

		}
		window.setTimeout("redirect()", 5000);
	</script>
</body>
</html>
