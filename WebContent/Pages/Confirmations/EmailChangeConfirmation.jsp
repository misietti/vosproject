<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Success!</title>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/homepage.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</head>
<body>
<header class="intro">
		<section id="all" class="container all-section text-center">
<h1>Success!</h1>
<p>Your mail has been changed to  <c:out value="${email}"></c:out></p>
<p>Redirecting back to your homepage...</p>
</section>
</header>
</body>
	<script type="text/javascript">
		function redirect() {
			window.location
					.replace("${pageContext.request.contextPath}/HomePageServlet");

		}
		window.setTimeout("redirect()", 4000);
	</script>
</html>