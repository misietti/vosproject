<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/homepage.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">



	<header class="intro">
		<div class="intro-body">
			<div class="container">
				<div>
					<br /> <br /> <br />
				<h1>Your vote has been confirmed, we'd like to thank you for
					you vote</h1>
				<p>Our dedicated team of monkeys is trying to redirect you
					back to the homepage</p>
				<p>
					If they are busy with bananas you can click here : <a
						href="${pageContext.request.contextPath}/HomePageServlet">
						Skip the monkeys :(</a>
				</p>

				<div > <img src="${pageContext.request.contextPath}/img/government-chimps.jpg" height="300" width="450"/></div >
			</div>
		</div>
</div>
			
	</header>

</body>
<script type="text/javascript">
	function redirect() {
		window.location
				.replace("${pageContext.request.contextPath}/HomePageServlet");

	}
	window.setTimeout("redirect()", 5000);
</script>
</html>
