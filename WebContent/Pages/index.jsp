<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>VosProject</title>

 <!-- Bootstrap Core CSS --> 
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<%-- <link href="${pageContext.request.contextPath}/css/bootstrap-theme.css" rel="stylesheet"> --%>

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/grayscale.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css">



<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css"> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->

	<!-- jQuery --> 
	<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

 	<!-- Bootstrap Core JavaScript --> 
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

 	<!-- Plugin JavaScript --> 
	<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>


<c:if test="${not empty success}" >
<script type="text/javascript">
$(document).ready(function(){
$("#myModal").modal('show');}); $('#btnYes').click(function()
{var link = $('#myModal').data('http://localhost:8080/VOSProject/Login');location.href = link; $('#myModal').modal('hide');});
</script>
</c:if>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">



<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Registration</p>
                <p class="text-success"><small>Congratulations! Now you are registered!</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="${pageContext.request.contextPath}/Pages/login.jsp" class="btn btn-default">Login</a>
            </div>
        </div>
    </div>
</div>




	<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div
				class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
					<li class="hidden"><a href="#page-top"></a></li>
					<li><a class="page-scroll" href="${pageContext.request.contextPath}/Pages/index.jsp">VOS</a></li>
					<li><a class="page-scroll" href="#about">About</a></li>
					<li><a class="page-scroll" href="#download">Begin</a></li>
					<li><a class="page-scroll" href="#contact">Contact</a></li>
					<li><a class="page-scroll" href="${pageContext.request.contextPath}/HomePageServlet">Home</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>

	<!-- Intro Header -->
	<header class="intro">
		<div class="intro-body">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1 class="brand-heading">VOS</h1>
						<p class="intro-text">
							A Free Poll creation and management system.<br>Created by
							Aurelia Pagano and Michal Kamil Bogdanowicz
						</p>
						<a href="#about" class="btn btn-circle page-scroll"> <i
							class="fa fa-angle-double-down animated"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- About Section -->
	<section id="about" class="container content-section text-center">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<h2>About VOS</h2>
				<p>
					VOS is a free voting system online.
					It can be used to create new polls online and to share them with 
					the whole world or decide to make them private!
				</p>
				<p>
					This theme features stock photos by <a
						href="http://gratisography.com/">Gratisography</a> and
						Bootstrap theme called Grayscale.
				</p>
				
			</div>
		</div>
	</section>

	<!-- Download Section -->
	<section id="download" class="content-section text-center">
		<div class="download-section">
			<div class="download-section">
				<div class="col-lg-8 col-lg-offset-2">
					<h2>Begin</h2>
					<p>You can either register or login</p>
					<ul  class="list-inline">
						<li><a
							href="${pageContext.request.contextPath}/Pages/registration.jsp"
							class="btn btn-default btn-lg">Register</a></li>
						<li><a
							href="${pageContext.request.contextPath}/Pages/login.jsp"
							class="btn btn-default btn-lg">Login</a></li>
					</ul>

				</div>
			</div>
		</div>
	</section>

	<!-- Contact Section -->
	<section id="contact" class="container content-section text-center">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<h2>Contact Us!</h2>
				<p>Feel free to email us to provide some feedback on our
					website, give us suggestions for new templates and themes, or to
					just say hello!</p>
				<p>
					<a href="mailto:feedback@startbootstrap.com">feedback@vos.com</a>
				</p>
				<ul class="list-inline banner-social-buttons">
					<li><a href="https://twitter.com/AureliaPagano"
						class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i>
							<span class="network-name">Twitter</span></a></li>
					<li><a
						href="https://bitbucket.org/misietti/vosproject"
						class="btn btn-default btn-lg"><i class="fa fa-bitbucket fa-fw"></i>
							<span class="network-name">BitBucket</span></a></li>
					<li><a href="https://plus.google.com/113223710338642954899/posts"
						class="btn btn-default btn-lg"><i
							class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>
					</li>
				</ul>
			</div>
		</div>
	</section>


	<!-- Footer -->
	<footer>
		<div class="container text-center">
			<p>Copyright &copy;  VOS 2015</p>
		</div>
	</footer>

</body>

</html>
