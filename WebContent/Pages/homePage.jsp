<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html lang="en">
<head>
<!-- This is the home page -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>VosProject</title>

<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/homepage.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>
<script src="${pageContext.request.contextPath}/js/sorter.js"></script>

</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">


	<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div
				class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
					<li class="hidden"><a href="#page-top"></a></li>
					<li><a class="page-scroll" href="#all">Available Polls</a></li>
					<li><a class="page-scroll" href="#voted">Voted Polls</a></li>
					<li><a class="page-scroll" href="#personal">Your Polls</a></li>
					<li><a
						href="${pageContext.request.contextPath}/Pages/newPoll.jsp">Create
							New Poll</a></li>
					<li><a
						href="${pageContext.request.contextPath}/UserSettingsRequestServlet">Settings</a></li>
					<li><a href="${pageContext.request.contextPath}/Logout">Logout</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>


	<!-- Available Polls -->

	<header class="intro">
		<section id="all" class="container all-section text-center">

			<br></br> <br></br> <br></br>
			<h2>Available Polls</h2>
			<br />

			<div class="container">
<c:if test="${not cookie.containsKey('name')}">
 	<c:set var="errorLoginMessage"
		value="You cannot enter this page like this! Use the vote button in the homepage ! :)"
		scope="request" />
  <c:redirect url="/Pages/login.jsp"/>
 
  
 </c:if>

			
				<table class="table" id="table1">
					<thead>
						<tr>
							<th>Question &#x25B2; &#x25BC;</th>
							<th>Creation Date &#x25B2; &#x25BC;</th>
							<th>Deadline &#x25B2; &#x25BC;</th>
							<th>Author &#x25B2; &#x25BC;</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${pollsthatcanbevoted}" var="item">

							<tr>
								<td><c:out value="${ item.question}" /></td>
								<td><c:out value="${item.startdate}" /></td>
								<td><c:out value="${item.dealineDate}" /></td>
								<td><c:out value="${item.creatorUsername}" /></td>
								<td>
									<form method="post"
										action="${pageContext.request.contextPath}/VoteRequestServlet">
										<input type="hidden" name="poll_id" value="${item.ID}">
										<input class="VotingButton" type="submit" value="Vote!" />
									</form>
								</td>
							</tr>
						</c:forEach>
						<tr></tr>
					</tbody>
				</table>
			</div>
			<br></br>

		</section>

	</header>


	<!-- Voted Polls Section -->
	<section id="voted" class="container voted-section text-center">
		<h2>Voted Polls</h2>
		<br />
		<div class="container">
			<table class="table" id="table2">
				<thead>
					<tr>
						<th>Question &#x25B2; &#x25BC;</th>
						<th>Start Date &#x25B2; &#x25BC;</th>
						<th>Deadline &#x25B2; &#x25BC;</th>
						<th>Author &#x25B2; &#x25BC;</th>
						<th>N. Users Voted &#x25B2; &#x25BC;</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${votedpolls}" var="item">

						<tr>
							<td><c:out value="${ item.question}" /></td>
							<td><c:out value="${item.startdate}" /></td>
							<td>
								<!-- 									check if is closed, if it is just write closed, if there still is a deadline write it -->
								<c:choose>
									<c:when test="${item.isClosed()}"> Closed! </c:when>
									<c:otherwise>
										<c:out value="${item.dealineDate}" />
									</c:otherwise>
								</c:choose>


							</td>
							<td><c:out value="${item.creatorUsername}" /></td>
							<td><c:out value="${item.numberOfUserVotes}" /></td>
							<td>
								<form method="post"
									action="${pageContext.request.contextPath}/VisualizeResult">
									<input type="hidden" name="poll_id" value="${item.ID}">
									<input class="ResultVoting" type="submit" value="Show Results" />
								</form>
							</td>
						</tr>
					</c:forEach>
					<tr></tr>
				</tbody>
			</table>
		</div>
		<br></br>

	</section>



	<!-- Personal polls Section -->
	<section id="personal" class="container personal-section text-center">
		<br /> <br />
		<h2>Polls created by you</h2>
		<br />
		<div class="container">
			<table class="table" id="table3">
				<thead>
					<tr>
						<th>Question &#x25B2; &#x25BC;</th>
						<th>Start Date &#x25B2;&#x25BC;</th>
						<th>Deadline &#x25B2; &#x25BC;</th>
						<th>Author &#x25B2; &#x25BC;</th>
						<th>N. Users Voted &#x25B2; &#x25BC;</th>
						<th>Action</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${personalpolls}" var="item">

						<tr>
							<td><c:out value="${item.question}" /></td>
							<td><c:out value="${item.startdate}" /></td>
							<td><c:choose>
									<c:when test="${item.isClosed()}"> Closed! </c:when>
									<c:otherwise>
										<c:out value="${item.dealineDate}" />
									</c:otherwise>
								</c:choose></td>
							<td><c:out value="${item.creatorUsername}" /></td>
							<td><c:out value="${item.numberOfUserVotes}" /></td>
							<td>
								<form method="post"
									action="${pageContext.request.contextPath}/VisualizeResult">
									<input type="hidden" name="poll_id" value="${item.ID}">
									<input type="submit" value="Show Results"
									<c:choose> 
												 <c:when test="${(item.numberOfUserVotes == 0)}"> class="DisabledButton1" disabled="true" </c:when> 
												 <c:otherwise>  class="ResultVoting" </c:otherwise>
												</c:choose> 
												/>
									
									
									
								</form>
							</td>

							<td>

								<form method="post"
									action="${pageContext.request.contextPath}/PollSettingsServlet">
									<input type="hidden" name="poll_id" value="${item.ID}">
									<input type="submit" value="Poll Settings"
										<c:choose> 
												 <c:when test="${item.isClosed()}"> class="DisabledButton1" disabled="true" </c:when> 
												 <c:otherwise> class="thePurpleOne"</c:otherwise>
												</c:choose> />

								</form>
							</td>
						</tr>
					</c:forEach>
					<tr></tr>
				</tbody>
			</table>
		</div>

		<br></br>

	</section>








</body>

<script>
	$.tablesorter.addParser({
		id : 'daymonthYear',
		is : function(s) {
			return false;
		},
		format : function(s) {
		 var input = s;
			if (input == "Closed!")
				{
				return 0 || '';
				}
		
			var from = input.split("/");
			return new Date(from[2], from[1] - 1, from[0]).getTime() || '';
		},
		type : 'Numeric'
	});

	$(document).ready(function() {
		
		$('#table1').tablesorter({
			headers : {
				1 : {
					sorter : 'daymonthYear'
				},
				2 : {
					sorter : 'daymonthYear'
				}
			}
		});
		$('#table2').tablesorter({
			headers : {
				1 : {
					sorter : 'daymonthYear'
				},
				2 : {
					sorter : 'daymonthYear'
				}
			}
		});
		$('#table3').tablesorter({
			headers : {
				1 : {
					sorter : 'daymonthYear'
				},
				2 : {
					sorter : 'daymonthYear'
				}
			}
		});
		
		
		
// 		$("#table1").tablesorter();
// 		$("#table2").tablesorter();
// 		$("#table3").tablesorter();
	});
</script>








</html>