<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >

<head>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">

<link href="${pageContext.request.contextPath}/css/recovery.css" rel="stylesheet">
<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>


</head>

<div style="text-align: center;">
	<div
		style="box-sizing: border-box; display: inline-block; width: auto; max-width: 480px; background-color: #FFFFFF; border: 2px solid #0361A8; border-radius: 5px; box-shadow: 0px 0px 8px #0361A8; margin: 50px auto auto;">
		<div
			style="background: #AB6565a ; border-radius: 5px 5px 0px 0px; padding: 15px;">
			<span
				style="font-family: verdana, arial; color: #379393; font-size: 1.00em; font-weight: bold;">Enter your e-mail for credentials recovery!</span>
		</div>
		<div style="background:; padding: 15px">
			

			<form method="post" action="${pageContext.request.contextPath}/PasswordRecovery"
				name="aform" target="_top">
				<input type="hidden" name="action" value="login"> <input
					type="hidden" name="hide" value="">
				<table class='center'>
					<tr>
						<td>E-mail:</td>
						<td><input type="email" name="email" required></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" value="Submit"></td>
					</tr>
					<tr>
						<td colspan=2>&nbsp;</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>