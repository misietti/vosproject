<%@page import="it.unibz.vos.util.WebPagesUtilities"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	// if user is not logged redirect!
	WebPagesUtilities.redirectIfNotLogged(response, request,
			this.getServletContext());
%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>New Poll</title>

<!-- Bootstrap Core CSS -->
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/newPoll.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/bootstrap-datepicker.css" />


<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


<!-- Bootstrap DatePicker JavaScript -->
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.0.2/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js"></script>

<!-- Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>

</head>
<body>

	<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->

			<div
				class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
					<li class="hidden"><a href="#page-top"></a></li>

					<li><a class="brand-heading"
						href="${pageContext.request.contextPath}/Pages/index.jsp">VOS</a></li>
					<li><a
						href="${pageContext.request.contextPath}/HomePageServlet">Home</a></li>
					<li><a
						href="${pageContext.request.contextPath}/Pages/newPoll.jsp">Create
							New Poll</a></li>
					<li><a
						href="${pageContext.request.contextPath}/UserSettingsRequestServlet">Settings</a></li>
					<li><a
						href="${pageContext.request.contextPath}/Pages/logout.jsp">Logout</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>
	<br></br>


	<div class="container">
		<form role="form" method="post"
			action="${pageContext.request.contextPath}/NewPollServlet">
			
			<div class="form-group">
				<br></br> <br></br> <br></br> <label for="email">Question:</label> <br></br>
				<input required type="text" class="form-control" id="email"
					placeholder="Enter question" name="question">
			</div>

			<label for="email">Options (At least two are required)</label> <br></br>


			<script>
				var number_of_rows = 0;
				$(function() {
					$(document)
							.on(
									'focus',
									'div.form-group-options div.input-group-option:last-child input',
									function() {
										var sInputGroupHtml = $(this).parent()
												.html();
										var sInputGroupClasses = $(this)
												.parent().attr('class');
										$(this).parent().parent().append(
												'<div class="'+sInputGroupClasses+'">'
														+ sInputGroupHtml
														+ '</div>');
									});

					$(document).on('click',
							'div.form-group-options .input-group-addon-remove',
							function() {
								$(this).parent().remove();
							});
				});
			</script>

			<div class="container">
				<div class="row">

					<div
						class="form-group form-group-options col-xs-11 col-sm-8 col-md-4">


						<input required type="text" name="option[]" class="form-control"
							placeholder="Type the new option..."> <input required
							type="text" name="option[]" class="form-control"
							placeholder="Type the new option...">


					</div>
				</div>
			</div>

			<label for="email">Additional Options</label> <br></br>
			<div class="container">
				<div class="row">

					<div
						class="form-group form-group-options col-xs-11 col-sm-8 col-md-4">
						<div class="input-group input-group-option col-xs-12">
							<input type="text" name="option[]" class="form-control"
								placeholder="Type the new option..."> <span
								class="input-group-addon input-group-addon-remove"> <span
								class="glyphicon glyphicon-remove"></span>
							</span>
						</div>
					</div>
				</div>
			</div>




			<label for="email">Settings</label> <br></br>




			<div class="form-group">
				<div class="col-xs-2">
					<label for="ex1">Number of max. options</label> <input required
						class="form-control" name="maxOptions" min="1" type="number">
				</div>
			</div>

			
			<br /><br /><br /><br />
			<div class="checkbox">
				<label><input type="checkbox" name="allowedToAddOptions">
					Allow users to add options</label>
			</div>
			<div class="checkbox">
				<label><input type="checkbox" name="isPollPrivate">The
					poll is private</label>
			</div>
			<div class="checkbox">
				<label><input type="checkbox" name="NotifyUSersWhenClosed">
					Notify users when closed</label>
			</div>
	


	<div class="container">
		<label>Deadline</label>
		<div class="row">
			<div class='col-sm-6'>
				<div class="form-group">
					<div class='input-group date' id='datepicker'>
						<input required type='text' class="form-control"
							name="deadlineDate" /> <span class="input-group-addon"> <span
							class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
			</div>

			<script type="text/javascript">
				var date = new Date();
				date.setDate(date.getDate() - 1);
				$(function() {
					$('#datepicker').datepicker({
						clearBtn : true,
						format : "dd/mm/yyyy",
						startDate : new Date()
					});

				});
			</script>
		</div>
	</div>

	<br></br>
	<button type="submit" class="btn btn-default">Submit</button>
	</form>
	</div>
</body>
</html>
