<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="javax.script.ScriptEngineManager" %>
	<%@page import="javax.script.ScriptEngine" %>
<!DOCTYPE html>


<html>
<head>
<link href="${pageContext.request.contextPath}/css/registration.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">	
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.css" rel="stylesheet">	
<link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrapValidator.css" rel="stylesheet">

<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/js/jquery-1.10-2.min.js"></script>
	
	<!-- Bootstrap Core JavaScript and Validation -->
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>


		


</head>




<body>


<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to save changes you made to document before closing?</p>
                <p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div
				class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
					<li class="hidden"><a href="#page-top"></a></li>
					<li><a href="${pageContext.request.contextPath}/Pages/index.jsp">VOSProject</a></li>
					<li><a href="${pageContext.request.contextPath}/Pages/login.jsp">Login</a></li>
			
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>
	
	<div id="fullscreen_bg" class="fullscreen_bg" >

<div class="form-group">

	<form  id="identicalForm"  class=" form-signin" action="${pageContext.request.contextPath}/RegistrationServlet" method="post"
	 data-fv-framework="bootstrap"
    data-fv-message="This value is not valid"
    data-fv-icon-valid="glyphicon glyphicon-ok"
    data-fv-icon-invalid="glyphicon glyphicon-remove"
    data-fv-icon-validating="glyphicon glyphicon-refresh">
		
		<h1 class="form-signin-heading text-muted">Register</h1>
		 <label for="name" class="control-label">Name</label>
		<input type="text" name="name" class="form-control"  id="name" 
		data-fv-notempty="true" data-fv-notempty-message="The last name is required and cannot be empty" placeholder="Name" required autofocus>
		 <label for="surname" class="control-label">Surname</label>
		<input type="text" name="surname" class="form-control" id="surname" placeholder="Surname" required autofocus>
		 <label for="surname" class="control-label">Username</label>
		<input type="text" name="username" class="form-control" id="username" placeholder="Username" required autofocus>
		 <label for="inputEmail" class="control-label">Email</label>
		<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Bruh, that email address is invalid" required autofocus>
		<label for="inputPassword" class="control-label">Password</label>
		<input type="password"  name="password" class="form-control" data-minlength="6" id="password" placeholder="Password" required autofocus>
		<label for="inputPasswordConfirm" class="control-label">Retype Password</label>
		<input type="password" class="form-control" name="password2" placeholder="Retype password" data-fv-identical="true" required autofocus>
		
		



			<%
			
		
		String errorMessage = (String) request.getAttribute("registerError");
		if (errorMessage != null) out.println("<p>" + errorMessage +" </p>");
		
	
		
		%>
		
		
		
		<button  class="btn btn-lg btn-primary btn-block" type="submit" >
			Register
		</button>
					
	</form>

	</div>
	</div>

</body>
</html>