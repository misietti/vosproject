<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">	
<link href="${pageContext.request.contextPath}/css/settings.css" rel="stylesheet">

<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/js/jquery-1.10-2.min.js"></script>
<!-- Bootstrap Core JavaScript-->
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Settings</title>
</head>



<body>


<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div
				class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
					<li class="hidden"><a href="#page-top"></a></li>
					<li><a href="${pageContext.request.contextPath}/Pages/index.jsp">VOSProject</a></li>
					<li><a href="${pageContext.request.contextPath}/HomePageServlet">Homepage</a></li>
			
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>

<div class="form-group">
	<h1 class="form-signin-heading text-muted">Invite user by e-mail</h1>
	<form class="form-signin" method="post"
		action="${pageContext.request.contextPath}/InvitationConfirmationServlet">
	<input type="hidden" name="pollID" value="<c:out value="${pollId}" />"  >
		<p> Email </p>
		<input type="text" class="form-control" name="email">
		<br />
			<button  class="btn btn-lg btn-primary btn-block" type="submit" >
			Submit
		</button>
	</form>
	<br />
	<h1 class="form-signin-heading text-muted">Invite user by username</h1>
		<form class="form-signin" method="post"
		action="${pageContext.request.contextPath}/InvitationConfirmationServlet">
		 <input type="hidden" name="pollID" value="<c:out value="${pollId}" />">
		<p> Username </p>
		<input type="text" class="form-control" name="username">
		<br />
			<button  class="btn btn-lg btn-primary btn-block" type="submit" >
			Submit
		</button>
	</form>
</div>
</body>
</html>