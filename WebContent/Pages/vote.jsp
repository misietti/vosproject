<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Voting Page</title>
<!-- Bootstrap Core CSS -->
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/newPoll.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css">




<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


<!-- Bootstrap DatePicker JavaScript -->
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.0.2/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js"></script>

<!-- Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>

<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>
<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/homepage.css"
	rel="stylesheet">

<script
	src="${pageContext.request.contextPath}/js/VotingJs/checkBoxLimitFunction.js"></script>

<!-- Check for the required parameteres here, if one is missing throw an error -->
<!-- TODO -->
<c:if test="${empty options}">
	<c:set var="theError"
		value="You cannot enter this page like this! Use the vote button in the homepage ! :)"
		scope="session" />
	<c:redirect url="/Pages/errorPage.jsp">
	</c:redirect>
</c:if>

<c:if test="${empty poll_id}">
	<c:set var="theError"
		value="You cannot enter this page like this! Use the vote button in the homepage ! :)"
		scope="session" />
	<c:redirect url="/Pages/errorPage.jsp">
	</c:redirect>
</c:if>

<c:if test="${empty QuestionText}">
	<c:set var="theError"
		value="You cannot enter this page like this! Use the vote button in the homepage ! :)"
		scope="session" />
	<c:redirect url="/Pages/errorPage.jsp">
	</c:redirect>
</c:if>

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

	<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-main-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div
				class="collapse navbar-collapse navbar-right navbar-main-collapse">
				<ul class="nav navbar-nav">
					<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
					<li class="hidden"><a href="#page-top"></a></li>
					<li><a class="page-scroll"
						href="${pageContext.request.contextPath}/Pages/homePage.jsp">Home</a></li>
					<li><a class="page-scroll"
						href="${pageContext.request.contextPath}/Pages/homePage.jsp#personal">Your
							Polls</a></li>
					<li><a
						href="${pageContext.request.contextPath}/Pages/newPoll.jsp">Create
							New Poll</a></li>
					<li><a
						href="${pageContext.request.contextPath}/Pages/settings.jsp">Settings</a></li>
					<li><a href="${pageContext.request.contextPath}/Logout">Logout</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>

	<!-- Generate the possible votable options  -->
	<!--  DO not change the structure if you don't know what you are doing, check the javascript  -->
	<header class="intro">
		<section id="all" class="container all-section text-center">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<p>
					<h1>
						Question : <br />
							
					</h1>
					
					<p> "<c:out value="${QuestionText}"></c:out>"</p>
					<br />
					<p>
						Limit of Choices :
						<c:out value="${numberOfLimitedChoices}"></c:out>
					</p>

					<FORM NAME="pickAble"
						action="${pageContext.request.contextPath}/VoteConfirmationServlet"
						method="post">



						<!-- Generate the messages-->


						<c:forEach items="${options}" var="option">
							<br />
							<INPUT TYPE="checkbox" class="singleCheckBox"
								NAME="MagicGeneratedCheckBox" value="${option.key}"
								onclick="checkboxlimit(${numberOfLimitedChoices},this)">

							<label for="${option.key}"><span> <c:out
										value="${option.value}"></c:out></span></label>

							<input type="hidden" name="poll_id" value="${poll_id}">

						</c:forEach>

					

						<!-- test for javascript, leaving the javascript there for now -->
						<!-- <button name="btnSomething" onclick="trySoimething()" >MEHEHEH </button> -->
						<c:if test="${userCanAddOptions}">
							<br />
									
						<div id="divForGeneratedTextFields" class="textfield"></div>
						<br />
							<button type="button" class="ResultVoting"
								name="btnSomething" onclick="addMore(${numberOfLimitedChoices})">Create
								additional Option</button>
							<button type="button" name="btnSomething"
								class="ResultVoting destroyer">Delete additional Option</button>
								<br />

						</c:if>
							<br /> <input type="submit" class="PlaceVotesButton" />
						
					</FORM>
				</div>
			</div>
		</section>
	</header>


</body>


</html>

