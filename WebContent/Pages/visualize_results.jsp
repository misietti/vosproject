<%@page import="java.util.Map"%>
<%@page import="java.util.Set"%>
<%@page import="it.unibz.vos.util.WebPagesUtilities"%>
<%@page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/css/grayscale.css" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.5.1.css">
<link href="${pageContext.request.contextPath}/css/visualizeResults.css" rel="stylesheet">

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>



<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script> -->

<script
	src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="http://cdn.oesmith.co.uk/morris-0.5.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/graph.js"></script>


<%
	Object  shouldBemap =  request.getAttribute("MapOfValues");
if (shouldBemap == null)
{
	WebPagesUtilities.redirectToErrorSite(response, request, this.getServletContext(), "Use the apprioprate site to use the visualization, that is log into the homepage and click the button for visualizing the results");
return;
	}
if (!(shouldBemap instanceof  Map)) 	
{
	WebPagesUtilities.redirectToErrorSite(response, request, this.getServletContext(), "The passed attribute wasn't a map of results.");
	return;
}
Map<String,Integer> theMap = (Map<String,Integer>)shouldBemap; // will crash if wrong
Set<String> labels = theMap.keySet();
%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Results</title>
</head>
<body>
<header class="intro">
		<section id="all" class="container all-section text-center">
	<h1>
		Here are the results for:
		"<c:out value="${question}" />"
	</h1>
	<br />


<p> Pie chart </p>
	<div id="chart"></div>
	<!-- really ugly way to write javascript.. -->
	<script type="text/javascript">
		Morris
				.Donut({
					element : 'chart',
					data : [
	<%Iterator<String> iter = labels.iterator();
	         while (iter.hasNext()) {
	             String label = iter.next();
	   		  out.println("{");
			  out.println("label: \"" + label + "\",");
			  out.println(" value: " + theMap.get(label));
			  out.println("}");
	             if ( iter.hasNext()) {
	            	    // last iteration of the whole thing
	            	 out.println(",");
	            	}
	         }%>
		]
				});
	</script>
<br><br><br>
<p> Bar chart </p>
<div class="divForGraph"></div>

<script>
arrayOfData = new Array(
		<%
		int maxLength = 0;
		Iterator<String> itera = labels.iterator();
        while (itera.hasNext()) {
        	
            String label = itera.next();
            if (label.length() > maxLength) maxLength = label.length();
  		  out.println("[");  
  		  out.println(theMap.get(label));
		  out.println(",'" + label +"',");
		  out.println("getRandomColor()");
		  out.println("]");
            if ( itera.hasNext()) {
           	    // last iteration of the whole thing
           	 out.println(",");
           	}
        }
		int elemts = labels.size() *( maxLength * 9);
		
        %>
	); 
	
$('.divForGraph').jqBarGraph({ data: arrayOfData ,width:<%   out.println(elemts);%> , sort: 'desc' });

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
</script>

</section>
</header>
</body>
</html>