<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    	
 	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/homepage.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">
<link
	href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Uups! Error!</title>
</head>
<body>

<header class="intro">
		<section id="all" class="container all-section text-center">
<h1>Uups! There was an ERROR! </h1> 
<img src="${pageContext.request.contextPath}/img/error.png" height="300" width="300"/> 
<c:if test="${empty theError}">	
<p>Error of Error, there is no error message, possibly you are just messing with me... Are you?</p>
</c:if>

<p> <c:out value="${theError}"> 
 </c:out>

</p>
<c:remove var="theError" scope="session"/>
</section>
</header>
</body>
</html>