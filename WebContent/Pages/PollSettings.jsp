<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Poll Settings</title>
<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/css/pollSettings.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/js/jquery.easing.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/js/grayscale.js"></script>


</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">


	<!-- Navigation -->
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-main-collapse">
				<i class="fa fa-bars"></i>
			</button>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div
			class="collapse navbar-collapse navbar-right navbar-main-collapse">
			<ul class="nav navbar-nav">
				<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
				<li class="hidden"><a href="#page-top"></a></li>
				<li><a class="page-scroll"
					href="${pageContext.request.contextPath}/HomePageServlet">Home</a></li>
				<li><a
					href="${pageContext.request.contextPath}/Pages/newPoll.jsp">Create
						New Poll</a></li>
				<li><a
					href="${pageContext.request.contextPath}/UserSettingsRequestServlet">Settings</a></li>
				<li><a href="${pageContext.request.contextPath}/Logout">Logout</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container --> </nav>


	<div class="wrapper">
		<div class="box">
			<div class="row row-offcanvas row-offcanvas-left">


				<!-- main right col -->
				<div class="column col-sm-10 col-xs-11" id="main">


					<div class="padding">
						<div class="full col-sm-9">

							<!-- content -->
							<div class="row">
								<h3>
									Question: "
									<c:out value="${poll.question}" />
									"
								</h3>
								<!-- main col left -->
								<div class="col-sm-5">

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4>Poll options</h4>
										</div>
										<div class="panel-body">
											<ul class="listCloser">
												<c:forEach items="${listofoptions}" var="item">
													<li><c:out value="${item.getOptionText()}"></c:out></li>

												</c:forEach>
											</ul>
										</div>
									</div>


									<div class="panel panel-default">
										<div class="panel-heading">
											<h4>Users options</h4>
										</div>
										<div class="panel-body">
											<c:choose>

												<c:when test="${poll.getCanUserAddOption()}">
													<td><p>Users can add options</p></td>
													<form
														action="${pageContext.request.contextPath}/ChangeUserAdditionSettingServlet"
														method="post">
														<input type="hidden" name="poll_id"
															value="${poll.getID()}"> <input type="hidden"
															name="pollenablinguser" value="0"> <input
															type="submit" class="SettingsButton"
															value="Remove ability of adding options">
													</form>
												</c:when>
												<c:otherwise>
													<td><p>Users cannot add options</p></td>
													<form
														action="${pageContext.request.contextPath}/ChangeUserAdditionSettingServlet"
														method="post">
														<input type="hidden" name="poll_id"
															value="${poll.getID()}"> <input type="hidden"
															name="pollenablinguser" value="1"> <input
															type="submit" class="SettingsButton"
															value="Add ability of adding options">
													</form>
												</c:otherwise>

											</c:choose>
										</div>
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4>Poll status</h4>
										</div>
										<div class="panel-body">

											<c:choose>

												<c:when test="${poll.isClosed()}">
													<td><p>The poll is closed!</p></td>
												</c:when>
												<c:otherwise>
													<p>The poll is currently Open!</p>
													<p>Caution once closed, forever closed! You have been
														warned!</p>
													<p>You will be redirected to the HomePage</p>
													<form method="post"
														action="${pageContext.request.contextPath}/ClosePollServlet">
														<input type="hidden" name="poll_id"
															value="${poll.getID()}"> <input
															class="SettingsButton" type="submit" value="Close Poll" />

													</form>
												</c:otherwise>

											</c:choose>




										</div>
									</div>





								</div>

								<!-- main col right -->
								<div class="col-sm-7">


									<div class="panel panel-default">
										<div class="panel-heading">
											<h4>Privacy Options</h4>
										</div>
										<div class="panel-body">

											<c:choose>
												<c:when test="${poll.isPrivate()}">
													<p>The poll is private</p>
													<ul class="list-inline">
														<li><form
																action="${pageContext.request.contextPath}/ChangePollPrivatenessServlet"
																method="post">
																<input type="hidden" name="poll_id"
																	value="${poll.getID()}"> <input type="hidden"
																	name="want_private" value="0"> <input
																	type="submit" class="SettingsButton"
																	value="Make Poll Public">

															</form></li>


														<li><form method="post"
																action="${pageContext.request.contextPath}/InviteUserServlet">
																<input type="hidden" name="poll_id"
																	value="${poll.getID()}"> <input
																	class="SettingsButton" type="submit"
																	value="Invite User To Vote" />

															</form></li>
													</ul>
												</c:when>
												<c:otherwise>
													<p>The poll is public</p>
													<form
														action="${pageContext.request.contextPath}/ChangePollPrivatenessServlet"
														method="post">
														<input type="hidden" name="poll_id"
															value="${poll.getID()}"> <input type="hidden"
															name="want_private" value="1"> <input
															type="submit" class="SettingsButton"
															value="Make Poll private">
													</form>
												</c:otherwise>
											</c:choose>
										</div>
									</div>


									<div class="panel panel-default">
										<div class="panel-heading">
											<h4>Notifications</h4>
										</div>
										<div class="panel-body">
											<p>
												<img
													src="${pageContext.request.contextPath}/img/alarm-icon.png"
													class="img-circle pull-right">

												<c:choose>

													<c:when test="${poll.willNotifyUsers()}">
														<td><p>When the poll will be closed the users
																will be notified</p></td>
														<form
															action="${pageContext.request.contextPath}/ChangeNotifyUsersServlet"
															method="post">
															<input type="hidden" name="poll_id"
																value="${poll.getID()}"> <input type="hidden"
																name="pollenablingNotify" value="0"> <input
																type="submit" class="SettingsButton"
																value="Disable Notification of Results">
														</form>
													</c:when>
													<c:otherwise>
														<td><p>When the poll will be closed the users
																won't be notified</p></td>
														<form
															action="${pageContext.request.contextPath}/ChangeNotifyUsersServlet"
															method="post">
															<input type="hidden" name="poll_id"
																value="${poll.getID()}"> 
																<input type="hidden"
																name="pollenablingNotify" value="1">
																 <input	type="submit" class="SettingsButton"
																value="Enable Notification of Results">
														</form>
													</c:otherwise>

												</c:choose>
										</div>
									</div>





								</div>
							</div>
							<!--/row-->


						</div>
						<!-- /col-9 -->
					</div>
					<!-- /padding -->
				</div>
				<!-- /main -->

			</div>
		</div>
	</div>


	<!--post modal-->
	<div id="postModal" class="modal fade" tabindex="-1" role="dialog"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">×</button>
					Update Status
				</div>
				<div class="modal-body">
					<form class="form center-block">
						<div class="form-group">
							<textarea class="form-control input-lg" autofocus
								placeholder="What do you want to share?"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<div>
						<button class="btn btn-primary btn-sm" data-dismiss="modal"
							aria-hidden="true">Post</button>
						<ul class="pull-left list-inline">
							<li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li>
							<li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li>
							<li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>



</body>
</html>