var currentNumberOfAdditionalTextboxes = 0;

function checkboxlimit(limit,element) {
var limit = limit;
	var sum = checkAmountOfChosenCheckboxes() + currentNumberOfAdditionalTextboxes;
	if (sum > limit) {
		element.checked = false;
		alert("Sorry but you can only select a maximum of " + limit
				+ " options");
		
	}

}

function checkAmountOfChosenCheckboxes() {
	var amount = 0;
	var checkgroup = document.getElementsByName("MagicGeneratedCheckBox");
	for (var i = 0; i < checkgroup.length; i++) {
		if (checkgroup[i].checked == true) {
			amount++;
		}
	}

	return amount;
}

function addMore(limit) {
	var limit = limit;
	var checkedBoxes = checkAmountOfChosenCheckboxes();
	if (limit == (currentNumberOfAdditionalTextboxes + checkedBoxes)) {
		alert("You cannot exceed the limit of votes");
		return;
	}

	var element = document.createElement("input");
	var element2 = document.createElement("input");

	
	
	
	// Create Labels
	var label = document.createElement("Label");
	label.innerHTML = "AdditionalOption " + currentNumberOfAdditionalTextboxes;

	// Assign different attributes to the element.
	element.setAttribute("type", "text");
	element.setAttribute("value", "");
	element.setAttribute("name", "newOptions[]");
	element.setAttribute("required", "");
//	element.setAttribute("style", "width:200px");
	element.setAttribute("class", "form-control textBoxNum"
			+ currentNumberOfAdditionalTextboxes); 
	element.setAttribute("placeholder","Type option to insert");
	var br = document.createElement('br');
	br.setAttribute("class", "form-control textBoxNum"
			+ currentNumberOfAdditionalTextboxes);
	label.setAttribute("style", "font-weight:normal");

	var foo = document.getElementById("divForGeneratedTextFields");
	foo.appendChild(element);
	//foo.appendChild(br);
	currentNumberOfAdditionalTextboxes++;
}

var number_of_rows = 0;
$(function() {
	$(document).on('click', '.destroyer', function() {

		$(".textBoxNum" + (currentNumberOfAdditionalTextboxes - 1)).remove();
		if (currentNumberOfAdditionalTextboxes != 0) {
			currentNumberOfAdditionalTextboxes--;
		} else {
			alert("You cannot delete what doesn't exist :)");
		}
	});

});

function deleteMyself() {
	this.Delete();
}
