delete from canvote;
delete from chooses;
delete from option;
delete from poll;
delete from wuser;


DROP TABLE CHOOSES;
DROP TABLE CANVOTE;
DROP TABLE OPTION;
DROP TABLE POLL;
DROP TABLE WUSER;


DROP SEQUENCE option_id_seq;
DROP SEQUENCE poll_id_seq;
DROP SEQUENCE user_id_seq;
