WEBAPPS_PATH=/home/michal/apache-tomcat-7.0.61/webapps
PROJECT_PATH=/home/michal/git/vosproject/target
TOMCAT_EXEC=/home/michal/apache-tomcat-7.0.61/bin
MAVEN_PROJECT_PATH=/home/michal/git/vosproject

cd $MAVEN_PROJECT_PATH
#mvn clean
#mvn eclipse:eclipse
mvn package
cp $PROJECT_PATH/VOSProject-0.war $WEBAPPS_PATH
cd $WEBAPPS_PATH
ls -l | grep --quiet VOSProject-0.war

if [ $? -eq 0 ]
then 
	echo "WAR successfully copied!"
	mv VOSProject-0.war VOSProject.war
	rm -R VOSProject
	cd $TOMCAT_EXEC
	echo "Shutting down Tomcat"
	bash $TOMCAT_EXEC/shutdown.sh
	bash $TOMCAT_EXEC/startup.sh
	cd ~
fi


exit 0

